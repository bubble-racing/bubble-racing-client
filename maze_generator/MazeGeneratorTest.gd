extends Node2D

# Generate a maze
var MazeGenerator = preload("../maze_generator/MazeGenerator.gd")
var maze_generator = MazeGenerator.new()
var maze_width = 50
var maze_height = 50
var maze = maze_generator.generate(maze_width, maze_height)

# Draw the maze
var line_color = 0xFFFFFFFF
var line_width = 8
var maze_scale = 10
var offset = Vector2(20, 20)
func _draw():
	for node in maze:
		for connection in node.connections:
			draw_line(
				node.position * maze_scale + offset,
				connection * maze_scale + offset,
				line_color,
				line_width
			)
