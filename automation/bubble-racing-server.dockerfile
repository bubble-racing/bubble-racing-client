# Image to run the app
FROM ubuntu:22.04 as base
RUN apt-get update -y && \
    apt-get install -y unzip wget && \
    wget -nv https://downloads.tuxfamily.org/godotengine/3.5.1/Godot_v3.5.1-stable_linux_server.64.zip && \
    unzip Godot_v3.5.1-stable_linux_server.64.zip && \
    rm Godot_v3.5.1-stable_linux_server.64.zip && \
    ln -s /Godot_v3.5.1-stable_linux_server.64 /usr/bin/godot

# Image to build the app
FROM barichello/godot-ci:3.5.1 as build
COPY ./ /src
WORKDIR /src
RUN mkdir -v -p exports && \
    godot -v --no-window --export-pack "Server" exports/bubble-racing-server.pck

# Final image
FROM base as app
COPY --from=build src/exports/bubble-racing-server.pck /
CMD ["godot", "--main-pack", "bubble-racing-server.pck", "--server"]