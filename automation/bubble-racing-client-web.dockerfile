# Image to build the app
FROM barichello/godot-ci:3.5.1 as build
COPY ./ /src
WORKDIR /src
RUN mkdir -v -p exports/web
RUN godot -v --no-window --export "HTML5" exports/web/index.html
RUN bash automation/cache-bust.sh /src/exports/web

# Image to serve the app
FROM nginx:1-alpine as web
RUN apk --no-cache upgrade
COPY --from=build src/exports/web /usr/share/nginx/html
COPY automation/nginx.conf /etc/nginx/nginx.conf
COPY privacy-policy.html /usr/share/nginx/html/privacy-policy.html
COPY support.html /usr/share/nginx/html/support.html