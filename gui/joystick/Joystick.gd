extends Node2D

var active = false
var max_dist = 90
var center = Vector2(0, 0)
var control_position = center


# Called when the node enters the scene tree for the first time.
func _ready():
	$TextureButton.connect("button_down", self, "_touch_start")
	$TextureButton.connect("button_up", self, "_touch_stop")


func _touch_start():
	active = true


func _touch_stop():
	active = false


func _process(delta):
	if active && Input.is_mouse_button_pressed( 1 ): # Left click
		var position = get_local_mouse_position().clamped(max_dist)
		control_position = position / max_dist
		$ControllerPosition.position = position
	else:
		control_position = center
		$ControllerPosition.position = center
