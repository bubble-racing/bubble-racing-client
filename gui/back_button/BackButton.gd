extends Node2D

var ring_initial_scale
var ring_grow_scale = Vector2(0.08, 0.08)
var ring_initial_alpha = 0.3
var ring_grow_alpha = 0.3

var circle_initial_scale
var circle_grow_scale = Vector2(0.05, 0.05)
var circle_initial_alpha = 0.5
var circle_grow_alpha = 0.5


func _ready():
	ring_initial_scale = $Ring.scale
	circle_initial_scale = $Circle.scale
	position = Vector2(85, 85)


func _process(_delta):
	var loudness = Music.loudness / 100.0
	$Ring.scale = ring_initial_scale + (ring_grow_scale * loudness)
	$Ring.modulate.a = ring_initial_alpha + (ring_grow_alpha * loudness)
	$Circle.scale = circle_initial_scale + (circle_grow_scale * loudness)
	$Circle.modulate.a = circle_initial_alpha + (circle_grow_alpha * loudness)
