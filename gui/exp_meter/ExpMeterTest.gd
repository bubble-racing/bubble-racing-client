extends Node2D


var rng: RandomNumberGenerator
var data: Dictionary


func _ready():
	rng = RandomNumberGenerator.new()
	$Randomize.connect('pressed', self, '_randomize')
	$WinPearls.connect('pressed', self, '_win_pearls')
	_randomize()


func _randomize():
	data = {
		'nature': {
			'speed': rng.randi_range(0, 3),
			'tough': rng.randi_range(0, 3),
			'skill': rng.randi_range(0, 3)
		},
		'nurture': {
			'speed': rng.randi_range(0, 3),
			'tough': rng.randi_range(0, 3),
			'skill': rng.randi_range(0, 3)
		}
	}
	data.pearls = 0
	$ExpMeter.render(data)


func _win_pearls():
	$ExpMeter.win_pearls(300)
