extends Node2D


var display_pearls: int = 0
var target_pearls: int = 0
var max_pearls: int = 0
var attribute: String = ''
var data: Dictionary


func _process(_delta):
	if display_pearls != target_pearls:
		var diff: float = float(target_pearls - display_pearls) * 0.005
		display_pearls += ceil(diff)
		var perc = float(display_pearls) / float(max_pearls)
		$ProgressBar.value = perc * 100
	elif attribute:
		if Settings.persist.sound_effects:
			$AudioStreamPlayer.play()
		var node_path = '{attribute}Row'.format({'attribute': attribute.capitalize()})
		get_node(node_path).level_up()
		Music.dampen(30, 1)
		attribute = ''


func render(_data: Dictionary):
	data = _data
	data.pearls = data.get('pearls', 0)
	target_pearls = data.pearls
	display_pearls = data.pearls
	max_pearls = _calc_train_cost(data)
	var perc = float(data.pearls) / float(max_pearls)
	$ProgressBar.value = perc * 100
	$SpeedRow.render(data.nature.speed, data.nurture.speed)
	$ToughRow.render(data.nature.tough, data.nurture.tough)
	$SkillRow.render(data.nature.skill, data.nurture.skill)


func win_pearls(pearl_count: int) -> void:
	if data.nurture.speed >= 3 && data.nurture.tough >= 3 && data.nurture.skill >= 3:
		return
	render(data)
	target_pearls = min(target_pearls + pearl_count, max_pearls)
	var cost = _calc_train_cost(data)
	data.pearls += pearl_count
	if data.pearls >= cost:
		data.pearls -= cost
		_rank_up(data)
	
	
func _rank_up(data: Dictionary) -> void:
	var attributes = []
	for attribute in ['speed', 'tough', 'skill']:
		if data.nurture[attribute] < 3:
			attributes.append(attribute)
	if len(attributes) > 0:
		attributes.shuffle()
		attribute = attributes[0]
		data.nurture[attribute] = min(3, data.nurture[attribute] + 1)


func _calc_train_cost(data: Dictionary):
	var nurture_rank = data.nurture.speed + data.nurture.tough + data.nurture.skill
	var nature_rank = data.nature.speed + data.nature.tough + data.nature.skill
	var rank = 1 + nature_rank + nurture_rank
	var cost = 100 * rank
	return cost
