extends Node2D


func _ready():
	$Button.connect('pressed', self, '_on_pressed')
	_render()


func _render():
	if Settings.persist.get('autopilot', false):
		$Circle.modulate = Color('117ccc')
		$Wheel.modulate = Color('ffffff')
		$Label.visible = true
	else:
		$Circle.modulate = Color(1.0, 1.0, 1.0, 0.7)
		$Wheel.modulate = Color('117ccc')
		$Label.visible = false


func _on_pressed():
	Settings.persist.autopilot = !Settings.persist.get('autopilot', false)
	_render()
