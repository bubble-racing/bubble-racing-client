extends Node2D

var api_url = BaseURL.get_base_url() + '/api/featured-bubble'
var time_left = 2000
var bubble_position: Vector2

func _ready():
	bubble_position = $Panel/Bubble.position
	
	# Connect signals for HTTPRequest node
	$HTTPRequest.connect("request_completed", self, "_on_request_completed")
	$Timer.connect('timeout', self, '_on_timer')

	# Send the GET request
	Logger.info('FeaturedBubble::_ready - get url: ', api_url)
	$HTTPRequest.request(api_url)


func _process(_delta):
	var target_position = $Panel/Bubble.get_local_mouse_position()
	$Panel/Bubble.target_position = target_position
	$Panel/Bubble.position = bubble_position


func _on_request_completed(result, response_code, headers, body_bytes):
	if response_code != 200:
		Logger.error('FeaturedBubble::_on_request_completed - got response code:', response_code)
		return
		
	var body_str = body_bytes.get_string_from_utf8()
	var json_result = JSON.parse(body_str)
	if json_result.error != OK:
		Logger.error('FeaturedBubble::_on_request_completed - failed to parse JSON response')
		return
	
	var response = json_result.result
	Logger.info('FeaturedBubble::_on_request_completed', response)
	$Panel/Bubble.customize(response.bubble)
	$AnimationPlayer.play('slide_in')
	

func _on_timer():
	if time_left > 0:
		time_left -= 1
		$Panel/Label.text = TimeFormatter.format(time_left)
