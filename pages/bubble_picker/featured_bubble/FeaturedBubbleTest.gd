extends Node2D


func _ready():
	var window_size = get_viewport().get_visible_rect().size
	$FeaturedBubble.position = Vector2(window_size.x, 0)
