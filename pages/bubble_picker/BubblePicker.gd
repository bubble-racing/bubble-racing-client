extends Node2D


func _ready():
	# Connect back button
	$BackButton/Button.connect('pressed', self, '_on_back_pressed')
	
	# Fit background to screen
	$Background.enable_auto_resize()
	
	# Auto position ui elements
	get_viewport().connect("size_changed", self, "_on_size_changed")
	_on_size_changed()
	
	# Bubble Customizer
	$Panel/BubbleCustomizer.init(MyBubbles.get_current_bubble_id(), MyBubbles.get_bubbles())


func _on_back_pressed():
	MyBubbles.set_current_bubble_id($Panel/BubbleCustomizer.selected_bubble_id)
	Settings.save_game()
	get_node("/root/Main").set_scene_by_name('title')


func _on_size_changed():
	var window_size = get_viewport().get_visible_rect().size
	$Panel.position = window_size / 2


func get_bubble_of_the_hour():
	var rng = RandomNumberGenerator.new()
	var time = Time.get_time_dict_from_system()
	rng.seed = hash(String(time.year) + String(time.month) + String(time.day) + time.hour)
	var nature_points = rng.randi_range(0, 9)
	var bubble_data = MyBubbles.generate_random_bubble()
	bubble_data.nature = MyBubbles.generate_nature(nature_points)
