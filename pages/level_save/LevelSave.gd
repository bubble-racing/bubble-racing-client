extends Node2D


func _ready():
	# Fit background to screen
	$Background.enable_auto_resize()
	
	# Listen to buttons
	$BackButton/Button.connect("pressed", self, "_on_back_pressed")
	$Panel/Button.connect("pressed", self, "_on_save_pressed")
	
	# Prefil UI
	var level = Settings.persist.get('wip_level', {})
	$Panel/LineEdit.text = level.get('title', '')
	$Panel/Publish.pressed = level.get('published', false)


func _on_back_pressed():
	get_node("/root/Main").set_scene_by_name('editor')


func _on_save_pressed():
	var level = Settings.persist.get('wip_level', {})
	level.title = $Panel/LineEdit.text
	level.published = $Panel/Publish.pressed
	
	print('----------------')
	print(to_json(level))
	print('----------------')
	
	LevelStorage.store(level)
	
	get_node("/root/Main").set_scene_by_name('editor')
