extends Node2D

var page = 0
var levels_per_page = 3
var levels: Array
var spacing: int = 260
var LevelOption = preload("res://pages/level_load/LevelOption.tscn")


func _ready():
	# Fit background to screen
	$Background.enable_auto_resize()
	
	# Listen to buttons
	$BackButton/Button.connect("pressed", self, "_on_back_pressed")
	$Panel/LeftButton/Button/Button.connect('pressed', self, '_on_left_pressed')
	$Panel/RightButton/Button/Button.connect('pressed', self, '_on_right_pressed')

	# Populate list of levels
	levels = LevelStorage.get_local_levels().keys()
	
	# Draw initial page
	render()
	
	# Auto position ui elements
	get_viewport().connect("size_changed", self, "_on_size_changed")
	_on_size_changed()
	
	# Load list of levels on account
	# var account = MyAccount.get_my_account()
	# if account:
	#	var url = BaseURL.get_base_url() + '/api/users/' + account.user_id + '/levels'
	#	$HTTPRequest.request(url)
	#	$HTTPRequest.connect('request_completed', self, '_on_request_completed')


func render():
	# Clear any old level options
	var holder = $Panel/OptionHolder
	var children = holder.get_children()
	for child in children:
		holder.remove_child(child)
	
	# Add new level options
	var i = page * levels_per_page
	var max_i = (page + 1) * levels_per_page
	while ( i < max_i ):
		if i < len(levels):
			var slug = levels[i]
			render_level_preview(slug, i % levels_per_page)
		i = i + 1
	
	# Activate / Deactivate pagination buttons
	if page == 0:
		$Panel/LeftButton.modulate.a = 0.3
	else:
		$Panel/LeftButton.modulate.a = 1
	
	if (page + 1) * levels_per_page >= len(levels):
		$Panel/RightButton.modulate.a = 0.3
	else:
		$Panel/RightButton.modulate.a = 1
		


func render_level_preview(slug: String, pos: int):
	var option: Node2D = LevelOption.instance()
	var level = LevelStorage.fetch(slug)
	var renderer = option.get_node("LevelRenderer")
	var map_data = level.map
	var map = Map2D.new()
	map.set_data(map_data)
	$Panel/OptionHolder.add_child(option)
	option.position = Vector2((pos - 1) * spacing, 0)
	renderer.add_dots = false
	renderer.mode = 'icon'
	renderer.render_map(map, 'remote')
	
	# Center the level
	var level_size = Vector2(map.max_pos.x - map.min_pos.x, map.max_pos.y - map.min_pos.y) * Settings.tile_size
	var target_size = Vector2(180, 280)
	var scale_xy = target_size / level_size
	var scale_min = min(scale_xy.x, scale_xy.y)
	var offset = map.min_pos * Settings.tile_size * scale_min
	var center_offset = (target_size - ((map.max_pos - map.min_pos) * Settings.tile_size * scale_min)) / 2
	renderer.scale = Vector2(scale_min, scale_min)
	renderer.position += -offset + center_offset
	
	# Make it clickable
	option.get_node('Button').connect('pressed', self, '_on_level_pressed', [slug])


func _on_size_changed():
	var window_size = get_viewport().get_visible_rect().size
	$Panel.position = window_size / 2


func _on_back_pressed():
	get_node("/root/Main").set_scene_by_name('editor')


func _on_left_pressed():
	if page > 0:
		page -= 1
		render()


func _on_right_pressed():
	if (page + 1) * levels_per_page < len(levels):
		page += 1
		render()
		

func _on_level_pressed(slug: String):
	var level = LevelStorage.fetch(slug)
	if level.has('map'): # handle old data without crashing
		Settings.persist.wip_level = level
	get_node("/root/Main").set_scene_by_name('editor')


#func _on_request_completed(result: int, response_code: int, headers: PoolStringArray, body: PoolByteArray):
#	if(result != 0):
#		print('Error making request, error num: ' + String(result))
#		print('See error codes here: https://docs.godotengine.org/en/stable/classes/class_httprequest.html')
#	if(response_code == 200):
#		var parse_result = JSON.parse(body.get_string_from_utf8())
#		if (parse_result.error):
#			print(parse_result.error_string)
#			return
#		
#		var listings = parse_result.result
#		var levels = Settings.persist.get('levels', {})
#		for listing in listings:
#			if !levels.has(listing.title):
#				$Panel/OptionButton.add_item(listing.title)
