extends Node2D


func _ready():
	# Connect back button
	$BackButton/Button.connect('pressed', self, '_on_back_pressed')
	
	# Fit background to screen
	$Background.enable_auto_resize()
	
	# Auto position ui elements
	get_viewport().connect("size_changed", self, "_on_size_changed")
	_on_size_changed()
	
	# Settings
	$Panel/SoundEffectsToggle.pressed = Settings.persist.get('sound_effects', true)
	$Panel/SoundEffectsToggle.connect('toggled', self, '_on_sound_effects_toggled')
	$Panel/MusicToggle.pressed = Settings.persist.get('music', true)
	$Panel/MusicToggle.connect('toggled', self, '_on_music_toggled')
	$Panel/Autopilot.pressed = Settings.persist.get('autopilot', false)
	$Panel/Autopilot.connect('toggled', self, '_on_autopilot_toggled')
	$Panel/FullscreenToggle.pressed = Settings.persist.get('fullscreen', false)
	$Panel/FullscreenToggle.connect('toggled', self, '_on_fullscreen_toggled')
	
	# Bubble Customizer
	$Panel/BubbleCustomizer.init(MyBubbles.get_current_bubble_id(), MyBubbles.get_bubbles())


func _on_back_pressed():
	Settings.save_game()
	get_node("/root/Main").set_scene_by_name('title')


func _on_size_changed():
	var window_size = get_viewport().get_visible_rect().size
	$Panel.position = window_size / 2


func _on_sound_effects_toggled(value: bool):
	Settings.persist.sound_effects = value


func _on_music_toggled(value: bool):
	Settings.persist.music = value
	if (value):
		Music.play_title()
	else:
		Music.stop(Music.title)


func _on_autopilot_toggled(value: bool):
	Settings.persist.autopilot = value


func _on_fullscreen_toggled(value: bool):
	Settings.persist.fullscreen = value
	OS.window_fullscreen = value
