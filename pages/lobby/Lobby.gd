extends Node2D

var player_name: String
var backoff: float = 2.0
var join_in_progress: bool = false
var connected: bool = false


func _ready():
	# Connect back button
	$BackButton/Button.connect('pressed', self, '_on_back_pressed')
	
	# Fit background to screen
	$Background.enable_auto_resize()
	
	# Stop the bubble from popping
	$Panel/Bubble.min_y = -9999
	$Panel/Bubble.customize(MyBubbles.get_current_bubble())
	$Panel/Bubble/Name.visible = false
	
	# Clear out old users from previous games
	ClientUsers.clear()
	
	# Start connecting
	_get_player_name()
	get_tree().connect('network_peer_connected', self, '_player_connected')
	get_tree().connect('network_peer_disconnected', self, '_player_disconnected')
	get_tree().connect('connected_to_server', self, '_connected_ok')
	get_tree().connect('connection_failed', self, '_connected_fail')
	get_tree().connect('server_disconnected', self, '_server_disconnected')
	RPC.connect('register_user_result', self, '_register_user_result')


func _on_back_pressed():
	ClientConnection.close()
	get_node('/root/Main').set_scene_by_name('title')


func _get_player_name():
	if !get_tree():
		return
		
	Logger.info('Lobby::Getting player name...')
	if (!Settings.persist.get('name')):
		Settings.persist.name = NameGenerator.generate()
	_got_player_name()


func _got_player_name():
	if !get_tree():
		return
		
	player_name = Settings.persist.name
	Logger.info('Lobby::Got player name! (hi ' + player_name + ')')
	_fetch_game_server()
	

func _fetch_game_server():
	if !get_tree():
		return
		
	var full_url = BaseURL.get_base_url() + '/api/lobby?platform=' + OS.get_name()
	Logger.info('Lobby::Fetching game server from ' + full_url)
	$HTTPRequest.connect('request_completed', self, '_got_game_server')
	$HTTPRequest.request(full_url)
	$Panel/Bubble.target_position.y = -90


func _got_game_server(result, response_code, _headers, body) -> void:
	if !get_tree():
		return
		
	if response_code != 200:
		Logger.info('Lobby::Fetch failed with result code ' + String(result) + ' and response code ' + String(response_code))
		_retry()
		return
	
	var game_server = JSON.parse(body.get_string_from_utf8()).result
	if !game_server.has('address'):
		Logger.info('Lobby::No gameserver was found')
		_retry()
		return
		
	Logger.info('Lobby::Got game server: ' + game_server.address + ':' + str(game_server.port))
	_join_game_server(game_server)


func _join_game_server(game_server: Dictionary) -> void:
	if join_in_progress || !get_tree():
		return
	
	join_in_progress = true
	ClientConnection.open(game_server.address)
	$Panel/Bubble.target_position.y = 0


# Called on both clients and server when a peer connects.
func _player_connected(id):
	Logger.info('Lobby::Player Connected: ' + str(id))
	

# Called on both clients and server when a peer disconnects.
func _player_disconnected(id):
	Logger.info('Lobby::Player Disconnected: ' + str(id))


# Only called on clients
func _connected_ok():
	if !get_tree():
		return
		
	Logger.info('Lobby::Connected Ok')
	connected = true
	_register_user()


# Only called on clients
func _connected_fail():
	if !get_tree():
		return
	Logger.info('Lobby::Connection Failed')
	_retry()
	
	
func _server_disconnected():
	Logger.info('Lobby::Server kicked us')


func _register_user():
	if !get_tree():
		return
		
	Logger.info('Lobby::Synchronizing')
	$Panel/Bubble.target_position.y = 90
	RPC.rpc_id(1, 'register_user', {
		'bubble': MyBubbles.get_current_bubble(),
		'level': LevelStorage.get_random_published_level()
	})


func _register_user_result(result: String) -> void:
	Logger.info('Lobby::_register_user_result ' + result)
	if !get_tree():
		return
	if result == 'success':
		Settings.mode = 'multi-player'
		get_node('/root/Main').set_scene_by_name('level-voter')
		pass
	else:
		Logger.info('Lobby::Request to join gameserver was denied')
		_retry()


func _retry() -> void:
	Logger.info('Lobby::Retrying in ' + String(round(backoff)) + ' seconds')
	get_tree().network_peer = null
	yield(get_tree().create_timer(backoff), 'timeout')
	backoff *= 1.3
	join_in_progress = false
	_fetch_game_server()
