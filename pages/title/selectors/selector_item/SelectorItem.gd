extends "res://seeker/Seeker.gd"

signal selected

var id = 0
var target_alpha = 0
var target_alpha_speed = 1
var focus = false
var lock = false
var time = ""


func _ready():
	$Ring.visible = false
	$Bubble/Lock.visible = false
	$Time.visible = false
	set_focus(focus)


func _process(delta):
	$Ring.initial_scale = Vector2(.30, .30) # set constantly, something somewhere is setting it back
	if modulate.a < target_alpha:
		modulate.a += delta / target_alpha_speed
	if modulate.a > target_alpha:
		modulate.a -= delta / target_alpha_speed


func set_focus(_focus):
	focus = _focus
	if focus:
		$Ring.visible = true
		$Bubble.focus = true
		$Bubble/Ring2.visible = false
	return self


func set_lock(_lock):
	lock = _lock
	if lock:
		$Bubble/Lock.visible = true
		$Time.visible = false
		$Label.visible = false
	return self


func set_label(_label):
	$Label.visible = true
	$Label.text = _label
	return self


func set_label_color(color: Color):
	$Label.add_color_override("font_color", color)
	return self


func set_time(_time):
	time = _time
	$Time.visible = true
	$Time.text = time
	return self


func _on_Button_pressed():
	if !lock:
		emit_signal('selected', id)
