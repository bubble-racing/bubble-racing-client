extends Node2D

var initial_scale = Vector2(1, 1)
var focus = false


func _process(_delta):
	if focus:
		scale = initial_scale + (initial_scale / 2 * Music.loudness / 200)
	else:
		scale = initial_scale
