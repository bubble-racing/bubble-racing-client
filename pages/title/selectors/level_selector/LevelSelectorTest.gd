extends Node2D

func _init():
	Settings.persist.campaign = {
		'rise': {
			'bubble': 10,
			'down': 20,
			'divers': 30,
			#'turtle': 40,
			#'maze': 70
		},
	}


func _ready():
	$LevelSelector.render('rise')
	$LevelSelector.activate()
	$LevelSelector.connect('selected', self, '_on_level_selected')
	Music.play_race()


func _on_Activate_pressed():
	$LevelSelector.activate()


func _on_Deactivate_pressed():
	$LevelSelector.deactivate()


func _on_level_selected(i):
	print('level selected: ', i)
