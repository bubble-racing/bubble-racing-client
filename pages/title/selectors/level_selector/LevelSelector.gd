extends GenericSelector


func _is_locked(levels:Array, category: String, level_name: String) -> bool:
	var index = levels.find(level_name)
	var lock = false
	if index > 0:
		var previous_level_name = levels[index -1]
		var best_time = Settings.persist.get('campaign', {}).get(category, {}).get(previous_level_name, 0)
		if !best_time:
			lock = true
	return lock


func render(category: String):
	_clear()
	var levels = Levels.levels.get(category).keys()
	for level_name in levels:
		var title = level_name.capitalize()
		var lock = _is_locked(levels, category, level_name)
		var focus = false
		var best_time = Settings.persist.get('campaign', {}).get(category, {}).get(level_name, 0)
		if !best_time && !lock:
			focus = true
		var item = add_item(level_name)
		item.set_label(title)
		item.set_focus(focus)
		item.set_lock(lock)
		if best_time:
			item.set_time(TimeFormatter.format(best_time / 1000.0))
