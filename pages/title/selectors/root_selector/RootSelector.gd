extends GenericSelector


func _ready():
	if Settings.enable_multiplayer:
		add_item('solo').set_label('Solo')
		add_item('lobby').set_label('Online').set_focus(true)
	else:
		add_item('solo').set_label('Race').set_focus(true)
	add_item('editor').set_label('Editor')
	add_item('bubble-picker').set_label('Bubbles')
	add_item('about').set_label('About')
