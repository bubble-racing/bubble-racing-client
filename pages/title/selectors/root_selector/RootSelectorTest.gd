extends Node2D


func _ready():
	$RootSelector.activate()
	$RootSelector.connect('selected', self, '_on_item_selected')
	Music.play_race()


func _on_Activate_pressed():
	$RootSelector.activate()


func _on_Deactivate_pressed():
	$RootSelector.deactivate()


func _on_item_selected(id):
	print('selected: ', id)
