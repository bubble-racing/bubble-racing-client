extends Node2D
class_name GenericSelector

signal selected

var SelectorItem = preload("res://pages/title/selectors/selector_item/SelectorItem.tscn")
var fan_min: int
var fan_max: int
var fan: float
var stagger: int
var item_scale: float
var out_dist: int = 300
var in_dist: int = 75
var items = []
var action: String
var i: int
onready var timer = get_node('Timer')


func _ready():
	get_viewport().connect("size_changed", self, "_on_size_changed")
	_on_size_changed()
	

func add_item(id):
	var item = SelectorItem.instance()
	add_child(item)
	item.modulate.a = 0
	item.id = id
	item.connect('selected', self, "_on_item_selected")
	items.append(item)
	
	_on_size_changed()
	set_item_position(items.size() - 1, in_dist, 0)
	
	return item


func move_item_index(id, new_index):
	var item
	var old_index
	for index in len(items):
		item = items[index]
		if item.id == id:
			old_index = index
			break
	items.remove(old_index)
	items.insert(new_index, item)
	_on_size_changed()


func _on_size_changed():
	var size = get_viewport().get_visible_rect().size
	if items.size() < 4:
		fan_min = 50
		fan_max = 130
		stagger = 0
		item_scale = 1.25 * scale.x
		timer.wait_time = 0.2
	elif items.size() < 6:
		fan_min = 20
		fan_max = 160
		stagger = 0
		item_scale = 1.25 * scale.x
		timer.wait_time = 0.2
	elif size.x > 700:
		fan_min = 0
		fan_max = 180
		stagger = 0
		item_scale = 1 * scale.x
		timer.wait_time = 0.1
	else:
		fan_min = 50
		fan_max = 130
		stagger = 40
		item_scale = 0.75 * scale.x
		timer.wait_time = 0.1
	
	if items.size() > 1:
		fan = (fan_max - fan_min) / float((items.size() - 1))
	
	if action == 'activate':
		activate()
	if action == 'deactivate':
		deactivate()


func activate():
	i = 0
	action = 'activate'


func deactivate():
	i = 0
	action = 'deactivate'


func _on_Timer_timeout():
	if i >= items.size():
		return
	
	if action == 'activate':
		set_item_position(i, out_dist, 1)
		
	if action == 'deactivate':
		set_item_position(i, in_dist, 0)
		
	i += 1


func set_item_position(i, dist, alpha):
	var item = items[i]
	
	# spread the bubbles out
	var angle = i * fan + fan_min
	
	# stagger the bubbles if the screen is small
	var staggered_dist = dist
	if i % 2 == 0:
		staggered_dist -= stagger
	else:
		staggered_dist += stagger
	
	# position the bubbles
	item.target_position.x = cos(deg2rad(180 - angle)) * staggered_dist
	item.target_position.y = sin(deg2rad(180 - angle)) * staggered_dist
	item.target_alpha = alpha
	
	# scale the bubbles
	item.get_node('Bubble').initial_scale = Vector2(item_scale, item_scale)
	item.get_node('Ring').initial_scale = Vector2(item_scale * 0.25, item_scale * 0.25)


func _on_item_selected(id):
	emit_signal("selected", id)


func _clear():
	for item in items:
		remove_child(item)
	items = []
	i = 0
