extends Node2D


func _ready():
	$GenericSelector.add_item('solo', 'Solo', false, false)
	$GenericSelector.add_item('multiplayer', 'Multiplayer', false, true)
	$GenericSelector.add_item('about', 'About', false, false)
	$GenericSelector.activate()
	$GenericSelector.connect('selected', self, '_on_item_selected')
	Music.play_race()


func _on_Activate_pressed():
	$GenericSelector.activate()


func _on_Deactivate_pressed():
	$GenericSelector.deactivate()


func _on_item_selected(id):
	print('selected: ', id)
