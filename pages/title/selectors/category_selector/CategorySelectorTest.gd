extends Node2D


func _ready():
	Settings.persist.campaign = {
		'rise': {
			'one': 100000,
			'two': 200000,
			'three': 3,
			'four': 4,
			'five': 5
		},
		'flow': {
			'one': 100000,
			'two': 200000,
			'three': 3,
			'four': 4,
			'five': 5
		},
		'fire': {
			'one': 100000,
			'two': 200000,
			'three': 3,
			'four': 4,
			'five': 5
		},
		'bugs': {
			'one': 100000,
			'two': 200000
		}
	}
	$CategorySelector.activate()
	$CategorySelector.connect('selected', self, '_on_category_selected')
	Music.play_race() # need music to see focused item


func _on_Activate_pressed():
	$CategorySelector.activate()


func _on_Deactivate_pressed():
	$CategorySelector.deactivate()


func _on_category_selected(i):
	print('category selected: ', i)
