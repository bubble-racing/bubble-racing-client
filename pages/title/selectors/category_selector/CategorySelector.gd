extends GenericSelector

var rendered = false
var categories = ['rise', 'flow', 'fire', 'bugs', 'ghosts']


func _ready():
	pass


func activate():
	_render()
	.activate()


func is_locked(category_name) -> bool:
	var index = categories.find(category_name)
	var lock = false
	if index > 0:
		var previous_category_name = categories[index -1]
		var previous_level_times = Settings.persist.get('campaign', {}).get(previous_category_name, {})
		if len(previous_level_times.keys()) < 5:
			lock = true
	return lock


func _render():
	if rendered:
		return
	rendered = true
	
	for category_name in categories:
		var focus = false
		var title = category_name.capitalize()
		var index = categories.find(category_name)
		var lock = is_locked(category_name)
		
		if index < len(categories) - 1:
			var next_category_name = categories[index + 1]
			var next_lock = is_locked(next_category_name)
			focus = !lock && next_lock
		else:
			focus = !lock
			
		var item = add_item(category_name)
		item.set_label(title)
		item.set_focus(focus)
		item.set_lock(lock)
