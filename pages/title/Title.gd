extends Node2D


func _ready():
	$Center/LevelSelector.connect('selected', self, '_on_level_selected')
	$Center/CategorySelector.connect('selected', self, '_on_category_selected')
	$Center/RootSelector.connect('selected', self, '_on_root_selected')
	$BackButton/Button.connect('pressed', self, '_on_back_pressed')
	
	_set_mode(Settings.title_page)
	
	Music.play_title()
	
	$Background.bg_size = Vector2(1000, 1300)
	$Background.min_scale = Vector2(0.50, 0.40)
	$Background.lock_y = false
	$Background.offset_y = -100
	$Background.enable_auto_resize()


func _on_back_pressed():
	_set_mode('root')


func _set_mode(mode: String) -> void:
	# hide back button if at the root
	$BackButton.visible = mode != 'root'
	
	if mode == 'about' || mode == 'config' || mode == 'editor' || mode == 'bubble-picker' || mode == 'lobby':
		get_node("/root/Main").set_scene_by_name(mode)
		return
		
	Settings.title_page = mode
	
	$Center/LevelSelector.deactivate()
	$Center/RootSelector.deactivate()
	$Center/CategorySelector.deactivate()
	
	if mode == 'root':
		$Center/RootSelector.activate()
	elif mode == 'solo':
		var categories = Levels.levels.keys()
		if ($Center/CategorySelector.is_locked(categories[1])):
			_on_category_selected(categories[0])
		else:
			$Center/CategorySelector.activate()
	else:
		$Center/LevelSelector.render(mode)
		$Center/LevelSelector.activate()
	

func _on_level_selected(id):
	Settings.level_name = id
	Settings.mode = "single-player"
	get_node("/root/Main").set_scene_by_name('game')


func _on_category_selected(id):
	Settings.level_category = id
	_set_mode(id)
	var campaign = Settings.persist.get('campaign', {})
	if (!campaign.get(id)):
		_on_level_selected(Levels.levels[id].keys()[0])


func _on_root_selected(id):
	_set_mode(id)
