extends Node2D

var initial_scale = Vector2(0.55, 0.55)
var loudness = 0
var drag = 4


func _process(_delta):
	loudness = ((loudness * drag) + Music.loudness) / (drag + 1)
	scale = initial_scale + (initial_scale / 1.3 * max(loudness, Music.loudness) / 100)
	modulate.a = 0.3 + (0.5 * Music.loudness / 100)
