extends CenteredNode2D


func _ready():
	if OS.get_name() != 'iOS':
		$RichTextLabel.connect("meta_clicked", self, "meta_clicked")
		$RichTextLabel3.connect("meta_clicked", self, "meta_clicked")


func meta_clicked(meta):
	OS.shell_open(meta)
