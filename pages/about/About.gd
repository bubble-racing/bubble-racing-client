extends Node2D


func _ready():
	$BackButton/Button.connect('pressed', self, '_on_back_pressed')
	$Background.enable_auto_resize()


func _on_back_pressed():
	get_node("/root/Main").set_scene_by_name('title')
