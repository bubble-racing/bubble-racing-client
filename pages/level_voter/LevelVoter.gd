extends Node2D

var BubbleLocal = preload('../../bubble/BubbleLocal.tscn')
var Bubble = preload('../../bubble/Bubble.tscn')
var ClientWatcher = preload('../../networking/ClientWatcher.tscn')

var vote = ''
var seconds_remaining = 10
var levels
var random
var popular
var new
var client_watcher
var start_position = Vector2(0, -400)

func _ready():
	random = $VoteAreas/RandomOption
	popular = $VoteAreas/PopularOption
	new = $VoteAreas/NewOption
	$Timer.connect('timeout', self, '_timeout')
	$Countdown.connect('timeout', self, '_countdown')
	$ParallaxBackground/Background.enable_auto_resize()
	$CanvasLayer/BackButton/Button.connect('pressed', self, '_on_back_pressed')
	RPC.connect('level_vote_update', self, '_level_vote_update')
	RPC.connect('level_vote_result', self, '_level_vote_result')
	RPC.connect('update_nodes', self, '_update_nodes')
	
	# Send position updates to the server
	client_watcher = ClientWatcher.instance()
	add_child(client_watcher)
	client_watcher.connect('update', self, '_on_update')
	
	# Ask for level options
	RPC.rpc('get_level_options')
	RPC.connect('get_level_options_result', self, '_get_level_options_result')
	
	# Add bubbles for users
	ClientUsers.connect('add_user', self, '_add_user')
	ClientUsers.connect('remove_user', self, '_remove_user')
	for id in ClientUsers.users:
		var account = ClientUsers.users[id]
		_add_user(id, account)


func set_levels(levels):
	Logger.info('LevelVoter::set_levels')
	var random_level = Map2D.new()
	random_level.import_data(levels.random)
	random.set_level(random_level)
	
	var popular_level = Map2D.new()
	popular_level.import_data(levels.popular)
	popular.set_level(popular_level)
	
	var new_level = Map2D.new()
	new_level.import_data(levels.new)
	new.set_level(new_level)


func set_votes(votes: Dictionary) -> void:
	Logger.debug('LevelVoter::set_votes')
	random.set_remote_votes(0)
	popular.set_remote_votes(0)
	new.set_remote_votes(0)
	for level_id in votes:
		var vote_count = votes[level_id]
		var option = get_node('VoteAreas/' + String(level_id).capitalize() + 'Option')
		option.set_remote_votes(vote_count)


func get_vote() -> String:
	Logger.debug('LevelVoter::get_vote')
	if random.local_votes > 0:
		return 'random'
	elif popular.local_votes > 0:
		return 'popular'
	elif new.local_votes > 0:
		return 'new'
	else:
		return ''


func _timeout() -> void:
	Logger.debug('LevelVoter::_timeout')
	var new_vote = get_vote()
	if vote != new_vote:
		vote = new_vote
		Logger.debug('LevelVoter::_timeout::vote_on_level', vote)
		RPC.rpc('vote_on_level', vote)


func _countdown() -> void:
	if seconds_remaining > 0:
		seconds_remaining = seconds_remaining - 1
		var s = "" if (seconds_remaining == 1) else "s"
		$VoteAreas/CountdownLabel.text = "Vote! " + String(seconds_remaining) + " second" + s + " remain"


func _on_back_pressed():
	ClientConnection.close()
	get_node("/root/Main").set_scene_by_name('title')


func _get_level_options_result(_levels, time_left):
	Logger.info('LevelVoter::_get_level_options_result ')
	
	# Skip the level vote if the game has already started
	if time_left <= 0:
		_goto_game()
		return
	
	# Add levels to vote on
	levels = _levels
	set_levels(_levels)
	seconds_remaining = time_left


func _goto_game() -> void:
	get_node("/root/Main").set_scene_by_name('game')


func _level_vote_update(votes: Dictionary) -> void:
	Logger.debug('LevelVoter::_level_vote_update ', votes)
	set_votes(votes)


func _level_vote_result(category: String) -> void:
	Logger.info('LevelVoter::_level_vote_result', category)
	_goto_game()


func _add_user(id, account):
	var my_id = get_tree().get_network_unique_id()
	var bubble: Bubble
	
	if id == my_id:
		bubble = BubbleLocal.instance()
		bubble.control = 'local'
	else:
		bubble = Bubble.instance()
		bubble.control = 'remote'
	
	bubble.position = Vector2(start_position.x, start_position.y)
	bubble.name = String(id)
	bubble.min_y = -9999
	add_child(bubble)
	bubble.customize(account)
	
	if id == my_id:
		bubble.camera.current = true
		client_watcher.target = bubble
		if Settings.persist.get('autopilot', false):
			var rng = RandomNumberGenerator.new()
			rng.randomize()
			bubble.target_position = Vector2(rng.randi_range($VoteAreas.position.x - 580, $VoteAreas.position.x + 580), rng.randi_range(200, 500))


func _remove_user(id):
	var bubble = get_node_or_null(String(id))
	if bubble:
		bubble.queue_free()


func _update_nodes(updates):
	Logger.debug('LevelVoter::_update_nodes', updates)
	for key in updates.keys():
		var update: Array = updates[key]
		var node: Seeker = get_node(String(key))
		if node:
			node.nudge(update)


func _on_update(update: Array):
	RPC.rpc_id(1, 'update_player_pos', update)
