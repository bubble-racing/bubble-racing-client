extends Node2D

var LevelGenerator = preload("../../../level_generator/LevelGenerator.gd")
var level_generator = LevelGenerator.new()


func _ready():
	var levels = {
		'random': _create_random_map().export_data(),
		'popular': _create_random_map().export_data(),
		'new':  _create_random_map().export_data()
	}
	$LevelVoter.set_levels(levels)


func _create_random_map():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var level = level_generator.generate({
		width = rng.randi_range(5, 10) * 3,
		height = rng.randi_range(10, 20) * 3,
		phosphorus = rng.randi_range(0, 12),
		speed = rng.randi_range(0, 10),
		current_up = rng.randi_range(0, 8),
		current_right = rng.randi_range(0, 2),
		current_down = rng.randi_range(0, 2),
		current_left = rng.randi_range(0, 2),
		debris = rng.randi_range(0, 12),
		vortex = rng.randi_range(0, 12),
		ghost = rng.randi_range(0, 12),
		oil = rng.randi_range(0, 12),
		blockers = rng.randi_range(0, 10),
		diver = max(rng.randi_range(-1, 1), 0),
		turtle = max(rng.randi_range(-2, 1), 0)
	})
	level.category = 'Random'
	level.title = NameGenerator.generate()
	return level

