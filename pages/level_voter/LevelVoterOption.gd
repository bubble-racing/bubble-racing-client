extends Node2D

var max_size = Vector2(15, 7)
var initial_position
var local_votes = 0
var remote_votes = 0


func _ready():
	initial_position = Vector2(position.x, position.y)
	$BG.visible = false
	$Area2D.connect('body_entered', self, '_body_entered')
	$Area2D.connect('body_exited', self, '_body_exited')
	set_remote_votes(0)


func _body_entered(body: Node) -> void:
	if (body.get_class() == 'BubbleLocal'):
		$BG.visible = true
		local_votes = 1


func _body_exited(body: Node) -> void:
	if (body.get_class() == 'BubbleLocal'):
		$BG.visible = false
		local_votes = 0


func set_level(level: Map2D):
	Logger.info('LevelVoterOption::set_level', level.category)

	# Fill in lables
	$CategoryLabel.text = '[' + level.category + ']'
	$UserLabel.text = level.user_nickname
	
	# Prune the level down to size
	var pruned_map = Map2D.new()
	for x in level.data:
		for y in level.data[x]:
			if x < max_size.x && y < max_size.y:
				pruned_map.set_tile(x, y, level.data[x][y])
	pruned_map.calc_bounds()
	
	# Render the level
	$LevelRenderer.add_dots = false
	$LevelRenderer.mode = 'icon'
	$LevelRenderer.render_map(pruned_map, 'remote')
	
	# Center the level
	var level_size = Vector2(pruned_map.max_pos.x - pruned_map.min_pos.x, pruned_map.max_pos.y - pruned_map.min_pos.y)
	$LevelRenderer.position.x += (max_size.x - level_size.x) * 200 * $LevelRenderer.scale.x * 0.5
	$LevelRenderer.position.y += (max_size.y - level_size.y) * 200 * $LevelRenderer.scale.y * 0.5


func set_remote_votes(count: int) -> void:
	remote_votes = count
	$VoteLabel.text = String(remote_votes) + " Vote"
	if count != 1:
		$VoteLabel.text += 's'
	
	
