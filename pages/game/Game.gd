extends Node2D

signal begin

var LevelGenerator = preload('../../level_generator/LevelGenerator.gd')
var BubbleLocal = preload('../../bubble/BubbleLocal.tscn')
var LevelRenderer = preload('../../level_renderer/LevelRenderer.tscn')
var FinishAnim = preload('res://pages/game/finish_anim/FinishAnim.tscn')
var FinishPanel = preload('res://pages/game/Finish.tscn')

var level_generator = LevelGenerator.new()
var level_renderer = LevelRenderer.instance()
var start_time = 0
var local_bubble: BubbleLocal
var map: Map2D
var map_data
var control = 'local'
onready var canvasModulate1 = get_node('CanvasModulate')
onready var canvasModulate2 = get_node('ParallaxBackground/CanvasModulate')
onready var background = get_node('ParallaxBackground/Background')
onready var sunset = get_node('ParallaxBackground/Sunset')


func _ready():
	add_child(level_renderer)
	$BrightnessTimer.connect('timeout', self, '_update_brightness')
	$AutopilotTimer.connect('timeout', self, '_on_autopilot_timeout')
	$CanvasLayer/BackButton/Button.connect('pressed', self, '_on_back_pressed')
	$CanvasLayer/AutopilotButton/Button.connect('pressed', self, '_on_autopilot_pressed')
	get_viewport().connect('size_changed', self, '_on_size_changed')
	background.enable_auto_resize()
	background.simplify()
	_on_size_changed()
	
	if '--server' in OS.get_cmdline_args():
		Logger.info('Game::_ready - Server Setup')
	else:
		Logger.info('Game::_ready - Client Setup')
		Logger.info('Game::_ready - Level Name: ', Settings.level_name)
		Logger.info('Game::_ready - Mode: ', Settings.mode)
		if (Settings.mode == 'single-player' || Settings.mode == 'editor-test'):
			if Settings.mode == 'editor-test':
				map = Map2D.new()
				map.set_data(Settings.persist.wip_level.map)
			elif Settings.mode == 'single-player':
				var level = Levels.levels[Settings.level_category][Settings.level_name]
				if level.get('type') == 'maze':
					map = level_generator.generate(level.settings)
				else:
					map = Map2D.new()
					map.set_data(level.map)
				map_data = map.data.duplicate(true)
				map = KeyDropper.drop(map)
			render_level(map)
		if (Settings.mode == 'multi-player'):
			control = 'remote'
			$ClientSetup.init()


func _on_size_changed():
	var window_size = get_viewport().get_visible_rect().size
	$CanvasLayer/AutopilotButton.position = Vector2(window_size.x - 85, 85)


func render_level(_map: Map2D):
	map = _map
	var window_size = get_viewport().get_visible_rect().size
	var level_size = (map.max_pos - map.min_pos) * Settings.tile_size
	var scale_xy = window_size / level_size
	var scale_both = min(scale_xy.x, scale_xy.y)
	var level_center = (map.max_pos + map.min_pos) / 2 * Settings.tile_size
	$Camera.position = level_center
	$Camera.zoom = Vector2(1 / scale_both, 1 / scale_both)
	level_renderer.add_dots = true
	level_renderer.render_map(map, control)
	level_renderer.connect('render_complete', self, '_on_render_level_complete')


func _on_render_level_complete():
	add_local_bubble()
	local_bubble.zoom_from($Camera.zoom, $Camera.position)
	$Camera.current = false
	begin()


func _process(delta):
	sunset.scale = background.scale


func add_local_bubble():
	var id = get_tree().get_network_unique_id()
	var settings = MyBubbles.get_current_bubble()
	local_bubble = level_renderer.add_bubble(String(id), 'local')
	local_bubble.customize(settings)
	local_bubble.connect('finished', self, '_on_local_finished')
	local_bubble.connect('removed', self, '_leave')


func _on_local_finished():
	var elapsed_ms = OS.get_ticks_msec() - start_time
	var screen_size = get_viewport().get_visible_rect().size
	$CanvasLayer/AutopilotButton.visible = false
	
	if Settings.mode == 'single-player':
		Settings.persist.campaign = Settings.persist.get('campaign', {})
		var campaign = Settings.persist.campaign
		var best_time = campaign.get(Settings.level_category, {}).get(Settings.level_name, 99999999)
		if best_time > elapsed_ms:
			campaign[Settings.level_category] = campaign.get(Settings.level_category, {})
			campaign[Settings.level_category][Settings.level_name] = elapsed_ms
		Settings.persist.campaign = campaign
	
	var pearl_count = Pickups.get_stowed('pearls')
	var key_count = Pickups.get_stowed('keys')
	Pickups.stow('pearls', -pearl_count)
	
	var finish_panel = FinishPanel.instance()
	finish_panel.position = Vector2(screen_size.x, 0)
	finish_panel.init(elapsed_ms, pearl_count, key_count)
	$CanvasLayer.add_child(finish_panel)
	
	var finish_anim = FinishAnim.instance()
	finish_anim.connect('selected', self, '_on_endgame_selector')
	finish_anim.position = screen_size / 2
	finish_anim.pearl_count = pearl_count
	finish_anim.bubble_data = MyBubbles.get_current_bubble()
	$CanvasLayer.add_child(finish_anim)
	
	$AutopilotTimer.start()
	
	if Settings.temp.get('bubble_in_tow', null):
		MyBubbles.add_bubble(Settings.temp.bubble_in_tow)
		Settings.temp.bubble_in_tow = null
	
	Pickups.commit()
	Settings.save_game()


func _on_endgame_selector(option: String):
	if option == 'menu':
		_on_back_pressed()
	if option == 'bubble-picker':
		get_node('/root/Main').set_scene_by_name('bubble-picker')
	if option == 'featured-bubble':
		get_node('/root/Main').set_scene_by_name('featured-bubble')
	if option == 'editor':
		Settings.mode = 'editor-test'
		Settings.persist.wip_level = Settings.persist.get('wip_level', {})
		Settings.persist.wip_level.map = map_data.duplicate(true)
		Settings.persist.wip_level.title = Settings.level_name
		_on_back_pressed()
	if option == 'next':
		if (Settings.mode == 'single-player'):
			var level_category = Settings.level_category
			var level_name = Settings.level_name
			var categories = Levels.levels.keys()
			var levels = Levels.levels[level_category].keys()
			var category_index = categories.find(level_category)
			var level_index = levels.find(level_name)
			if level_index < len(levels) - 1:
				Settings.level_name = levels[level_index + 1]
				get_node('/root/Main').set_scene_by_name('game')
			elif category_index < len(categories) - 1:
				Settings.level_category = categories[category_index + 1]
				Settings.level_name = Levels.levels[Settings.level_category].keys()[0]
				get_node('/root/Main').set_scene_by_name('game')
			else:
				get_node('/root/Main').set_scene_by_name('game')
		elif (Settings.mode == 'multi-player'):
			get_node('/root/Main').set_scene_by_name('lobby')
		elif (Settings.mode == 'editor-test'):
			_leave()


func begin():
	Logger.info('Game::begin')
	RPC.rpc_id(1, 'start_game')
	start_time = OS.get_ticks_msec()
	Music.play_race()
	if Settings.persist.get('autopilot', false):
		local_bubble.activate_autopilot(map)
	emit_signal('begin')


func _on_back_pressed():
	_leave()


func _on_autopilot_pressed():
	if !local_bubble:
		return
	if !map:
		return
	if !Settings.persist.autopilot:
		if local_bubble.autopilot:
			local_bubble.autopilot.resume()
		else:
			local_bubble.activate_autopilot(map)
	else:
		if local_bubble.autopilot:
			local_bubble.autopilot.pause()
			local_bubble.target_position = local_bubble.position


func _leave():
	print('Game::_leave ', Settings.mode)
	local_bubble = null
	Pickups.clear()
	ClientConnection.close()
	if Settings.mode == 'editor-test':
		get_node('/root/Main').set_scene_by_name('editor')
	elif Settings.mode == 'single-player':
		var category_list = Levels.levels.keys()
		var category_index = category_list.find(Settings.level_category)
		var level_list = Levels.levels[Settings.level_category].keys()
		var level_index = level_list.find(Settings.level_name)
		if level_index >= len(level_list) - 1 && category_index < len(category_list) - 1:
			var next_category = category_list[category_index + 1]
			if len(Settings.persist.campaign.get(next_category, {}).keys()) == 0:
				Settings.level_category = next_category
				Settings.title_page = next_category
		get_node('/root/Main').set_scene_by_name('title')
	elif Settings.mode == 'multi-player':
		Settings.title_page = 'root'
		get_tree().network_peer = null
		get_node('/root/Main').set_scene_by_name('title')


func _update_brightness():
	if !local_bubble || !map:
		return
	if local_bubble.oil_countdown > 0:
		_set_brightness(0.1)
	else:
		var min_y = 0
		var max_y = (map.max_pos.y - map.min_pos.y) * Settings.tile_size
		var y = local_bubble.position.y - (map.min_pos.y * Settings.tile_size)
		if y < min_y:
			y = min_y
		if y > max_y:
			y = max_y
		var brightness = 1.3 - (y / max_y)
		_set_brightness(min(brightness, 1.0))
		
		
func _set_brightness(value):
	canvasModulate1.color.r = value
	canvasModulate1.color.g = value
	canvasModulate1.color.b = value
	canvasModulate2.color.r = value
	canvasModulate2.color.g = value
	canvasModulate2.color.b = value


func _on_autopilot_timeout():
	if Settings.persist.get('autopilot', false):
		_on_endgame_selector('next')
