extends GenericSelector

# var ChestIcon = preload("res://tiles/chest/ChestIcon.tscn")


func _ready():
	if Settings.mode != 'editor-test':
		add_item('editor').set_label('Editor')
		add_item('bubble-picker').set_label('Bubbles')
		# $FeaturedBubbleTimer.connect("timeout", self, "_on_featured_bubble")		
	add_item('next').set_label('Next').set_focus(true)


# func _on_featured_bubble():
#	if FeaturedBubble.bubble && !FeaturedBubble.is_owned():
#		add_item('featured-bubble').set_label('?').set_label_color(Color(1.0, 1.0, 1.0))
#		move_item_index('featured-bubble', 2)
#		
#		var button = get_children()[get_child_count()-1]
#		
#		var chest_icon = ChestIcon.instance()
#		button.add_child(chest_icon)
#		chest_icon.scale = Vector2(0.65, 0.65)
#		button.move_child(chest_icon, 3)
