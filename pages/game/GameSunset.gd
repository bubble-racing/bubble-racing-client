extends Sprite


func _ready():
	var _result = get_viewport().connect("size_changed", self, "_on_size_changed")
	_on_size_changed()


func _process(_delta):
	var mod =  ((Music.loudness / 15) / 3.5) + 1
	modulate.r = mod
	modulate.g = mod
	modulate.b = mod


func _on_size_changed():
	var size = get_viewport().get_visible_rect().size
	var textureSize = texture.get_size()
	var new_scale = size / textureSize
	scale.x = new_scale.x * 1.5
	# set_scale(scale)
	# position = size / 2
