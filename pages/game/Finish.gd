extends Node2D


func _ready():
	pass


func init(elapsed_ms: int, pearl_count: int, key_count: int):
	$Holder/TimeValue.text = TimeFormatter.format(round(elapsed_ms / 1000))
	$Holder/PearlValue.text = String(pearl_count)
	$Holder/KeysValue.text = String(key_count)
