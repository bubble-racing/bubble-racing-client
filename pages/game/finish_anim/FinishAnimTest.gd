extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	$FinishAnim.bubble_data = {
		'nature': {
			'speed': 1,
			'tough': 1,
			'skill': 1
		},
		'nurture': {
			'speed': 1,
			'tough': 2,
			'skill': 3
		},
		'pearls': 0
	}
	$BubbleLocal.finish()
	$BubbleLocal.target_position = $FinishAnim.position

