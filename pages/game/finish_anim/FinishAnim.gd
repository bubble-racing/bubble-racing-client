extends Node2D

signal selected
onready var animator = get_node('Animator')
onready var selector = get_node('PostgameSelector')
onready var pearl_holder = get_node('PearlHolder')
onready var pickup_sound = get_node('PickupSound')
onready var pickup_sound_timer = get_node('PickupSoundTimer')
onready var exp_meter = get_node('ExpMeter')
var pearl_count = 200
var bubble_data: Dictionary
var ThrownPearl = preload('res://pages/game/finish_anim/ThrownPearl.tscn')
var rng = RandomNumberGenerator.new()


func _ready():
	animator.play('finish')
	selector.connect('selected', self, '_option_selected')
	pickup_sound_timer.connect('timeout', self, '_on_pickup_sound_timer')


func throw_pearls():
	exp_meter.render(bubble_data)
	for i in pearl_count:
		var pearl = ThrownPearl.instance()
		pearl.target_position = Vector2(rng.randi_range(75, 350), 0).rotated(rng.randf_range(0, PI*2))
		pearl_holder.add_child(pearl)


func count_pearls():
	pickup_sound_timer.start()
	for pearl in pearl_holder.get_children():
		pearl.delay_target_position(rng.randf_range(0, 1.5), Vector2(0, -50))


func consume_pearls():
	exp_meter.win_pearls(pearl_count)


func open_menu():
	pickup_sound_timer.stop()
	selector.activate()


func victor_dance():
	pass


func _option_selected(option: String):
	emit_signal('selected', option)


func _on_pickup_sound_timer():
	if Settings.persist.sound_effects:
		pickup_sound.play()
