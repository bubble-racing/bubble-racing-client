extends Node2D

onready var timer = get_node('Timer')
var target_position = Vector2(0, 0)
var delayed_target_position: Vector2
var remove_when_done = false


func _process(delta):
	var dist = (target_position - position) * 4.0 * delta
	position += dist
	if (remove_when_done && position.distance_to(target_position) < 5):
		queue_free()


func delay_target_position(delay: float, _position: Vector2):
	delayed_target_position = _position
	timer.wait_time = delay
	timer.connect('timeout', self, '_timeout')
	timer.start()


func _timeout():
	remove_when_done = true
	target_position = delayed_target_position
