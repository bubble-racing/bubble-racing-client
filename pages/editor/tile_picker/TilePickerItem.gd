extends Node2D

signal pressed

var tile_id


func _ready():
	$square.connect('pressed', self, '_on_pressed')
	
	
func select():
	$square.self_modulate.a = 0.5


func deselect():
	$square.self_modulate.a = 0


func _on_pressed():
	emit_signal('pressed')
