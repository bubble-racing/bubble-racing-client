extends Node2D

signal selected

var EmptyIcon = preload("../../../tiles/empty/EmptyIcon.tscn")
var TilePickerItem = preload('./TilePickerItem.tscn')
var TileCreator = preload('../../../tiles/TileCreator.gd')

var tile_creator = TileCreator.new()
var spacing = Vector2(75, 75)
var columns = 9
var item_count = 0
var items = []
var active = false


func _ready():
	add_item(TileTypes.EMPTY)
	add_item(TileTypes.WALL)
	add_item(TileTypes.PHOSPHORUS)
	add_item(TileTypes.SPEED)
	add_item(TileTypes.CURRENT_UP)
	add_item(TileTypes.CURRENT_RIGHT)
	add_item(TileTypes.CURRENT_DOWN)
	add_item(TileTypes.CURRENT_LEFT)
	add_item(TileTypes.DEBRIS)
	add_item(TileTypes.VORTEX)
	add_item(TileTypes.GHOST)
	add_item(TileTypes.OIL)
	add_item(TileTypes.BUBBLE)
	add_item(TileTypes.TURTLE)
	add_item(TileTypes.DIVER)
	add_item(TileTypes.CHERRIES)
	add_item(TileTypes.STARFISH)
	_select_item(TileTypes.WALL)


func add_item(tile_id):
	var item = TilePickerItem.instance()
	item.position.x = (len(items) % columns) * spacing.x + (spacing.x / 2)
	item.position.y = floor(len(items) / columns) * spacing.y + (spacing.y / 2)
	item.tile_id = tile_id
	item.scale = Vector2(0.75, 0.75)
	items.append(item)
	item.connect('pressed', self, "_on_item_selected", [tile_id])
	item.get_node('square').connect("button_down", self, "_touch_start")
	item.get_node('square').connect("button_up", self, "_touch_stop")
	add_child(item)
	
	# draw an icon in the button
	var icon = tile_creator.create_icon(tile_id)
	if !icon:
		icon = EmptyIcon.instance()
	icon.scale = Vector2(0.4, 0.4)
	item.add_child(icon)

	# scale the background to fit the button
	$Background.scale.x = min(len(items), columns) * spacing.x / 100
	$Background.scale.y = (len(items) / columns + 1) * spacing.y / 100


func _touch_start():
	active = true


func _touch_stop():
	active = false


func _on_item_selected(id):
	_select_item(id)


func _deselect_all_items():
	for item in items:
		item.deselect()


func _select_item(id):
	for item in items:
		if item.tile_id == id:
			item.select()
		else:
			item.deselect()
	emit_signal('selected', id)


func _exit_tree():
	items = []
