extends Node2D

var selected_tile_type = 1
var map = Map2D.new()
var scroll_speed = -700
var active = false
var key_control_position = Vector2(0, 0)
var top_ui_y = 0
var top_ui_width = 140


func _ready():
	top_ui_y = $BackButton.position.y
	
	$TilePicker.connect("selected", self, "_on_tile_picked")
	$DrawArea.connect("button_down", self, "_on_draw_start")
	$DrawArea.connect("button_up", self, "_on_draw_stop")
	$BackButton/Button.connect("pressed", self, "_on_back_pressed")
	$SaveButton/Button.connect("pressed", self, "_on_save_pressed")
	$LoadButton/LoadButton/Button.connect("pressed", self, "_on_load_pressed")
	$TestButton/TestButton/Button.connect("pressed", self, "_on_test_pressed")
	$NewButton/NewButton/Button.connect("pressed", self, "_on_new_pressed")
	
	# Play title music
	Music.play_title()
	
	# Scale background to fit screen
	$Background.enable_auto_resize()
	
	# Auto resize things
	get_viewport().connect("size_changed", self, "_on_size_changed")
	_on_size_changed()
	
	# Create a new wip level if none exist
	if !Settings.persist.has('wip_level'):
		_on_new_pressed()
	
	# Render an existing work in progress
	map.set_data(Settings.persist.wip_level.map)
	$LevelRenderer.mode = 'icon' # Draw icons instead of real game elements
	$LevelRenderer.render_map(map, 'remote')


func _process(delta):
	if active && Input.is_mouse_button_pressed(1): # Left click
		var position = $LevelRenderer.get_local_mouse_position() / $LevelRenderer.tile_size
		position.x = round(position.x)
		position.y = round(position.y)
		
		var tile = map.get_tile(position.x, position.y)
		if tile != selected_tile_type:
			map.set_tile(position.x, position.y, selected_tile_type)
			$LevelRenderer.swap_tile(selected_tile_type, position.x, position.y)
	
	# Scroll the map via the joystick
	$LevelRenderer.position += $Joystick.control_position * scroll_speed * delta

	# Scroll via keys
	key_control_position.x = Input.get_axis("ui_left", "ui_right")
	key_control_position.y = Input.get_axis("ui_up", "ui_down")
	$LevelRenderer.position += key_control_position * scroll_speed * delta


func _on_tile_picked(tile_type):
	selected_tile_type = tile_type


func _on_draw_start():
	active = true


func _on_draw_stop():
	active = false


func _on_size_changed():
	var window_size = get_viewport().get_visible_rect().size
	var safe_rect = OS.get_window_safe_area()
	var window_scale = window_size.y / get_tree().root.size.y
	var ui_y = top_ui_y + safe_rect.position.y * window_scale
	
	$BackButton.position.y = ui_y
	
	$TestButton.position.y = ui_y
	$TestButton.position = Vector2(window_size.x + 50 - top_ui_width * 1, ui_y)
	$LoadButton.position = Vector2(window_size.x + 50 - top_ui_width * 2, ui_y)
	$NewButton.position = Vector2(window_size.x + 50 - top_ui_width * 3, ui_y)
	$Joystick.position = window_size - Vector2(105, 105)
	$TilePicker.position = Vector2(30, window_size.y - 180)
	$DrawArea.rect_size = Vector2(window_size)
	
	if window_size.x < 900:
		$Joystick.position.y = window_size.y - 290


func _on_back_pressed():
	goto_page('title')


func _on_save_pressed():
	goto_page('level-save')


func _on_load_pressed():
	goto_page('level-load')


func _on_test_pressed():
	Settings.mode = "editor-test"
	goto_page('game')


func goto_page(page_id):
	map.transform_to_zero()
	Settings.persist.wip_level.map = map.data
	Settings.save_game()
	get_node("/root/Main").set_scene_by_name(page_id)


func _on_new_pressed():
	# Save existing map
	if (len(map.data.keys()) > 1):
		var level = Settings.persist.get('wip_level', {})
		level.map = map.data
		LevelStorage.store(level)

		# Print	
		print('----------------')
		print(to_json(level))
		print('----------------')
	
	# Clear existing map
	map.clear()
	$LevelRenderer.clear()
	
	# Create a new map
	Settings.persist.wip_level = {
		"title": NameGenerator.generate(),
		"published": false,
		"map": {}
	}
