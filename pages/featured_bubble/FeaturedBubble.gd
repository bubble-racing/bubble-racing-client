extends Node2D

var Face = preload("res://face/Face.tscn")
var bubble
var face
var loading = false


func _ready():
	$Center/PriceSelector.connect('selected', self, '_on_price_selected')
	$BackButton/Button.connect('pressed', self, '_on_back_pressed')
	get_viewport().connect('size_changed', self, '_on_size_changed')
	$Timer.connect('timeout', self, '_render')
	
	$Background.bg_size = Vector2(1000, 1300)
	$Background.min_scale = Vector2(0.50, 0.40)
	$Background.lock_y = false
	$Background.offset_y = -100
	$Background.enable_auto_resize()
	
	$Center/PriceSelector.activate()
	
	_on_size_changed()
	_render()


func _render():
	$Center/TimeLeft.text = "Time Left: " + TimeFormatter.format(FeaturedBubble.get_time_left())
	
	if !FeaturedBubble.bubble:
		return
	
	if bubble && FeaturedBubble.bubble.id == bubble.id:
		return
	
	if !face:
		face = Face.instance()
		$Center/FaceHolder.add_child(face)
		face.auto_follow_mouse = true
		
	bubble = FeaturedBubble.bubble
	face.customize(bubble)
	
	
func _on_back_pressed():
	get_node("/root/Main").set_scene_by_name('title')


func _on_price_selected(id):
	if loading:
		return
	loading = true
	$Center/PriceSelector.deactivate()
	
	if id == '1':
		MyBubbles.add_bubble(FeaturedBubble.bubble)
		get_node("/root/Main").set_scene_by_name('bubble-picker')
	else:
		var location = JavaScript.get_interface("location")
		if location:
			location.assign(BaseURL.get_base_url() + "/api/stripe/create-checkout-session?priceId=" + id)


func _on_size_changed():
	var window_size = get_viewport().get_visible_rect().size
	$Center.position = window_size / 2
	$Center.position.x = round($Center.position.x)
	$Center.position.y = round($Center.position.y)
