extends Node2D

var rng = RandomNumberGenerator.new()


# Called when the node enters the scene tree for the first time.
func _ready():
	rng.randomize()
	$PageTransition/AnimationPlayer.connect("animation_finished", self, '_on_animation_finished')


func _on_TransitionIn_pressed():
	$PageTransition/AnimationPlayer.play("transition-in")


func _on_animation_finished(animation_name):
	if animation_name == "transition-in":
		$BG.modulate.r = rng.randf()
		$BG.modulate.g = rng.randf()
		$BG.modulate.b = rng.randf()
		$PageTransition/AnimationPlayer.play("transition-out")
