extends Node2D


func _ready():
	get_viewport().connect("size_changed", self, "_on_size_changed")
	_on_size_changed()


func _on_size_changed():
	var window_size = get_viewport().get_visible_rect().size
	var bg_size = $Transition.texture.get_size()
	scale = Vector2(window_size.x / bg_size.x, window_size.y / bg_size.y * 2)
	position.x = 0
	position.y = 0
