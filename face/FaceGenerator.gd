class_name FaceGenerator

static func generate():
	var base_count = 9
	var hat_count = 9
	var eyes_count = 9
	var pupil_count = 9
	var mouth_count = 9
	var ear_count = 9
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	
	var face = {
		'base': {
			'id': rng.randi_range(1, base_count),
			'color': Color(
				rng.randf_range(0.2, 1),
				rng.randf_range(0.2, 1),
				rng.randf_range(0.2, 1),
				1
			).to_html()
		},
		'eyes': {
			'id': rng.randi_range(1, eyes_count),
			'color': Color(
				rng.randf_range(0.85, 1),
				rng.randf_range(0.85, 1),
				rng.randf_range(0.85, 1),
				1
			).to_html()
		},
		'pupils': {
			'id': rng.randi_range(1, pupil_count),
			'color': Color(
				rng.randf_range(0, 0.85),
				rng.randf_range(0, 0.85),
				rng.randf_range(0, 0.85),
				1
			).to_html()
		},
		'mouth': {
			'id': rng.randi_range(1, mouth_count),
			'color': Color(
				rng.randf_range(0.3, 1),
				rng.randf_range(0.3, 1),
				rng.randf_range(0.3, 1),
				1
			).to_html()
		},
		'ear': {
			'id': rng.randi_range(1, ear_count),
			'color': Color(
				rng.randf_range(0.5, 1),
				rng.randf_range(0.5, 1),
				rng.randf_range(0.5, 1),
				1
			).to_html()
		},
		'hat': {
			'id': rng.randi_range(1, hat_count),
			'color': Color(
				rng.randf_range(0, 1),
				rng.randf_range(0, 1),
				rng.randf_range(0, 1),
				1
			).to_html()
		}
	}
	
	return face
