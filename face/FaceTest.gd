extends Node2D

var rows = 9
var columns = 5
var Face = preload("res://face/Face.tscn")
var FaceGenerator = preload("./FaceGenerator.gd")


func _ready():
	for row in rows:
		for column in columns:
			var face = Face.instance()
			face.position = Vector2(row * 140 + 100, column * 140 + 100)
			add_child(face)
			face.customize(FaceGenerator.generate_premium())


func _process(delta):
	var target_position = get_global_mouse_position()
	for face in get_children():
		face.target_position = target_position - face.position
