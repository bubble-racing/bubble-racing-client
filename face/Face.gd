extends Node2D


var max_dist = 250
var pupil_range = 25
var pupil_count = 2
var pupil_dist = 30
var eye_range = 15
var mouth_range = 10
var ear_range = 5
var hat_range = 5
var target_position = Vector2(0, 0)
var pupil_start
var eye_start
var mouth_start
var ear_start
var hat_start
var swim = Vector2(1, 0)
var base_swim_speed = 13
var flip_threshold = 10
var data: Dictionary
var auto_follow_mouse = false
var start_position: Vector2

var nudges = {
	'hat': {
		2: Vector2(0, -10),
		3: Vector2(25, 0),
		4: Vector2(-25, -10),
		5: Vector2(-5, -15), # king
		7: Vector2(-15, 23),
		8: Vector2(23, 0)
	},
	'eyes': {
		2: Vector2(0, -5),
		5: Vector2(0, -5), # king
		7: Vector2(0, -5)
	},
	'ear': {
		3: Vector2(15, -15),
		5: Vector2(10, 25), # king
		6: Vector2(-5, 0), # queen
		8: Vector2(-20, 30), # heart tail
		9: Vector2(40, 0) # bat wings
	},
	'mouth': {
		4: Vector2(0, 5),
		6: Vector2(5, 0) # queen
	}
}

func _ready():
	pupil_start = $Pupils.position
	eye_start = $Eyes.position
	mouth_start = $Mouth.position
	ear_start = $Ear.position
	hat_start = $Hat.position
	customize(FaceGenerator.generate())
	start_position = position


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	if auto_follow_mouse:
		target_position = get_parent().get_local_mouse_position()
		position = start_position
	
	# Flip to face target position
	if scale.x == -1 && target_position.x > flip_threshold:
		scale.x = 1
	elif scale.x == 1 && target_position.x < -flip_threshold:
		scale.x = -1
	
	# Flip fancy pupils to face target position
	if data.pupils.id == 101:
		if target_position.y > flip_threshold:
			$Pupils.scale.y = 1
		elif target_position.y < -flip_threshold:
			$Pupils.scale.y = -1
	
	# Custom part shenanigans
	var _pupil_range = pupil_range
	if data != null:
		if data.eyes.id == 5: # king
			_pupil_range = Vector2(25, 19)
		elif data.eyes.id == 6: # queen
			_pupil_range = Vector2(22, 21)
		elif data.eyes.id == 101:
			_pupil_range = Vector2(22, 21)
	
	# Move face features towards target position
	var perc = min(1, target_position.length() / max_dist)
	var norm = target_position.normalized()
	norm.x *= scale.x
	$Mouth.position = norm * perc * mouth_range + mouth_start
	$Eyes.position = norm * perc * eye_range + eye_start
	$Pupils.position = norm * perc * _pupil_range + pupil_start
	$Ear.position = norm * perc * ear_range + ear_start
	$Hat.position = norm * perc * hat_range + hat_start
	
	# "swim" speed determined by distance to target
	swim = swim.rotated(delta * base_swim_speed * perc).normalized()
	scale.y = 1 + swim.y / 10


func customize(new_data):
	data = new_data
	
	if data.eyes.id == 4:
		pupil_count = 1
	else:
		pupil_count = 2
	
	if data.eyes.id == 7:
		pupil_dist = 40
	elif data.eyes.id == 8:
		pupil_dist = 55
	else:
		pupil_dist = 30
	
	# stop spikes from showing up on top of hats
	if data.ear.id == 3:
		move_child($Ear, 1)
	elif data.ear.id == 8 || data.ear.id == 9: # tail and wings should be below hat and base
		move_child($Ear, 0)
	else:
		move_child($Ear, 3)
	
	var parts = ['Base', 'Ear', 'Hat', 'Mouth', 'Eyes', 'Pupils']
	for part in parts:
		clear_part(part)
		if part != 'Pupils' || pupil_count == 1:
			add_part(part, data)
		else:
			add_part(part, data, Vector2(-pupil_dist / 2, 0))
			add_part(part, data, Vector2(pupil_dist / 2, 0))


func clear_part(part_name: String):
	var holder = get_node(part_name)
	for child in holder.get_children():
		holder.remove_child(child)


func add_part(part_name, data, base_position: Vector2 = Vector2(0, 0)):
	var part_data = data[part_name.to_lower()]
	var holder = get_node(part_name)
	var nudge_group = nudges.get(part_name.to_lower(), {})
	var nudge = nudge_group.get(int(part_data.id), Vector2(0, 0))
	var part_position = base_position + nudge
	
	var color = Sprite.new()
	color.set_texture(load("res://face/%s/%s-%s-color.png" % [part_name.to_lower(), part_name.to_lower(), part_data.id]))
	color.modulate = Color(part_data.color)
	color.position = part_position
	holder.add_child(color)
	
	var lines = Sprite.new()
	lines.texture = load("res://face/%s/%s-%s-lines.png" % [part_name.to_lower(), part_name.to_lower(), part_data.id])
	lines.position = part_position
	holder.add_child(lines)

