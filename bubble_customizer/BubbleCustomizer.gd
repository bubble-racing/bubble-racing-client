extends Node2D

var bubble_position
var bubble_map = {}
var bubble_list = []
var i = 0
var selected_bubble_id: String


func _ready():
	bubble_position = $BubbleHolder/Bubble.position
	$BubbleHolder/Bubble/Light2D.visible = false
	$BubbleHolder/Bubble/Name.visible = false
	$Upgrader.visible = true
	$Customizer.visible = false
	$Upgrader/RightButton.connect("pressed", self, "_on_right_pressed")
	$Upgrader/LeftButton.connect("pressed", self, "_on_left_pressed")
	$Upgrader/EditButton.connect("pressed", self, "_on_edit_pressed")
	$Customizer/SaveButton.connect("pressed", self, "_on_save_pressed")
	$Customizer/BaseCustomizer.connect("changed", self, "_on_face_changed")
	$Customizer/EarCustomizer.connect("changed", self, "_on_face_changed")
	$Customizer/EyeCustomizer.connect("changed", self, "_on_face_changed")
	$Customizer/HatCustomizer.connect("changed", self, "_on_face_changed")
	$Customizer/MouthCustomizer.connect("changed", self, "_on_face_changed")
	$Customizer/PupilCustomizer.connect("changed", self, "_on_face_changed")
	$Customizer/HatCustomizer/Label.text = 'Hats'
	$Customizer/EarCustomizer/Label.text = 'Ears'
	$Customizer/EyeCustomizer/Label.text = 'Eyes'
	$Customizer/PupilCustomizer/Label.text = 'Pupils'
	$Customizer/MouthCustomizer/Label.text = 'Mouths'
	$Customizer/BaseCustomizer/Label.text = 'Heads'
	$BubbleHolder/Bubble.sprite_base_scale = scale


func _process(_delta):
	var target_position = $BubbleHolder/Bubble.get_local_mouse_position()
	$BubbleHolder/Bubble.target_position = target_position
	$BubbleHolder/Bubble.position = bubble_position


func init(selected_bubble, _bubble_map):
	print('BubbleCustomizer::init ', _bubble_map)
	bubble_map = _bubble_map
	bubble_list = _bubble_map.values()
	
	var bubble_data = bubble_map[selected_bubble]
	i = bubble_list.find(bubble_data)
	selected_bubble_id = bubble_data.id
	render(bubble_data)


func _on_right_pressed():
	scroll_bubbles(1)


func _on_left_pressed():
	scroll_bubbles(-1)


func _on_train_pressed():
	var data = bubble_list[i]
	var attributes = []
	Pickups.add('pearls', -_calc_train_cost(data))
	for attribute in ['speed', 'tough', 'skill']:
		if data.nurture[attribute] < 3:
			attributes.append(attribute)
	if len(attributes) > 0:
		attributes.shuffle()
		var attribute = attributes[0]
		data.nurture[attribute] = min(3, data.nurture[attribute] + 1)
	$AudioStreamPlayer.play()
	Settings.save_game()
	render(data)
	
	var attribute = attributes[0]
	var skill_total = data.nature[attribute] + data.nurture[attribute]
	var node_path = "Upgrader/{attribute}Row/SkillPoint{i}/AnimationPlayer".format({"i": skill_total - 1, "attribute": attribute.capitalize()})
	get_node(node_path).play("burn")
	
	Music.dampen(30, 1)


func _on_edit_pressed():
	$Upgrader.visible = false
	$Customizer.visible = true
	$AnimationPlayer.play("grow")


func _on_save_pressed():
	$Upgrader.visible = true
	$Customizer.visible = false
	$AnimationPlayer.play("grow", -1, -1, true)


func _on_face_changed():
	var data = bubble_list[i]
	data.base.id = $Customizer/BaseCustomizer.id
	data.base.color = $Customizer/BaseCustomizer.color
	data.ear.id = $Customizer/EarCustomizer.id
	data.ear.color = $Customizer/EarCustomizer.color
	data.eyes.id = $Customizer/EyeCustomizer.id
	data.eyes.color = $Customizer/EyeCustomizer.color
	data.hat.id = $Customizer/HatCustomizer.id
	data.hat.color = $Customizer/HatCustomizer.color
	data.mouth.id = $Customizer/MouthCustomizer.id
	data.mouth.color = $Customizer/MouthCustomizer.color
	data.pupils.id = $Customizer/PupilCustomizer.id
	data.pupils.color = $Customizer/PupilCustomizer.color
	$BubbleHolder/Bubble/Sprite/Face.customize(data)


func scroll_bubbles(count):
	i += count
	if i >= len(bubble_list):
		i = 0
	if i < 0:
		i = len(bubble_list) - 1
	var bubble_data = bubble_list[i]
	selected_bubble_id = bubble_data.id
	render(bubble_data)


func render(data):
	var cost = _calc_train_cost(data)
	
	$BubbleHolder/Bubble/Sprite/Face.customize(data)
	$Upgrader/Name.text = data.name
	$Upgrader/SpeedRow.render(data.nature.speed, data.nurture.speed)
	$Upgrader/ToughRow.render(data.nature.tough, data.nurture.tough)
	$Upgrader/SkillRow.render(data.nature.skill, data.nurture.skill)
	$Customizer/BaseCustomizer.set_data(data.base)
	$Customizer/EarCustomizer.set_data(data.ear)
	$Customizer/EyeCustomizer.set_data(data.eyes)
	$Customizer/HatCustomizer.set_data(data.hat)
	$Customizer/MouthCustomizer.set_data(data.mouth)
	$Customizer/PupilCustomizer.set_data(data.pupils)


func _calc_train_cost(data):
	var nurture_rank = data.nurture.speed + data.nurture.tough + data.nurture.skill
	var nature_rank = data.nature.speed + data.nature.tough + data.nature.skill
	var rank = 1 + nature_rank + nurture_rank
	var cost = 100 * rank
	return cost


func _calc_nurture_rank(data):
	return data.nurture.speed + data.nurture.tough + data.nurture.skill
