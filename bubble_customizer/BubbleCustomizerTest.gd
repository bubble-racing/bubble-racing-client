extends Node2D


var selected_bubble = "b3"
var bubble_map = {
	"b1": {
		"name": "Velocity Jobs",
		"base": { "id": 1, "color": "FFFFFF" },
		"ear": { "id": 1, "color": "FFFFFF" },
		"hat": { "id": 1, "color": "FFFFFF" },
		"mouth": { "id": 1, "color": "FFFFFF" },
		"eyes": { "id": 1, "color": "000000" },
		"pupils": { "id": 1, "color": "FFFFFF" },
		"nature": {
			"speed": 0,
			"tough": 0,
			"skill": 0
		},
		"nurture": {
			"speed": 0,
			"tough": 0,
			"skill": 0
		}
	},
	"b2": {
		"name": "Fast Brass",
		"base": { "id": 2, "color": "FFFFFF" },
		"ear": { "id": 2, "color": "FFFFFF" },
		"hat": { "id": 2, "color": "FFFFFF" },
		"mouth": { "id": 2, "color": "FFFFFF" },
		"eyes": { "id": 2, "color": "000000" },
		"pupils": { "id": 2, "color": "FFFFFF" },
		"nature": {
			"speed": 3,
			"tough": 3,
			"skill": 3
		},
		"nurture": {
			"speed": 3,
			"tough": 3,
			"skill": 0
		}
	},
	"b3": {
		"name": "Frozen Brakes",
		"base": { "id": 3, "color": "FFFFFF" },
		"ear": { "id": 3, "color": "FFFFFF" },
		"hat": { "id": 3, "color": "FFFFFF" },
		"mouth": { "id": 3, "color": "FFFFFF" },
		"eyes": { "id": 3, "color": "000000" },
		"pupils": { "id": 3, "color": "FFFFFF" },
		"nature": {
			"speed": 1,
			"tough": 2,
			"skill": 3
		},
		"nurture": {
			"speed": 3,
			"tough": 2,
			"skill": 1
		}
	}
}


# Called when the node enters the scene tree for the first time.
func _ready():
	Settings.persist.pearls = 10000
	Music.play_race()
	$BubbleCustomizer.init(selected_bubble, bubble_map)
