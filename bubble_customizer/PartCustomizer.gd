extends Node2D
signal changed

var id = 1
var min_id = 1
var max_id = 9
var color = Color.black
var color_did_change = false
var picker_position: Vector2
onready var color_picker = get_node("ColorPicker")


func _ready():
	color_picker.connect("color_changed", self, "_on_color_changed")
	$LeftButton.connect("pressed", self, "_on_left_pressed")
	$RightButton.connect("pressed", self, "_on_right_pressed")
	$TextureButton.connect("button_down", self, "_on_color_pressed")
	color_picker.visible = false
	$ColorBG.visible = false
	
	# Base positioning on the editor values
	picker_position = Vector2(color_picker.rect_position.x, color_picker.rect_position.y)
	
	# Hacky simplification of the color picker
	var children = color_picker.get_children()
	var columns = children[0].get_children()
	columns[1].rect_min_size.x = 80
	children[1].visible = false
	children[2].visible = false
	children[3].visible = false
	children[4].visible = false


func _input(event):
	if (event is InputEventMouseButton && event.button_index == BUTTON_LEFT):
		if(!event.pressed):
			if (!color_did_change):
				color_picker.visible = false
				$ColorBG.visible = false
		color_did_change = false


func _on_color_changed(new_color: Color):
	color = new_color.to_html()
	color_did_change = true
	$TextureButton.self_modulate = new_color
	emit_signal("changed")


func _on_left_pressed():
	id -= 1
	if id < min_id:
		id = max_id
	emit_signal("changed")


func _on_right_pressed():
	id += 1
	if id > max_id:
		id = min_id
	emit_signal("changed")


func _on_color_pressed():
	color_did_change = true
	var screen_size = get_viewport().get_visible_rect().size
	if(screen_size.x - global_position.x <= 500):
		color_picker.rect_position.x = picker_position.x - 243
		$ColorBG/Square.position.x = 136.5
	if(screen_size.y - global_position.y <= 400):
		color_picker.rect_position.y = picker_position.y - 181
	color_picker.visible = !color_picker.visible
	color_picker.color = color
	$ColorBG/Square2.position = color_picker.rect_position + (color_picker.rect_size * color_picker.rect_scale / 2)
	$ColorBG.visible = color_picker.visible
	if color_picker.visible:
		var p = get_parent()
		p.remove_child(self)
		p.add_child(self)


func set_data(data):
	id = data.id
	color = data.color
	$TextureButton.self_modulate = data.color
