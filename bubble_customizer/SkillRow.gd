extends Node2D

var nature_points = 0
var nurture_points = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func render(_nature_points, _nurture_points):
	nature_points = _nature_points
	nurture_points = _nurture_points
	
	for i in range(0, nature_points):
		var point = get_node("SkillPoint{i}".format({"i": i}))
		point.render("nature-filled")
	
	for i in range(nature_points, nature_points + nurture_points):
		var point = get_node("SkillPoint{i}".format({"i": i}))
		point.render("nurture-filled")

	for i in range(nature_points + nurture_points, nature_points + nurture_points + (3 - nurture_points)):
		var point = get_node("SkillPoint{i}".format({"i": i}))
		point.render("nurture-empty")
	
	for i in range(nature_points + 3, 6):
		var point = get_node("SkillPoint{i}".format({"i": i}))
		point.render("void")


func level_up():
	render(nature_points, nurture_points + 1)
	var path = 'SkillPoint{i}/AnimationPlayer'.format({'i': (nature_points + nurture_points - 1)})
	get_node(path).play('burn')
