extends Node2D
signal pressed


func _ready():
	$Button.connect("pressed", self, "_on_pressed")


func _on_pressed():
	emit_signal("pressed")
