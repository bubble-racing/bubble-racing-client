extends Node2D

signal cherry_begin
signal cherry_end
signal render_complete

var Dot = preload("../tiles/dot/Dot.tscn")
var Bubble = preload('../bubble/Bubble.tscn')
var BubbleWander = preload('../bubble/BubbleWander.tscn')
var BubbleLocal = preload('../bubble/BubbleLocal.tscn')
var BubbleServer = preload('../bubble/BubbleServer.tscn')
var TileCreator = preload('../tiles/TileCreator.gd')
var tile_size = 256
var tile_creator = TileCreator.new()
var bubble_positions = []
var mode = 'node' # can be node or icon
var map
var exiting = false
var current_position: Vector2
var max_tiles_per_frame = 5
var add_dots = false
var rendering = false
var control = 'local'


func _ready():
	connect('tree_exiting', self, '_on_tree_exiting')


func _process(delta: float):
	if !rendering:
		return
		
	for i in max_tiles_per_frame:
		var tile_type: int = map.get_tile(current_position.x, current_position.y)
		if tile_type:
			render_tile(tile_type, int(current_position.x), int(current_position.y))
		if tile_type != TileTypes.WALL && add_dots:
			var dot = Dot.instance()
			dot.position.x = current_position.x * tile_size
			dot.position.y = current_position.y * tile_size
			add_child(dot)
		if current_position.is_equal_approx(map.max_pos):
			rendering = false
			emit_signal('render_complete')
			break
			
		current_position.x += 1
		if current_position.x > map.max_pos.x:
			current_position.x = map.min_pos.x
			current_position.y += 1


func _on_tree_exiting():
	exiting = true


func render_map(map2d: Map2D, _control: String) -> void:
	control = _control
	map = map2d
	rendering = true
	current_position = map.min_pos


func render_map_sync(map2d: Map2D) -> void:
	map = map2d
	for x in map.data:
		for y in map.data[x]:
			var tile = map.get_tile(x, y)
			render_tile(tile, x, y)


func render_tile(tile_type: int, x: int, y: int):
	if mode == 'node' && tile_type == TileTypes.BUBBLE && len(bubble_positions) == 0:
		bubble_positions.append(Vector2(x * tile_size, y * tile_size))
		return
		
	var node: Node2D = create_node(tile_type)
	
	if tile_type == TileTypes.WALL && mode == 'node':
		render_wall(node, x, y)
	
	if node:
		if node.get('control'):
			node.control = control
		if node.get('min_pos'):
			node.min_pos = map.min_pos * tile_size
			node.max_pos = map.max_pos * tile_size
		node.position.x = x * tile_size
		node.position.y = y * tile_size
		node.name = '{x}-{y}'.format({'x': x, 'y': y})
		node.connect('tree_exited', self, 'on_node_exiting', [tile_type, Vector2(x, y)])
		add_child(node)
		
		if tile_type == TileTypes.BUBBLE && mode == 'node':
			node.activate_autopilot(map)


func render_wall(node: Node2D, x: int, y: int):
	node.tl = map.get_tile(x-1, y-1) == TileTypes.WALL
	node.t = map.get_tile(x, y-1) == TileTypes.WALL
	node.tr = map.get_tile(x+1, y-1) == TileTypes.WALL
	node.r = map.get_tile(x+1, y) == TileTypes.WALL
	node.br = map.get_tile(x+1, y+1) == TileTypes.WALL
	node.b = map.get_tile(x, y+1) == TileTypes.WALL
	node.bl = map.get_tile(x-1, y+1) == TileTypes.WALL
	node.l = map.get_tile(x-1, y) == TileTypes.WALL
	node.redraw()


func render_wall_block(pos: Vector2):
	var positions = [
		pos + Vector2(-1, -1), pos + Vector2(0, -1), pos + Vector2(1, -1),
		pos + Vector2(-1, 0), pos + Vector2(0, 0), pos + Vector2(1, 0),
		pos + Vector2(-1, 1), pos + Vector2(0, 1), pos + Vector2(1, 1)
	]
	for position in positions:
		var tile = map.get_tile(position.x, position.y)
		if tile == TileTypes.WALL:
			var node_name = '{x}-{y}'.format({'x': position.x, 'y': position.y})
			var node = get_node(node_name)
			if node:
				render_wall(node, position.x, position.y)


func on_node_exiting(tile_type, tile_pos):
	if exiting:
		return
	map.set_tile(tile_pos.x, tile_pos.y, TileTypes.EMPTY)
	if tile_type == TileTypes.WALL && mode == 'node':
		render_wall_block(tile_pos)


func swap_tile(tile_id:int, x:int, y:int):
	var name = '{x}-{y}'.format({'x': x, 'y': y})
	var new = create_node(tile_id)
	if new:
		new.position.x = x * tile_size
		new.position.y = y * tile_size
	swap_node(name, new)


func create_node(tile_id):
	if mode == 'node':
		return tile_creator.create_tile(tile_id)
	if mode == 'icon':
		return tile_creator.create_icon(tile_id)
	

func swap_node(name: String, new: Node2D):
	var old: Node2D = get_node(name)
	
	if old:
		remove_child(old)
	
	if new:
		new.name = name
		add_child(new)
	
	return new


func add_bubble(name: String, type: String):
	Logger.info('LevelRenderer::add_bubble', name, type)
	var bubble:Bubble
	if type == 'local':
		bubble = BubbleLocal.instance()
		bubble.control = 'local'
	elif type == 'wander':
		bubble = BubbleWander.instance()
	elif type == 'server':
		bubble = BubbleServer.instance()
	elif type == 'bot':
		bubble = Bubble.instance()
	else:
		bubble = Bubble.instance()
		bubble.control = 'remote'
	bubble.name = name
	bubble.position = get_next_bubble_position()
	add_child(bubble)
	return bubble


func get_next_bubble_position():
	if len(bubble_positions) > 0:
		return bubble_positions[0]
	return Vector2(0, 0)


func clear():
	bubble_positions = []
	for node in get_children():
		remove_child(node)
		node.queue_free()
