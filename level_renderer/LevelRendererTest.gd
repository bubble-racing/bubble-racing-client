extends Node2D

var LevelGenerator = preload("../level_generator/LevelGenerator.gd")
var SinglePlayerLevels = preload("../level_generator/SinglePlayerLevels.gd")

var level_generator = LevelGenerator.new()
var level_settings = SinglePlayerLevels.new().level_settings


func _ready():
	var map = level_generator.generate(level_settings[Settings.level_id])
	$LevelRenderer.add_dots = true
	$LevelRenderer.render_map(map, 'local')
