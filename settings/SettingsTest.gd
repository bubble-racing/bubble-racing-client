extends Node2D


func _on_Save_pressed():
	Settings.persist.test = $TextEdit.text
	Settings.save_game()


func _on_Load_pressed():
	Settings.load_game()
	$TextEdit.text = Settings.persist.get('test', '')


func _on_Delete_pressed():
	Settings.delete_game()
	Settings.persist.test = ""
	$TextEdit.text = ""
