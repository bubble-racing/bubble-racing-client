extends Node
var level_category: String
var level_name: String
var mode = "single-player" # must be single-player, multi-player, or editor-test
var save_file_path = "user://bubbleracing.save"
var bubble_counter = 0
var title_page = 'root'
var min_dimensions = Vector2(100, 100)
var max_dimensions = Vector2(1000, 1000)
var persist = {
	'sound_effects': true,
	'music': true,
	'fullscreen': false,
	'autopilot': false
}
var temp = {}
var tile_size = 256
var enable_multiplayer = true


func save_game():
	var file = File.new()
	file.open(save_file_path, File.WRITE)
	file.store_line(to_json(persist))
	file.close()


func load_game():
	var file = File.new()
	if not file.file_exists(save_file_path):
		return

	file.open(save_file_path, File.READ)
	persist = parse_json(file.get_line())
	if !'user_id' in persist:
		persist.user_id = UUID.v4()
	file.close()


func delete_game():
	var file = File.new()
	if not file.file_exists(save_file_path):
		return # Error! We don't have a save to load.
	
	var dir = Directory.new()
	dir.remove(save_file_path)
