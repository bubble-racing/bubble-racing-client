extends Node2D

var scenes = {
	'title': preload('res://pages/title/Title.tscn'),
	'game': preload('res://pages/game/Game.tscn'),
	'lobby': preload('res://pages/lobby/Lobby.tscn'),
	'editor': preload('res://pages/editor/Editor.tscn'),
	'about': preload('res://pages/about/About.tscn'),
	'config': preload('res://pages/config/Config.tscn'),
	'level-load': preload('res://pages/level_load/LevelLoad.tscn'),
	'level-save': preload('res://pages/level_save/LevelSave.tscn'),
	'level-voter': preload('res://pages/level_voter/LevelVoter.tscn'),
	'bubble-picker': preload('res://pages/bubble_picker/BubblePicker.tscn'),
	'featured-bubble': preload('res://pages/featured_bubble/FeaturedBubble.tscn')
}
var current_scene
var next_scene_name
var player


func _ready():
	# Disable multiplayer on ios
	if OS.get_name() == 'iOS':
		Settings.enable_multiplayer = false
		
	if '--autopilot' in OS.get_cmdline_args():
		Settings.persist.autopilot = true
	if '--server' in OS.get_cmdline_args():
		Settings.persist.music = false
		Settings.persist.sound_effects = false
		print('Running in server mode')
	else:
		print('Running in client mode')
		Settings.load_game()
		OS.window_fullscreen = Settings.persist.get('fullscreen', false)
		print('about to test')
		if ('next_load_scene' in Settings.persist):
			print('next_load_scene found')
			set_scene_by_name(Settings.persist.next_load_scene)
			Settings.persist.erase('next_load_scene')
		else:
			print('next_load_scene not found')
			set_scene_by_name('title')
	
	player = $CanvasLayer/PageTransition/AnimationPlayer
	player.connect("animation_finished", self, '_on_animation_finished')

	if Engine.has_singleton("InAppStore"):
		print("InAppStore is available")
	else:
		print("iOS IAP plugin is not available on this platform.")


func set_scene_by_name(scene_name):
	print('set_scene_by_name: ', scene_name)
	if current_scene:
		next_scene_name = scene_name
		player.play("transition-in")
		if Settings.persist.sound_effects:
			$CanvasLayer/PageTransition/WaveSound.play()
	else:
		var scene = scenes[scene_name].instance()
		set_scene(scene)


func set_scene(scene):
	if current_scene:
		current_scene.queue_free()
		yield(get_tree().create_timer(0.1), "timeout") # Give the old scene time to be removed
	current_scene = scene
	add_child(scene)


func _on_animation_finished(animation_name):
	if animation_name == 'transition-in' and next_scene_name:
		var scene = scenes[next_scene_name].instance()
		set_scene(scene)
		player.play("transition-out")
		next_scene_name = ''
