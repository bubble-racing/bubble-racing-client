extends Node2D

var initialScale = Vector2(1, 2)


func _process(_delta):
	scale = initialScale + (initialScale / 2 * Music.loudness / 100)
	modulate.a = 0.7 + (0.3 * Music.loudness / 100)
