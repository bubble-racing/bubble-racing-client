extends Node2D


func _process(_delta):
	var mod =  ((Music.loudness / 15) / 3.5) + 1
	modulate.r = mod
	modulate.g = mod
	modulate.b = mod
