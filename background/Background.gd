extends Node2D

var rng = RandomNumberGenerator.new()
var lastModulate = {
	'r': 1,
	'g': 1,
	'b': 1
}
var nextModulate = {
	'r': 1,
	'g': 1,
	'b': 1
}
var delta = {
	'r': 0,
	'g': 0,
	'b': 0
}

var bg_size = Vector2(1000, 900)
var min_scale = Vector2(1, 1)
var lock_y = true
var offset_y = -50


func _ready():
	rng.randomize()
	set_water_line(0)


func _process(_elapsed):
	var progress = (($Timer.wait_time - $Timer.time_left) / $Timer.wait_time)
	var target
	if ($WaterEffect.visible):
		target = $WaterEffect
	else:
		target = $WaterFlat
	target.modulate.r = lastModulate.r + (delta.r * progress)
	target.modulate.g = lastModulate.g + (delta.g * progress)
	target.modulate.b = lastModulate.b + (delta.b * progress)
	


func _on_Timer_timeout():
	lastModulate = nextModulate
	nextModulate = {
		'r': .70 + rng.randf_range(0, 0.40),
		'g': .50 + rng.randf_range(0, 0.65),
		'b': .70 + rng.randf_range(0, 0.40)
	}
	delta = {
		'r': nextModulate.r - lastModulate.r,
		'g': nextModulate.g - lastModulate.g,
		'b': nextModulate.b - lastModulate.b
	}


func simplify():
	$WaterEffect.visible = false
	$WaterFlat.visible = true
	$Fade.visible = true
	$BackgroundLight.visible = false


func set_water_line(num):
	$BackgroundSunset.position.y = num - 477
	$Fade.position.y = num + 100
	$WaterEffect.material.set_shader_param('top_offset', num / 1000)


func enable_auto_resize():
	get_viewport().connect("size_changed", self, "_on_size_changed")
	_on_size_changed()


func _on_size_changed():
	var window_size = get_viewport().get_visible_rect().size
	var base_scale = window_size / bg_size
	var streatched_scale = Vector2(max(base_scale.x, min_scale.x), max(base_scale.y, min_scale.y))
	var aspect_scale = Vector2(max(streatched_scale.x, streatched_scale.y), max(streatched_scale.x, streatched_scale.y))
	set_scale(aspect_scale)
	
	var pos = window_size / 2
	pos.x = round(pos.x)
	pos.y = round(pos.y + offset_y)
	if lock_y:
		pos.y = offset_y
	position = pos
