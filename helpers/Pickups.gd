class_name Pickups


static func stow(type: String, num: int) -> void:
	var stow = Settings.temp.get('stow', {})
	stow[type] = stow.get(type, 0) + num
	Settings.temp.stow = stow


static func add(type: String, num: int) -> void:
	Settings.persist[type] = Settings.persist.get(type, 0) + num


static func commit() -> void:
	var stowed = Settings.temp.get('stow', {})
	for type in stowed:
		var num = Settings.temp.stow[type]
		Settings.persist[type] = Settings.persist.get(type, 0) + num
	Settings.temp.stow = {}


static func clear() -> void:
	Settings.temp.stow = {}


static func get_stowed(type: String) -> int:
	return Settings.temp.get('stow', {}).get(type, 0)


static func get_current(type: String) -> int:
	return Settings.persist.get(type, 0)


static func get_total(type: String) -> int:
	return get_stowed(type) + get_current(type)
