extends Node2D

var api_url = BaseURL.get_base_url() + '/api/featured-bubble'
var bubble
var time_left = 1000


#func _ready():
#	$HTTPRequest.connect("request_completed", self, "_on_request_completed")
#	$Timer.connect("timeout", self, "_fetch")
#	if !('--server' in OS.get_cmdline_args()):
#		$Timer.start()


func _fetch():
	$HTTPRequest.request(api_url)


func _on_request_completed(result, response_code, headers, body_bytes):
	if response_code != 200:
		_handle_error('_on_request_completed - got response code: ' + String(response_code))
		return
		
	var body_str = body_bytes.get_string_from_utf8()
	var json_result = JSON.parse(body_str)
	if json_result.error != OK:
		_handle_error('_on_request_completed - failed to parse JSON response')
		return
	
	var response = json_result.result
	Logger.info('FeaturedBubble::_on_request_completed', response)
	bubble = response.bubble
	time_left = response.timeLeft
	$Timer.wait_time = time_left
	$Timer.start()


func _handle_error(message):
	Logger.error("FeaturedBubble::" + message)
	bubble = null
	$Timer.wait_time = 120
	$Timer.start()


func is_owned() -> bool:
	if !bubble:
		return false
	
	var my_bubbles = MyBubbles.get_bubbles()
	return bubble.id in my_bubbles


func get_time_left() -> int:
	return int($Timer.time_left)
