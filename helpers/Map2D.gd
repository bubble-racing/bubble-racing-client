extends Object
class_name Map2D

var data = {}
var min_pos = Vector2(0, 0)
var max_pos = Vector2(0, 0)
var title = ''
var user_nickname = ''
var category = ''


func get_tile(x: int, y: int) -> int:
	if !data.get(x):
		return 0
	var value = data[x].get(y)
	if !value:
		return 0
	return value


func export_data() -> Dictionary:
	return {
		'data': data,
		'title': title,
		'user_nickname': user_nickname,
		'category': category
	}


func import_data(dict: Dictionary) -> void:
	title = dict.get('title', '')
	user_nickname = dict.user_nickname
	category = dict.category
	data = dict.data
	calc_bounds()
	ensure_start_position()


func set_tile(x: int, y: int, value):
	if !data.get(x):
		data[x] = {}
	data[x][y] = value


func is_tile_passable(x: int, y: int) -> bool:
	var type = get_tile(x, y)
	if type == TileTypes.WALL:
		return false
	else:
		return true


func is_tile_in_bounds(tile: Vector2) -> bool:
	return tile.x >= min_pos.x && tile.x <= max_pos.x && tile.y >= min_pos.y && tile.y <= max_pos.y


func set_data(new_data):
	clear()
	for x in new_data:
		for y in new_data[x]:
			var value = new_data[x][y]
			# x can get turned into a string if json_encoded
			if value:
				set_tile(int(x), int(y), value)
	
	calc_bounds()
	ensure_start_position()


# determine the overall dimensions of this map
func calc_bounds():
	min_pos = Vector2(INF, INF)
	max_pos = Vector2(-INF, -INF)
	
	for x in data:
		for y in data[x]:
			if data[x][y] != TileTypes.EMPTY:
				if x < min_pos.x:
					min_pos.x = x
				if x > max_pos.x:
					max_pos.x = x
				if y < min_pos.y:
					min_pos.y = y
				if y > max_pos.y:
					max_pos.y = y
	
	# if data is empty, min and max pos will still have infinity values
	if min_pos.x == INF || max_pos.x == -INF || min_pos.y == INF || max_pos.y == -INF:
		min_pos = Vector2(0, 0)
		max_pos = Vector2(0, 0)


func find_start_position() -> Vector2:
	for x in data:
		for y in data[x]:
			var value = data[x][y]
			if value == TileTypes.BUBBLE:
				return(Vector2(x, y))
	return Vector2(0, 0)


# If there is no start position, add one around the bottom left of the map
func ensure_start_position():
	if find_start_position():
		return
	
	for x in range(min_pos.x, max_pos.x, 1):
		for y in range(max_pos.y, min_pos.y, -1):
			if get_tile(x, y) == 0:
				set_tile(x, y, TileTypes.BUBBLE)
				return
	
	# This line will only run if the above loop does not find any empty spots
	set_tile(0, 5, TileTypes.BUBBLE)


func transform_to_zero() -> void:
	calc_bounds()
	if min_pos.y != 0:
		transform(Vector2(0, -min_pos.y))


func transform(t: Vector2) -> void:
	var data_copy = {}
	for x in data:
		for y in data[x]:
			if !data_copy.get(x + t.x):
				data_copy[x + t.x] = {}
			data_copy[x + t.x][y + t.y] = data[x][y]
	data = data_copy
	calc_bounds()


func clear():
	data = {}
	min_pos = Vector2(0, 0)
	max_pos = Vector2(0, 0)
