extends Node2D


func _ready():
	$Label1.text = ""
	$Label2.text = "Blurb"
	$Label3.text = "Bob Villa"
	$Label4.text = NameGenerator.generate()
	$Label5.text = "Way too many words"
	
	$ValidLabel1.text = NameGenerator.standardize($Label1.text)
	$ValidLabel2.text = NameGenerator.standardize($Label2.text)
	$ValidLabel3.text = NameGenerator.standardize($Label3.text)
	$ValidLabel4.text = NameGenerator.standardize($Label4.text)
	$ValidLabel5.text = NameGenerator.standardize($Label5.text)
