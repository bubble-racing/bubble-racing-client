class_name TimeFormatter


static func format(float_seconds: float) -> String:
	var int_seconds = int(round(float_seconds))
	var minutes = floor(int_seconds / 60)
	var seconds = int_seconds % 60
	var minutes_str = _left_pad(str(minutes), 1, '0')
	var seconds_str = _left_pad(str(seconds), 2, '0')
	return minutes_str + ':' + seconds_str


static func _left_pad(string: String, length: int, pad: String) -> String:
	while len(string) < length:
		string = str(pad, string)
	return string
