extends Node2D


func _ready():
	_test(123)
	_test(154.215648)
	_test(154.9)
	_test(0)
	_test(-1)
	

func _test(seconds):
	var result = TimeFormatter.format(seconds)
	print(seconds, ': ', result)
