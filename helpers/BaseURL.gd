class_name BaseURL

static func get_base_url() -> String:
	if '--local' in OS.get_cmdline_args() || OS.is_debug_build() || OS.get_environment('BR_ENV') == 'local':
		return 'http://localhost'
	elif '--dev' in OS.get_cmdline_args() || OS.get_environment('BR_ENV') == 'dev':
		return 'https://dev.bubbleracing.com'
	else:
		return 'https://bubbleracing.com'
