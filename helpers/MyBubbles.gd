class_name MyBubbles


static func get_current_bubble() -> Dictionary:
	var bubbles = get_bubbles()
	return bubbles[get_current_bubble_id()]


static func get_current_bubble_id() -> String:
	var bubbles = get_bubbles() # double purpose of creating initial current_bubble_id
	return Settings.persist.get('current_bubble_id', bubbles.keys()[0])


static func set_current_bubble_id(id: String):
	Settings.persist.current_bubble_id = id


static func get_bubbles() -> Dictionary:
	var bubbles = Settings.persist.get('bubbles', {})
	
	# Give a starter bubble to first time players
	while(len(bubbles.keys()) < 3):
		var bubble = generate_random_bubble()
		bubbles[bubble.id] = bubble
		Settings.persist.bubbles = bubbles
		Settings.persist.current_bubble_id = bubble.id
	
	return bubbles


static func add_bubble(bubble: Dictionary) -> void:
	var bubbles = get_bubbles()
	bubbles[bubble.id] = bubble
	Settings.persist.current_bubble_id = bubble.id
	Settings.persist.bubbles = bubbles


static func generate_random_bubble() -> Dictionary:
	var bubble = FaceGenerator.generate()
	bubble.nature = {
		"speed": 0,
		"tough": 0,
		"skill": 0
	}
	bubble.nurture = {
		"speed": 0,
		"tough": 0,
		"skill": 0
	}
	bubble.id = UUID.v4()
	bubble.name = NameGenerator.generate()
	return bubble


static func generate_nature(points: int, rng_seed: int = 0) -> Dictionary:
	var rng = RandomNumberGenerator.new()
	if rng_seed:
		rng.seed = rng_seed
	else:
		rng.randomize()
		
	var nature = {
		"speed": 0,
		"tough": 0,
		"skill": 0
	}
	var keys = nature.keys()
	for _b in range(0, points):
		var index = rng.randi_range(0, len(keys) - 1)
		var key = keys[index]
		nature[key] += 1
		if nature[key] >= 3:
			keys.remove(index)
	return nature
