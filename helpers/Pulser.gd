extends Node2D
class_name Pulser

var initial_scale
var grow_scale = Vector2(0.08, 0.08)
var initial_alpha = 0.3
var grow_alpha = 0.3


func _ready():
	initial_scale = scale


func _process(_delta):
	var loudness = Music.loudness / 100.0
	scale = initial_scale + (grow_scale * loudness)
	modulate.a = initial_alpha + (grow_alpha * loudness)
