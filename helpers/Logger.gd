extends Node2D

const ALL = 0
const DEBUG = 1
const INFO = 2
const ERROR = 3
const NONE = 4
var log_level = INFO

func _ready():
	var l = OS.get_environment('BR_LOG_LEVEL')
	if l == 'ALL':
		log_level = ALL
	if l == 'DEBUG':
		log_level = DEBUG
	if l == 'INFO':
		log_level = INFO
	if l == 'ERROR':
		log_level = ERROR
	if l == 'NONE':
		log_level = NONE
	print('Logger setting log level to ', log_level, ' (', l, ')')


func error(p1, p2='', p3='', p4='') -> void:
	if ERROR >= log_level:
		print(_time(), ' ', p1, ' ', p2, ' ', p3, ' ', p4)


func info(p1, p2='', p3='', p4='') -> void:
	if INFO >= log_level:
		print(_time(), ' ', p1, ' ', p2, ' ', p3, ' ', p4)


func debug(p1, p2='', p3='', p4='') -> void:
	if DEBUG >= log_level:
		print(_time(), ' ', p1, ' ', p2, ' ', p3, ' ', p4)


func _time() -> String:
	# todo: after upgradeing to new version: Time.get_datetime_string_from_system()
	var time = OS.get_time()
	return String(time.hour) + ":" + String(time.minute) + ":" + String(time.second)
