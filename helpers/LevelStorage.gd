# singleton
extends Node2D

var rng: RandomNumberGenerator


func _ready():
	rng = RandomNumberGenerator.new()
	rng.randomize()


func get_local_levels():
	if !Settings.persist.has('local_levels'):
		Settings.persist.local_levels = {}
	return Settings.persist.local_levels


func store(level):
	# save to local storage
	var slug = _to_slug(level.title)
	var file = File.new()
	file.open("user://" + slug + ".level.json", File.WRITE)
	file.store_line(to_json(level))
	file.close()

	# save title
	var levels = get_local_levels()
	levels[slug] = {'published': true, 'title': level.title}
	Settings.save_game()

	# keep open the possibility of writing to a server
	yield(get_tree().create_timer(0.1), 'timeout')
	return level


func fetch(title):
	# fetch from local storage
	var slug = _to_slug(title)
	var file = File.new()
	file.open("user://" + slug + ".level.json", File.READ)
	var line = file.get_line()
	var level = parse_json(line)
	file.close()
	return level


func get_random_published_level():
	var levels = get_local_levels()
	var published_levels = []
	for level_id in levels:
		var level = levels[level_id]
		if level.published:
			published_levels.append(level)
	
	if len(published_levels) == 0:
		return null
	
	var random_index = rng.randi_range(0, len(published_levels) - 1)
	var published_level = published_levels[random_index]
	return published_level


func _to_slug(input_str: String) -> String:
	var lower_str = input_str.to_lower()
	var regex = RegEx.new()
	regex.compile("[^-a-z0-9]")
	return regex.sub(lower_str, "-", true)
