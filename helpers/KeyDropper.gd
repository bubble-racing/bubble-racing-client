class_name KeyDropper


static func drop(map: Map2D) -> Map2D:
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	map.calc_bounds()
	var width = map.max_pos.x - map.min_pos.x
	var height = map.max_pos.y - map.min_pos.y
	var area = width * height
	
	var max_keys = area / 50
	var min_keys = 0
	var keys = rng.randi_range(min_keys, max_keys)
	place_item(map, TileTypes.KEY, keys)
	
	var max_chests = area / 100
	var min_chests = 0
	var chests = rng.randi_range(min_chests, max_chests)
	place_item(map, TileTypes.CHEST, chests)
	
	var max_pearl_groups = area / 10
	var min_pearl_groups = area / 50
	var pearl_groups = rng.randi_range(min_pearl_groups, max_pearl_groups)
	place_item(map, TileTypes.BREADCRUMBS, pearl_groups)
	
	return map


static func place_item(map: Map2D, tile_type: int, count: int) -> void:
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	for i in range(0, count):
		var x = rng.randi_range(map.min_pos.x, map.max_pos.x)
		var y = rng.randi_range(map.min_pos.y, map.max_pos.y)
		var existing_tile = map.get_tile(x, y)
		if existing_tile == TileTypes.EMPTY:
			map.set_tile(x, y, tile_type)
