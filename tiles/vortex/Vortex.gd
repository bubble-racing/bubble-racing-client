extends Node2D

var reach = 300
var Debris = preload('../debris/DebrisPiece.tscn')
var rng = RandomNumberGenerator.new()
var impulse = 6000
var min_impulse = 1000
var impulse_decay = 5


func _ready():
	rng.randomize()
	var debris_count = rng.randi_range(0, 3)
	for i in debris_count:
		_add_debris()
	
	if Settings.persist.sound_effects:
		$VortexSound.play()


func _add_debris() -> void:
	var debris = Debris.instance()
	debris.position.x = position.x + rng.randi_range(-50, 50)
	debris.position.y = position.y + rng.randi_range(-50, 50)
	get_node('../').add_child(debris)


func _process(delta):
	# spin the spiral
	$Spiral.rotation -= delta * 3
	
	# spin bodies that are close
	var bodies = $Area2D.get_overlapping_bodies()
	for body in bodies:
		if body.get_class() == 'Turtle':
			return
		if 'Bubble' in body.get_class() && body.control == 'local':
			Music.dampen(25, 2)
		var spin_direction = (body.position - position).tangent()
		var pull_direction = (position - body.position)
		var direction = (spin_direction + pull_direction).normalized()
		var distance = min(spin_direction.length(), reach)
		var strength = 1 - (distance / reach)
		body.apply_central_impulse(direction * delta * strength * impulse)
		body.apply_torque_impulse(-strength * 150)
		if impulse > min_impulse && 'Bubble' in body.get_class():
			impulse -= impulse_decay
