# Singleton

var tile_scene_map = {
	TileTypes.WALL: preload("../tiles/wall/Wall.tscn"),
	TileTypes.PHOSPHORUS: preload("../tiles/phosphorus/Phosphorus.tscn"),
	TileTypes.SPEED: preload("../tiles/speed/Speed.tscn"),
	TileTypes.CURRENT_UP: preload("../tiles/current/CurrentUp.tscn"),
	TileTypes.CURRENT_RIGHT: preload("../tiles/current/CurrentRight.tscn"),
	TileTypes.CURRENT_DOWN: preload("../tiles/current/CurrentDown.tscn"),
	TileTypes.CURRENT_LEFT: preload("../tiles/current/CurrentLeft.tscn"),
	TileTypes.DEBRIS: preload("../tiles/debris/Debris.tscn"),
	TileTypes.VORTEX: preload("../tiles/vortex/Vortex.tscn"),
	TileTypes.GHOST: preload("../tiles/ghost/Ghost.tscn"),
	TileTypes.OIL: preload("../tiles/oil/Oil.tscn"),
	TileTypes.BUBBLE: preload('../bubble/Bubble.tscn'),
	TileTypes.BREADCRUMBS: preload('../tiles/breadcrumbs/Breadcrumbs.tscn'),
	TileTypes.KEY: preload('../tiles/key/Key.tscn'),
	TileTypes.CHEST: preload('../tiles/chest/Chest.tscn'),
	TileTypes.TURTLE: preload('../tiles/turtle/Turtle.tscn'),
	TileTypes.DIVER: preload('../tiles/diver/Diver.tscn'),
	TileTypes.CHERRIES: preload('../tiles/cherries/Cherries.tscn'),
	TileTypes.STARFISH: preload('../tiles/starfish/Starfish.tscn')
}

var icon_scene_map = {
	TileTypes.WALL: preload("../tiles/wall/WallIcon.tscn"), # preload("../tiles/wall/WallIcon.tscn"),
	TileTypes.PHOSPHORUS: preload("../tiles/phosphorus/PhosphorusIcon.tscn"),
	TileTypes.SPEED: preload("../tiles/speed/SpeedIcon.tscn"),
	TileTypes.CURRENT_UP: preload("../tiles/current/CurrentUpIcon.tscn"),
	TileTypes.CURRENT_RIGHT: preload("../tiles/current/CurrentRightIcon.tscn"),
	TileTypes.CURRENT_DOWN: preload("../tiles/current/CurrentDownIcon.tscn"),
	TileTypes.CURRENT_LEFT: preload("../tiles/current/CurrentLeftIcon.tscn"),
	TileTypes.DEBRIS: preload("../tiles/debris/DebrisIcon.tscn"),
	TileTypes.VORTEX: preload("../tiles/vortex/VortexIcon.tscn"),
	TileTypes.GHOST: preload("../tiles/ghost/GhostIcon.tscn"),
	TileTypes.OIL: preload("../tiles/oil/OilIcon.tscn"),
	TileTypes.BUBBLE: preload('../bubble/BubbleIcon.tscn'),
	TileTypes.BREADCRUMBS: preload('../tiles/breadcrumbs/BreadcrumbsIcon.tscn'),
	TileTypes.KEY: preload('../tiles/key/KeyIcon.tscn'),
	TileTypes.CHEST: preload('../tiles/chest/ChestIcon.tscn'),
	TileTypes.TURTLE: preload('../tiles/turtle/TurtleIcon.tscn'),
	TileTypes.DIVER: preload('../tiles/diver/DiverIcon.tscn'),
	TileTypes.CHERRIES: preload('../tiles/cherries/CherriesIcon.tscn'),
	TileTypes.STARFISH: preload('../tiles/starfish/StarfishIcon.tscn')
}


func create_tile(tile_type: int):
	var scene = tile_scene_map.get(tile_type)
	if scene:
		return scene.instance()


func create_icon(tile_type: int):
	var scene = icon_scene_map.get(tile_type)
	if scene:
		return scene.instance()
