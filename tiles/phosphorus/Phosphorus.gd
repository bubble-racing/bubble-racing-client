extends Node2D

var bubble_count = 0
var heat = 0
var heat_up_speed = 3
var cool_down_speed = 2
var max_heat = 5
var base_light_scale = Vector2(0.25, 0.25)
var base_energy = 0.8
var grow_energy = 0.5
var base_fire_scale = Vector2(2.0, 2.0)
var grow_fire_scale = Vector2(1.1, 1.1)
var base_fire_alpha = 0.0
var grow_fire_alpha = 1.0
var melted = false
var light_rng = RandomNumberGenerator.new()
var Dot = preload("../dot/Dot.tscn")


# This is a Phosphorus
func get_class():
	return 'Phosphorus'
	

func _ready():
	$Area2D.connect("body_entered", self, "_on_body_entered")
	$Area2D.connect("body_exited", self, "_on_body_exited")


func _process(delta):
	var progress = heat / max_heat
	
	if bubble_count > 0 and !melted:
		if heat < max_heat:
			heat += delta * heat_up_speed
			
	if heat > 0 and !melted:
		heat += 1 * delta
			
	if melted and heat > 0:
		heat -= delta * cool_down_speed
		if heat <= 0 and melted:
			queue_free()
	
	if heat >= max_heat and !melted:
		melt()
		
	if melted:
		_heat_wall(Vector2(-1, 0), delta)
		_heat_wall(Vector2(1, 0), delta)
		_heat_wall(Vector2(0, -1), delta)
		_heat_wall(Vector2(0, 1), delta)
	
	if heat > 0:
		$Light2D.energy = base_energy + (grow_energy * progress)
		$Light2D.scale = base_light_scale + (Vector2(heat, heat) / 7 * light_rng.randf_range(0.75, 1.0))
		$Light2D.visible = true
		$Fire.scale = base_fire_scale + (grow_fire_scale * progress * light_rng.randf_range(0.7, 1.0))
		$Fire.modulate.a = base_fire_alpha + (grow_fire_alpha * progress * light_rng.randf_range(0.5, 1.0))
		$Fire.visible = true
		if !melted:
			$Phosphorus.modulate.a = 1 - progress
	else:
		$Fire.visible = false
		$Light2D.visible = false
	
	var sound = $FireSound
	if bubble_count > 0 && Settings.persist.sound_effects:
		if !sound.playing:
			sound.play()
			sound.volume_db = -30
		if sound.volume_db < 5:
			sound.volume_db += 1
	else:
		if sound.playing:
			sound.volume_db -= 1
			if sound.volume_db < -30:
				sound.stop()


func melt() -> void:
	if melted:
		return
	melted = true
	$Phosphorus.queue_free()
	$LightOccluder2D.queue_free()
	$CollisionShape2D.disabled = true
	_break_wall(Vector2(-1, 0))
	_break_wall(Vector2(1, 0))
	_break_wall(Vector2(0, -1))
	_break_wall(Vector2(0, 1))
	for x in range(0, 3):
		for y in range(0, 3):
			var dot = Dot.instance()
			dot.position.x = position.x + x * 50
			dot.position.y = position.y + x * 50
			get_node('../').add_child(dot)


func _break_wall(dist: Vector2) -> void:
	var tile = (position / Settings.tile_size) + dist
	var tile_name = '../{x}-{y}'.format({'x': int(tile.x), 'y': int(tile.y)})
	var body = get_node_or_null(tile_name)
	if body && body.has_method('crumble'):
		body.crumble()


func _heat_wall(dist: Vector2, delta) -> void:
	var tile = (position / Settings.tile_size) + dist
	var tile_name = '../{x}-{y}'.format({'x': int(tile.x), 'y': int(tile.y)})
	var body = get_node_or_null(tile_name)
	if body && body.get_class() == 'Phosphorus' && !body.melted:
		body.heat += delta * heat_up_speed * 2


func _on_body_entered(body):
	if ('Bubble' in body.get_class()):
		bubble_count += 1


func _on_body_exited(body):
	if ('Bubble' in body.get_class()):
		bubble_count -= 1
