extends Node2D


func _on_body_entered(body):
	if ('Bubble' in body.get_class()):
		var vector: Vector2 = (body.position - position).normalized()
		var impulse = vector * 1000
		body.apply_central_impulse(impulse)
		body.activate_oil()
		if Settings.persist.sound_effects:
			$SlowSound.play()


func _process(delta):
	$Spinner.rotation += delta
