extends Wanderer


# This is a Speed
func get_class():
	return "Speed"
	
	
func _on_Area2D_body_entered(body):
	if ('Bubble' in body.get_class()):
		body.activate_speed()
		if Settings.persist.sound_effects:
			$SpeedSound.play()
	if control == 'local':
		pick_random_target()


func _process(_delta):
	$Glowbug.rotation_degrees = rad2deg((position - target_position).angle())
