extends Seeker
class_name Wanderer

var timer = Timer.new()
var stopped_threshold = 85
var max_pos = Vector2(INF, INF)
var min_pos = Vector2(-INF, 0)


# Called when the node enters the scene tree for the first time.
func _ready():
	._ready()
	add_child(timer)
	timer.connect("timeout", self, "_on_timeout")
	timer.wait_time = 1
	timer.start()


# Move towards target position
func _on_timeout():
	if control != 'local':
		return
	if (linear_velocity.length() < stopped_threshold):
		pick_random_target()
	target_position.x = clamp(target_position.x, min_pos.x, max_pos.x)
	target_position.y = clamp(target_position.y, min_pos.y, max_pos.y)


func pick_random_target():
	var move_x = rand_range(-1000, 1000)
	var move_y = rand_range(-1000, 1000)
	target_position = position + Vector2(move_x, move_y)
