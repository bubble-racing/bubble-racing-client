extends Node2D

var direction = Vector2(1, 0)
var impulse = 1800
var impulse_decay = 9
var impulse_grow = 1
var min_impulse = 500
var max_impulse = 1900
onready var current_sound = get_node('CurrentSound')


# push bodies that are close
func _process(delta):
	var bodies = $Area2D.get_overlapping_bodies()
	for body in bodies:
		if body.has_method('apply_central_impulse'):
			if 'Bubble' in body.get_class() && body.speed_countdown > 0:
				delta /= 2
				
			# Only start playing a sound if we interact with the local player
			if !$CurrentSound.playing && body.get_class() == 'BubbleLocal' && Settings.persist.sound_effects:
				$CurrentSound.play()
				
			body.apply_central_impulse(direction * delta * impulse)
			if impulse > min_impulse:
				impulse -= impulse_decay
	if impulse < max_impulse:
		impulse += impulse_grow
