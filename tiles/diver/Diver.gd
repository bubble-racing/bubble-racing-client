extends Wanderer


func get_class():
	return 'Diver'


func _ready():
	$AnimationPlayer.play("swim")


func _on_timeout():
	._on_timeout()
	if control != 'local':
		return
	angular_velocity = angular_velocity + rand_range(-2, 0)
