extends Node2D
class_name Dot

var target           # current target
var speed = 0        # current speed
var accel = .3       # acceleration towards target
var max_alpha = 0.9  # can not have higher alpha than this
var min_alpha = 0    # fully remove dot when alpha reaches min_alpha
var fade_speed = .02 # alpha change per frame
var fade_dist = 50   # start fading when dist < fade_dist
var fade_damp = .97  # slow speed when fading to avoid overshooting the target
var type = "pearls"


func _ready():
	set_process(false)
	$Area2D.connect('body_entered', self, '_on_body_entered')


func _on_body_entered(body):
	if (body.get_class() == 'BubbleLocal'):
		_picked_up(body)
		$Area2D.disconnect('body_entered', self, '_on_body_entered')
		set_process(true)
		
		if Settings.persist.sound_effects:
			$PickupSound.play()
		
		target = body


func _picked_up(body):
	Pickups.stow(type, 1)
	

func _process(_delta):
		
	# remove dot if alpha reaches 0
	if (modulate.a <= min_alpha):
		set_process(false)
		queue_free()
		return
		
	# move towards target bubble
	var vector = (target.position - position).normalized()
	speed += accel
	position += vector * speed
	
	# fade out if near target
	if (target.position.distance_to(position) < fade_dist):
		modulate.a -= fade_speed
		speed = speed * fade_damp
	elif (modulate.a < max_alpha):
		modulate.a += fade_speed
