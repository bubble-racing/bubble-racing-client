extends Node

var EMPTY = 0
var WALL = 1
var PHOSPHORUS = 2
var SPEED = 3
var CHEST = 4
var DEBRIS = 5
var VORTEX = 6
var GHOST = 7
var OIL = 8
var BUBBLE = 9
var BREADCRUMBS = 10
var KEY = 11
var CURRENT_UP = 12
var CURRENT_RIGHT = 13
var CURRENT_DOWN = 14
var CURRENT_LEFT = 15
var TURTLE = 16
var DIVER = 17
var CHERRIES = 18
var STARFISH = 19
