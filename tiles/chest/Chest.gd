extends Node2D


var BubbleFollow = preload("res://bubble/BubbleFollow.tscn")
var target
var open = false
var energy = 0
var max_energy = 100
var energy_speed = 0.4
var color_vec = Vector2(0, 1)
var key_cost = 5
var rng = RandomNumberGenerator.new()


func _ready():
	set_process(false)
	rng.randomize()
	$Area2D.connect('body_entered', self, '_on_body_entered')
	key_cost = rng.randi_range(0, 99)
	$KeyLabel.text = String(key_cost)


func _on_body_entered(body):
	if (!target && 'Bubble' in body.get_class() && key_cost <= Pickups.get_total("keys")):
		set_process(true)
		target = body


func _process(delta):
	if target && !open:
		var dist = position.distance_to(target.position)
		if dist > 150:
			target = null
		elif energy < max_energy:
			energy += energy_speed
		elif energy >= max_energy:
			_open()
	elif energy > 0:
		energy -= energy_speed * 2
		
	color_vec = color_vec.rotated(energy * delta * 0.3).normalized()
	var color_perc = (color_vec.y + 1) / 2
	modulate.r = color_perc


func _open():
	if !open && target:
		open = true
		$ChestClosed.visible = false
		$ChestOpen.visible = true
		energy = max_energy / 2
		
		var nature_points = random_nature_points(key_cost)
		var bubble_data = MyBubbles.generate_random_bubble()
		bubble_data.nature = MyBubbles.generate_nature(nature_points)
		Settings.temp.bubble_in_tow = bubble_data
		Pickups.stow("keys", -key_cost)
		
		var follower = BubbleFollow.instance()
		follower.position = position
		follower.target = target
		get_parent().add_child(follower)
		follower.customize(bubble_data)


func _exit_tree():
	target = null


func random_nature_points(minimum) -> int:
	var r = rng.randi_range(minimum, 100)
	if r >= 99:
		return 9
	elif r >= 89:
		return 8
	elif r >= 78:
		return 7
	elif r >= 67:
		return 6
	elif r >= 56:
		return 5
	elif r >= 45:
		return 4
	elif r >= 34:
		return 3
	elif r >= 23:
		return 2
	elif r >= 12:
		return 1
	else:
		return 0
