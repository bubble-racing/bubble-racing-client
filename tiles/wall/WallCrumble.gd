extends Particles2D


func _ready():
	emitting = true
	$Timer.connect('timeout', self, '_on_timeout')
	if Settings.persist.sound_effects:
		$Smash.play()


func _on_timeout():
	queue_free()
