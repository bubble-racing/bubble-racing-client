extends Node2D


func _ready():
	$Right.l = true
	$Right.redraw()
	
	$Center.t = true
	$Center.r = true
	$Center.b = true
	$Center.l = true
	$Center.redraw()
	
	$Top.b = true
	$Top.redraw()
	
	$Left.r = true
	$Left.redraw()
	
	$Bottom.t = true
	$Bottom.redraw()
