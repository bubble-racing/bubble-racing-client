extends Node2D

var WallCrumble = preload('res://tiles/wall/WallCrumble.tscn')

var tl = false
var t = false
var tr = false
var r = false
var br = false
var b = false
var bl = false
var l = false

var textures = {
	"BL-000": preload("res://tiles/wall/BL-000.png"),
	"BL-001": preload("res://tiles/wall/BL-001.png"),
	"BL-100": preload("res://tiles/wall/BL-100.png"),
	"BL-101": preload("res://tiles/wall/BL-101.png"),
	"BL-111": preload("res://tiles/wall/BL-111.png"),
	"BR-000": preload("res://tiles/wall/BR-000.png"),
	"BR-001": preload("res://tiles/wall/BR-001.png"),
	"BR-100": preload("res://tiles/wall/BR-100.png"),
	"BR-101": preload("res://tiles/wall/BR-101.png"),
	"BR-111": preload("res://tiles/wall/BR-111.png"),
	"TL-000": preload("res://tiles/wall/TL-000.png"),
	"TL-001": preload("res://tiles/wall/TL-001.png"),
	"TL-100": preload("res://tiles/wall/TL-100.png"),
	"TL-101": preload("res://tiles/wall/TL-101.png"),
	"TL-111": preload("res://tiles/wall/TL-111.png"),
	"TR-000": preload("res://tiles/wall/TR-000.png"),
	"TR-001": preload("res://tiles/wall/TR-001.png"),
	"TR-100": preload("res://tiles/wall/TR-100.png"),
	"TR-101": preload("res://tiles/wall/TR-101.png"),
	"TR-111": preload("res://tiles/wall/TR-111.png")
}


func _ready():
	redraw()


func get_class():
	return 'Wall'


func redraw():
	update_section('TL', t, tl, l)
	update_section('TR', t, tr, r)
	update_section('BL', b, bl, l)
	update_section('BR', b, br, r)


func update_section(node_name, top, corner, side):
	var node = get_node(node_name)
	var shape = "{top}{corner}{side}".format({
		"top": bool_to_str(top),
		"corner": bool_to_str(corner),
		"side": bool_to_str(side)
	})
	shape = alias_shapes(shape)
	var texture_name = "{node_name}-{shape}".format({
		"node_name": node_name,
		"shape": shape
	})
	var texture = textures[texture_name]
	node.texture = texture


func bool_to_str(b: bool) -> String:
	if b:
		return "1"
	else:
		return "0"


func alias_shapes(shape: String) -> String:
	if shape == "010":
		return "000"
	elif shape == "011":
		return "001"
	elif shape == "110":
		return "100"
	else:
		return shape


func crumble() -> void:
	var parent = get_parent()
	if parent:
		var crumble = WallCrumble.instance()
		crumble.position = position
		parent.add_child(crumble)
	queue_free()
