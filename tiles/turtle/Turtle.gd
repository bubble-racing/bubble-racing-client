extends Wanderer


func get_class():
	return "Turtle"


func _ready():
	._ready()
	$AnimationPlayer.play('swim')
	stopped_threshold = 20
	connect('body_entered', self, '_on_body_entered')


func _on_timeout():
	._on_timeout()
	if target_position.x > position.x:
		$Holder.scale.x = -1
	else:
		$Holder.scale.x = 1


func _on_body_entered(body: Node):
	# Break walls
	if body.has_method('crumble'):
		body.crumble()
	
	# Bubbles can steer the turtle
	if body.get_class() == 'Bubble' || body.get_class() == 'BubbleLocal':
		target_position = (position - body.position).normalized() * 500
