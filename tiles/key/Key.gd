extends Dot


func _ready():
	type = "keys"


func _process(delta):
	$key.rotation_degrees += delta * 100


func _picked_up(body):
	._picked_up(body)
	body.flash_keys()
