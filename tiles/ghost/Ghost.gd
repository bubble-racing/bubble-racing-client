extends Seeker

var last_direction = 'none'
var travel_dist = 256.0
var rng = RandomNumberGenerator.new()
var directions = {
	'up': Vector2(0, -1),
	'right': Vector2(1, 0),
	'down': Vector2(0, 1),
	'left': Vector2(-1, 0),
	'none': Vector2(0, 0)
}
var max_pos = Vector2(INF, INF)
var min_pos = Vector2(-INF, 0)


func get_class():
	return "Ghost"
	
	
# Init
func _ready():
	target_position = position
	rng.randomize()
	connect('body_entered', self, '_on_body_entered')
	get_parent().connect('cherry_begin', self, '_on_cherry_begin')
	get_parent().connect('cherry_end', self, '_on_cherry_end')
	$AnimationPlayer.play('swim')


func _on_body_entered(body):
	if (body.has_method('impact')):
		if body.get('cherry_countdown') && body.get('cherry_countdown') > 0:
			body.chomp()
			queue_free()
		else:
			var vector: Vector2 = (body.position - position).normalized()
			var hit = body.impact(vector)
			if hit && Settings.persist.sound_effects:
				$WobbleSound.play()


func _on_cherry_begin():
	$Squid.modulate.r = 0.0
	$Squid.modulate.b = 0.8


func _on_cherry_end():
	$Squid.modulate.r = 1.0
	$Squid.modulate.b = 1.0


# Run on a timer
func _on_Timer_timeout() -> void:
	if control != 'local':
		return
		
	# Where we would like to go (yet to be determined)
	var next_direction
	
	# Where we are right now
	var current_position = Vector2(
		round(position.x / travel_dist) * travel_dist,
		round(position.y / travel_dist) * travel_dist
	)
	
	# Poll our senses
	var senses = {
		'up': _is_clear($UpSensor),
		'right': _is_clear($RightSensor),
		'down': _is_clear($DownSensor),
		'left': _is_clear($LeftSensor)
	}
	
	# Enforce boundaries
	if position.x < min_pos.x:
		senses.left = false
	if position.x > max_pos.x:
		senses.right = false
	if position.y < min_pos.y:
		senses.up = false
	if position.y > max_pos.y:
		senses.down = false
	
	# Check if we can keep going the same direction
	if last_direction != 'none' and current_position.is_equal_approx(target_position) and senses[last_direction]:
		next_direction = last_direction
	
	# Otherwise pick a new direction
	else:
		next_direction = _select_a_direction(senses)
		
	# Set target_position based on the selected direction
	target_position = current_position + (directions[next_direction] * travel_dist)
	last_direction = next_direction


func _is_clear(sensor) -> bool:
	var bodies = sensor.get_overlapping_bodies()
	for body in bodies:
		if body is StaticBody2D:
			return false
	return true


func _select_a_direction(senses) -> String:
	# make a list of directions we could choose
	var possible_directions = []
	for direction in senses:
		if senses[direction]:
			possible_directions.append(direction)
		
	# if there are no directions, choose 'none'
	if len(possible_directions) == 0:
		return 'none'
	
	# pick a random direction from the list of possibiliies
	var i = rng.randi_range(0, len(possible_directions) - 1)
	return possible_directions[i]
