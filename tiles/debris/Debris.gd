extends Node2D


var rng = RandomNumberGenerator.new()


func _ready():
	rng.randomize()
	pos($DebrisPiece1)
	pos($DebrisPiece2)
	pos($DebrisPiece3)
	pos($DebrisPiece4)
	pos($DebrisPiece5)


func pos(body: RigidBody2D) -> void:
	body.rotation_degrees = rng.randf_range(0, 360)
