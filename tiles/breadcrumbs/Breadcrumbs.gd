extends Node2D


var Dot = preload("../dot/Dot.tscn")
var count = 12
var radius = 400


func _ready():
	var parent = get_parent()
	
	for i in count:
		var dist = 50
		var angle_rad = (i / float(count)) * (2 * PI)
		var dot_position = Vector2(0, dist).rotated(angle_rad) + position
		var dot = Dot.instance()
		dot.position = dot_position
		parent.call_deferred("add_child", dot)
	
	queue_free()
