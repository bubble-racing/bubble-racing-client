extends Node2D


var angle = Vector2(1, 0)
var rng = RandomNumberGenerator.new()


func _ready():
	$Body.linear_damp = 10000000
	$Body.connect('body_entered', self, '_on_body_entered')
	$Timer.connect('timeout', self, '_end_destruction')
	angle = angle.rotated(rng.randi_range(0, 360))
	
	var bodies = $Body.get_colliding_bodies()
	for body in bodies:
		if body.has_method('crumble'):
			body.crumble()


func _process(delta):
	angle = angle.rotated(delta / 4)
	$Body.angular_velocity = angle.x * 7
	# $Body.rotation_degrees += angle.x * 2


func _on_body_entered(body: Node):
	if body.has_method('crumble'):
		body.crumble()


func _end_destruction():
	$Body.disconnect('body_entered', self, '_on_body_entered')
