extends Node2D

var rng = RandomNumberGenerator.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	# start out will all of the rocks invisible
	$rock1.visible = false
	$rock2.visible = false
	$rock3.visible = false
	
	# pick one rock at random
	rng.randomize()
	var rock_num = rng.randi_range(1, 3)
	var rock = self.get_node("rock" + str(rock_num))
	
	# make the one lucky rock visible
	rock.visible = true
	
	# flip horizontally and vertically
	rock.flip_h = rng.randi_range(0, 1)
	rock.flip_v = rng.randi_range(0, 1)
	
	# color shift
	modulate.r = .80 + rng.randf_range(0, 0.30)
	modulate.g = .60 + rng.randf_range(0, 0.40)
	modulate.b = .80 + rng.randf_range(0, 0.30)
	
	# scale a bit
	rock.scale.x += rng.randf_range(0, 0.3)
	rock.scale.y += rng.randf_range(0, 0.3)
	
	# rotate a bit
	rock.rotation_degrees = rng.randi_range(0, 3) * 90
	rock.rotation_degrees += rng.randf_range(-5.0, 5.0)
	
	# Rotate the light occluder
	$LightOccluder2D.rotation_degrees = rng.randi_range(0, 3) * 90
