extends Node2D
class_name LevelSolver

var UP = 'up'
var RIGHT = 'right'
var DOWN = 'down'
var LEFT = 'left'
var rng = RandomNumberGenerator.new()
var vectors = {
	UP: Vector2(0, -1),
	RIGHT: Vector2(1, 0),
	DOWN: Vector2(0, 1),
	LEFT: Vector2(-1, 0)
}
var opposites = {
	UP: DOWN,
	RIGHT: LEFT,
	DOWN: UP,
	LEFT: RIGHT
}
var memory = Map2D.new()
var prefered_option = ''
var current_position = Vector2(0, 0)
var map: Map2D


func _init():
	rng.randomize()


func activate(_map, _position) -> void:
	map = _map
	current_position = _position
	

func get_next_position() -> Vector2:
	var option = choose_option(map, current_position, prefered_option, memory)
	prefered_option = option
	var direction = vectors[option]
	current_position = current_position + direction
	return current_position


func get_options(map: Map2D, pos: Vector2) -> Array:
	var options = []
	if map.is_tile_passable(pos.x, pos.y - 1):
		options.append(UP)
	if map.is_tile_passable(pos.x + 1, pos.y):
		options.append(RIGHT)
	if map.is_tile_passable(pos.x, pos.y + 1):
		options.append(DOWN)
	if map.is_tile_passable(pos.x - 1, pos.y):
		options.append(LEFT)
	return options


func weigh_options(pos: Vector2, options: Array, memory: Map2D) -> Array:
	var min_weight = INF
	var results = {}
	var good_options = []
	
	for option in options:
		var new_pos = pos + vectors[option]
		var weight = memory.get_tile(new_pos.x, new_pos.y)
		if weight < min_weight:
			min_weight = weight
		results[option] = weight
	
	for option in options:
		var weight = results[option]
		if weight == min_weight:
			good_options.append(option)
	
	return good_options


func choose_option(map: Map2D, pos: Vector2, preferred_option: String, memory: Map2D) -> String:
	var options = get_options(map, pos)
	var weight = memory.get_tile(pos.x, pos.y)
	var selected_option: String = ''
	
	# bump the weight of the current tile
	memory.set_tile(pos.x, pos.y, weight + 5 - options.size())
	
	# filter out paths that have been visited frequently
	options = weigh_options(pos, options, memory)
	
	# if we are at the exit, go for it
	if pos.y <= 1 && options.has(UP):
		selected_option = UP
	
	# keep going the same way if possible, but mix it up every now and again
	elif options.has(preferred_option) and rng.randi_range(0, 9) < 9:
		selected_option = preferred_option
		
	# if up is an option, it is usually the right one
	elif options.has(UP):
		selected_option = UP
	
	# otherwise pick one option at random
	else:
		selected_option = options[rng.randi() % options.size()]
	
	#
	return selected_option
