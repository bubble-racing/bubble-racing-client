extends Node2D

var LevelGenerator = preload("../level_generator/LevelGenerator.gd")
var SinglePlayerLevels = preload("../level_generator/SinglePlayerLevels.gd")
var Triangle = preload("../basic_shapes/Triangle.png")

var level_solver = LevelSolver.new()
var level_generator = LevelGenerator.new()
var level_settings = SinglePlayerLevels.new().level_settings
var path: Array
var i = 0


func _ready():
	var map = level_generator.generate({width = 27, height = 18})
	path = level_solver.solve(map, map.find_start_position())
	# path = level_solver.perfect_solve(map, map.find_start_position(), Vector2(13, 0))
	$LevelRenderer.render_map(map, 'local')
	$Timer.connect("timeout", self, "_on_timeout")


func _on_timeout():
	if i < path.size():
		var step = path[i]
		i += 1
		
		var sprite = Sprite.new()
		sprite.texture = Triangle
		sprite.position.x = step.x * $LevelRenderer.tile_size
		sprite.position.y = step.y * $LevelRenderer.tile_size
		sprite.self_modulate.a = 0.30
		sprite.scale = Vector2(3, 3)
		$LevelRenderer.add_child(sprite)
		
