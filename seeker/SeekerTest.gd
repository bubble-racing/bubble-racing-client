extends Node2D

var rng = RandomNumberGenerator.new()

func _on_Timer_timeout():
	var x = rng.randi_range(100, 400)
	var y = rng.randi_range(100, 400)
	$Seeker.target_position = Vector2(x, y)
