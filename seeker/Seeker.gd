extends RigidBody2D
class_name Seeker

# Affects how long it takes for an object to go from 0 to max velocity
# Larger number = faster acceleration
var acceleration = 25

# This is the force of friction, which is always trying to bring an 
# object to a stop. Larger number = more friction
var base_linear_damp = 3

# Once the seeker is within this X pixels of the target, it starts to slow down
# to avoid overshooting the target_position
var throttle_dist = 125

# Used by multiplayer system to keep seekers synchronized
var nudge_vel = Vector2(0, 0)
var nudge_strength = 0.2
var nudge_vel_decay = 0.9

## The x,y coordinates that the seeker should be trying to reach
var target_position = Vector2(0 ,0)

# Can be local or remote
var control = 'remote'


# Called when the node enters the scene tree for the first time.
func _ready():
	set_linear_damp(base_linear_damp)
	target_position = position


# Move towards target position
func _physics_process(delta):
	var vector = (target_position - position).normalized()
	var dist = target_position.distance_to(position)
	var throttle = min(dist, throttle_dist) / throttle_dist
	var impulse = (vector * acceleration * throttle) + nudge_vel
	nudge_vel *= nudge_vel_decay
	apply_central_impulse(impulse * 60 * delta)


# Used to gently keep seeker synchronized with server
func nudge(update: Array):
	
	# feels bad if we let remote change local controlls
	if control == 'local':
		return
		
	# set target position to match whoever does have control
	target_position.x = update[2]
	target_position.y = update[3]
	
	# account for drift
	var server_position = Vector2(update[0], update[1])
	var dist = position.distance_to(server_position)
	if dist > 500:
		position = server_position
		nudge_vel = Vector2(0, 0)
	else:
		nudge_vel = (server_position - position) * nudge_strength


func get_update() -> Array:
	return [round(position.x), round(position.y), round(target_position.x), round(target_position.y)]
