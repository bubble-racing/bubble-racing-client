# Legend of the Seeker

![legend of the seeker TV show](readme_images/legend-of-the-seeker.jpg)

Err, OK. Not that seeker. (Did anyone else watch that show?)

This seeker will be a hidden helper within our game. It tries to get to a `target_position`, but it will be stopped by `walls` and other such things. The behaviour will come in handy, and we'll use it later to make `bubbles` and `fish`.

## Make the Seeker class

So really the Seeker is all about the code. But we will give the seeker a face regardless, mostly because it will make the test in the next section look better. Here comes a numbered list!

1. Make a new folder called `seeker`
2. Make a new Scene named `Seeker`, and save it in the `seeker` folder.
4. Pick 2DNode to start with, but you'll have to change to root node to `RigidPhysicsBody`
5. Download this image of a charming broomstick riding individual from above and move it to the `seeker` folder. ![stick figure riding a broomstick](seeker.png)
6. Drag the charming broomstick riding individual into your `Seeker` scene.
7. Click on the funny little parchment paper on the top left. It's the one with a green x.
8. Go with the default options, but save the new script in the `seeker` folder.
9. Copy and paste [this fine code](Seeker.gd)

[![create-seeker](readme_images/create-seeker.gif)](https://tube.jigg.io/videos/watch/f92915dc-807a-4004-9447-caa19fef4a47)

## Test it Out

To test our seeker out, we're going to build a little obsticle course. Then we're going to tell the seeker to fly around completely at random. Nice.

1. Create a new Scene named `SeekerTest`. Save it in the `seeker` folder.
2. Drag some `walls` into the scene. We can't let those walls be slacking off, now can we?
3. Drag a `Seeker` into the scene.
4. Create a new script for SeekerTest and copy and paste [this script](SeekerTest.gd).
5. Add a Timer to the scene, and set the timer to automatically start.
6. Link the timer's `timeout` signal to the SeekerTest script. This will run the code in that timeout handler once per second (by default).
7. Hit the `Play Scene` button on the top right to send our charming broomstick riding individual flying.
8. Congratulations! You now have done a thing.

[![test-seeker](readme_images/test-seeker.gif)](https://tube.jigg.io/videos/watch/13281bd5-30d0-4514-b73f-eb415fc12262)