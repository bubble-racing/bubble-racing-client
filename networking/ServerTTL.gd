extends Node2D


func _ready():
	if '--server' in OS.get_cmdline_args():
		print('Server will try to shutdown after ', $SoftTTL.wait_time, ' seconds, and force shutdown after ', $ForceTTL.wait_time, ' seconds.')
		$SoftTTL.connect('timeout', self, '_on_soft_ttl')
		$SoftTTL.start()
		$ForceTTL.connect('timeout', self, '_on_force_ttl')
		$ForceTTL.start()


func _on_soft_ttl():
	print('Server is pending shutdown, waiting for the next game to end')
	Settings.temp.shutdown_pending = true


func _on_force_ttl():
	print('Server is shutting down now!')
	get_tree().notification(MainLoop.NOTIFICATION_WM_QUIT_REQUEST)
