extends Node2D

var LevelGenerator = preload("../level_generator/LevelGenerator.gd")
var level_generator = LevelGenerator.new()
var player_one_name: String = 'player1'
var player_two_name: String = 'player2'
var player_one_latency:int = 250
var player_two_latency:int = 250
var BubbleWander = preload('../bubble/BubbleWander.tscn')
var map: Map2D



# Called when the node enters the scene tree for the first time.
func _ready():
	var settings = {
		'width': 9,
		'height': 12,
		'ghost': 5,
		'speed': 2
	}
	map = level_generator.generate(settings)
	$Player1LevelRenderer.render_map(map, 'remote')
	yield($Player1LevelRenderer, 'render_complete')
	$Player2LevelRenderer.render_map(map, 'remote')
	yield($Player2LevelRenderer, 'render_complete')
	$ServerLevelRenderer.render_map(map, 'local')
	yield($ServerLevelRenderer, 'render_complete')
	
	add_control_bubble(player_one_name, $Player1LevelRenderer, $Player1Watcher)
	add_remote_bubble(player_one_name, $Player2LevelRenderer)
	add_server_bubble(player_one_name, $ServerLevelRenderer)
	
	add_control_bubble(player_two_name, $Player2LevelRenderer, $Player2Watcher)
	add_remote_bubble(player_two_name, $Player1LevelRenderer)
	add_server_bubble(player_two_name, $ServerLevelRenderer)
	
	$ServerWatcher.set_target_container($ServerLevelRenderer)
	$ServerWatcher.start()


func add_control_bubble(name, renderer, watcher):
	var bubble = renderer.add_bubble(name, 'wander')
	watcher.target = bubble
	bubble.modulate.b = 0


func add_remote_bubble(name, renderer):
	var _bubble = renderer.add_bubble(name, 'remote')
	
	
func add_server_bubble(name, renderer):
	var _bubble = renderer.add_bubble(name, 'server')


func _on_Player1Watcher_update(update: Array):
	# Simulate netowrk latency
	yield(get_tree().create_timer(player_one_latency / 1000.0), "timeout")
	_on_client_update(player_one_name, update)


func _on_Player2Watcher_update(update: Array):
	# Simulate network latency
	yield(get_tree().create_timer(player_two_latency / 1000.0), "timeout")
	_on_client_update(player_two_name, update)


func _on_client_update(name: String, update: Array) -> void:
	var node: Seeker = $ServerLevelRenderer.get_node(name)
	node.update_from_client(update)


func _on_ServerWatcher_update(ms: int, updates: Dictionary) -> void:
	for key in updates.keys():
		var update: Array = updates[key]
		var node1: Seeker = get_node("Player1LevelRenderer/{key}".format({"key": key}))
		var node2: Seeker = get_node("Player2LevelRenderer/{key}".format({"key": key}))
		_update_node(node1, update, player_one_latency)
		_update_node(node2, update, player_two_latency)


func _update_node(node: Seeker, update: Array, latency: int) -> void:
	yield(get_tree().create_timer(latency / 1000.0), "timeout") # Simulate netowrk latency
	node.nudge(update)


func _on_Player2Latency_text_changed(new_text):
	player_two_latency = int(new_text)


func _on_Player1Latency_text_changed(new_text):
	player_one_latency = int(new_text)
