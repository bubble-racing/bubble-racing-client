extends Node2D

var port = 9999
var protocols = []
var gd_mp_api = true


func _ready():
	if not '--server' in OS.get_cmdline_args():
		return
	var peer = WebSocketServer.new()
	peer.listen(port, protocols, gd_mp_api)
	get_tree().network_peer = peer
	print('Listening on port ', port)
