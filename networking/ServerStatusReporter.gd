extends Node2D

var late_join_seconds = 10


func _ready():
	if not '--server' in OS.get_cmdline_args():
		return
	$Timer.connect('timeout', self, '_on_timeout')
	_on_timeout()


func _on_timeout():
	var gameserver_id = int(OS.get_environment('GAMESERVER_ID'))
	var body = {
		'gameserver_id': gameserver_id,
		'player_count': ServerUsers.get_user_count(),
		'is_open': _is_open(),
		'address': OS.get_environment('SELF_URL') + '/' + str(gameserver_id),
		'port': 443
	}
	var headers = [
		"Gameserver-Key: " + OS.get_environment('GAMESERVER_KEY')
	]
	var url = OS.get_environment('API_URL') + '/gameservers/' + String(gameserver_id)
	print('sending put ' + url + ' ' + to_json(body))
	$HTTPRequest.request(url, headers, false, HTTPClient.METHOD_PUT, to_json(body))


func _on_HTTPRequest_request_completed(result, response_code, _headers, body):
	print("response: " + String(result) + " " + String(response_code) + " " + body.get_string_from_utf8())


func _is_open() -> bool:
	if !ServerGame.game:
		return true
	return ServerGame.started_at > OS.get_unix_time() - late_join_seconds


func stop():
	$Timer.stop()
