extends Node2D

# Imports
var LevelGenerator = preload("../../level_generator/LevelGenerator.gd")
var Bubble = preload('../bubble/Bubble.tscn')

#
var levels = {}
var level_generator = LevelGenerator.new()
var selected_level_data = null
var start_position = Vector2(0, -400)


func _ready():
	if not '--server' in OS.get_cmdline_args():
		Logger.debug('ServerLevelVoter skipped')
		return
	
	Logger.debug('ServerLevelVoter initializing')
	RPC.connect('get_level_options', self, '_get_level_options')
	RPC.connect('vote_on_level', self, '_vote_on_level')
	$VotingCutoff.connect('timeout', self, '_stop')
	$UpdateInterval.connect('timeout', self, '_send_votes')
	ServerUsers.connect('user_added', self, '_user_added')
	ServerUsers.connect('user_removed', self, '_user_removed')


func _get_level_options():
	Logger.debug('ServerLevelVoter::_get_level_options')
	var id = get_tree().get_rpc_sender_id()
	
	# Let user know voting is over, there is an active game to join
	if ServerGame.game:
		Logger.debug('ServerLevelVoter::_get_level_options - voting is over, they should go to the game')
		RPC.rpc_id(id, 'get_level_options_result', {}, 0)
		return
	
	# If this is the first user to join, start up level voting
	if !levels.has('new'):
		_start()
		
	RPC.rpc_id(id, 'get_level_options_result', levels, int($VotingCutoff.time_left))


func _start():
	Logger.debug('ServerLevelVoter::_start')
	levels.new = LevelDB.get_new_level()
	levels.popular = LevelDB.get_popular_level()
	levels.random = _create_random_map().export_data()
	
	if levels.popular:
		levels.popular = levels.popular.export_data()
	else:
		levels.popular =  _create_random_map().export_data()
		levels.popular.category = 'popular'
	
	if levels.new:
		levels.new = levels.new.export_data()
	else:
		levels.new =  _create_random_map().export_data()
		levels.new.category = 'new'
	
	# Start countdown to cut off new players
	$UpdateInterval.start()
	$VotingCutoff.start()
	RPC.connect('update_player_pos', self, '_update_player_pos')
	RPC.connect('update_nodes', self, '_update_nodes')
	ServerWatcher.set_target_container($Bubbles)
	ServerWatcher.connect('update', self, '_on_watcher_update')
	ServerWatcher.start()


func _stop():
	Logger.debug('ServerLevelVoter::_stop')
	var category = _get_category_with_most_votes()
	selected_level_data = levels[category]
	levels = {}
	$UpdateInterval.stop()
	$VotingCutoff.stop()
	ServerWatcher.stop()
	ServerWatcher.disconnect('update', self, '_on_watcher_update')
	for child in $Bubbles.get_children():
		child.queue_free()
	ServerGame.init(selected_level_data)
	RPC.rpc('level_vote_result', category)
	RPC.disconnect('update_player_pos', self, '_update_player_pos')


func _get_category_with_most_votes() -> String:
	Logger.debug('ServerLevelVoter::_get_category_with_most_votes')
	var most_votes = 0
	var selected_level_category = 'random'
	var votes = _tally_votes()
	for level_id in votes:
		var vote_count = votes[level_id]
		if vote_count > most_votes:
			most_votes = vote_count
			selected_level_category = level_id
	return selected_level_category


func _tally_votes() -> Dictionary:
	Logger.debug('ServerLevelVoter::_tally_votes')
	var votes = {}
	for id in ServerUsers.users:
		var user = ServerUsers.users[id]
		var vote = user.get('vote', false)
		if vote:
			votes[vote] = 1 + votes.get(vote, 0)
	return votes


func _vote_on_level(vote: String) -> void:
	Logger.debug('ServerLevelVoter::_vote_on_level', vote)
	# setup vars
	var id = get_tree().get_rpc_sender_id()
	var user = ServerUsers.users[id]
	
	# sanity checks
	if !user:
		return
	
	user.vote = vote


func _create_random_map():
	Logger.debug('ServerLevelVoter::_create_random_map')
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var map = level_generator.generate({
		width = rng.randi_range(2, 5) * 3,
		height = rng.randi_range(10, 20) * 3,
		phosphorus = rng.randi_range(0, 12),
		speed = rng.randi_range(0, 10),
		current_up = rng.randi_range(0, 8),
		current_right = rng.randi_range(0, 2),
		current_down = rng.randi_range(0, 2),
		current_left = rng.randi_range(0, 2),
		debris = rng.randi_range(0, 12),
		vortex = rng.randi_range(0, 12),
		ghost = rng.randi_range(0, 12),
		oil = rng.randi_range(0, 12),
		blockers = rng.randi_range(0, 10),
		diver = max(rng.randi_range(-1, 1), 0),
		turtle = max(rng.randi_range(-2, 1), 0)
	})
	map.user_nickname = 'Randomizer'
	map.category = 'random'
	return map


func _send_votes() -> void:
	var votes = _tally_votes()
	Logger.debug('ServerLevelVoter::_send_votes', votes)
	RPC.rpc('level_vote_update', votes)


func _user_added(id: int, bubble_info: Dictionary):
	Logger.debug('ServerLevelVoter::_user_added')
	var bubble:Bubble = Bubble.instance()
	bubble.position = Vector2(start_position.x, start_position.y)
	bubble.customize(bubble_info)
	bubble.name = String(id)
	bubble.min_y = -9999
	$Bubbles.add_child(bubble)


func _user_removed(id: int):
	Logger.debug('ServerLevelVoter::_user_removed')
	var bubble:Bubble = $Bubbles.get_node(String(id))
	if bubble:
		remove_child(bubble)


func _on_watcher_update(_ms: int, updates: Dictionary) -> void:
	RPC.rpc('update_nodes', updates)


func _update_player_pos(update: Array):
	var id = get_tree().get_rpc_sender_id()
	var bubble = $Bubbles.get_node(String(id))
	if bubble:
		bubble.target_position.x = update[2]
		bubble.target_position.y = update[3]
