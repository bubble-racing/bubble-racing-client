extends Node2D

signal update
var targets: Array
var target_container: Node2D


func start():
	$ScanContainerTimer.connect('timeout', self, '_scan_target_container')
	$UpdateTimer.connect('timeout', self, '_send_update')


func stop():
	$ScanContainerTimer.disconnect('timeout', self, '_scan_target_container')
	$UpdateTimer.disconnect('timeout', self, '_send_update')


func set_target_container(container: Node2D) -> void:
	target_container = container
	_scan_target_container()


func _scan_target_container() -> void:
	if !target_container:
		return
	targets = []
	for child in target_container.get_children():
		if child is Seeker:
			targets.append(child)


func _send_update() -> void:
	var updates = {}
	for target in targets:
		updates[target.name] = target.get_update()
	emit_signal('update', OS.get_ticks_msec(), updates)
