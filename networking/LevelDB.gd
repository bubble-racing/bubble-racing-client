extends Node

var levels = []
var rng = RandomNumberGenerator.new()
var max_level_count = 250
var popular_cutoff = 5


func _ready():
	rng.randomize()


func add_level(level: Dictionary): 
	level.hash = to_json(level).sha256_text()
	level.votes = 0
	
	for existing_level in levels:
		if existing_level.hash == level.hash:
			break
			
	levels.push_back(level)
	
	if len(levels) > max_level_count:
		levels.pop_front()


func compare_by_votes(a, b):
	return a.votes > b.votes


func get_popular_level():
	if len(levels) == 0:
		return null
	var sorted = levels.duplicate().sort_custom(self, 'compare_by_votes')
	var popular = sorted.slice(0, popular_cutoff)
	return popular[rng.randi_range(0, popular_cutoff)]


func get_new_level():
	if len(levels) == 0:
		return null
	return levels[len(levels) - 1]
