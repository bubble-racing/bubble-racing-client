extends Node2D

var Game = preload('res://pages/game/Game.tscn')
var game
var server_watcher
var started_at
var rng: RandomNumberGenerator
var bot_count = 0


func _ready():
	rng = RandomNumberGenerator.new()
	rng.randomize()
	$BotTimer.connect('timeout', self, '_add_bot')

	# Listen for incoming events
	RPC.connect('update_player_pos', self, '_update_player_pos')
	RPC.connect('join_game', self, '_join_game')
	RPC.connect('start_game', self, '_start_game')
	ServerUsers.connect('user_added', self, '_user_added')
	ServerUsers.connect('user_removed', self, '_user_removed')


func init(level_data: Dictionary) -> void:
	Logger.info('ServerGame::init')
	
	if game:
		Logger.info('ServerGame::start - bail out, game is already running')
		return
	
	# Create game
	game = Game.instance()
	game.map = Map2D.new()
	game.map.import_data(level_data)
	add_child(game)
	game.level_renderer.control = 'local'
	game.level_renderer.render_map_sync(game.map)


func _add_bot():
	print('_add_bot')
	if !game || !game.map:
		return
	if bot_count + len(ServerUsers.users) >= 4:
		$BotTimer.stop()
		return
	
	bot_count += 1
	var bot_id = 100 + bot_count
	var bubble_info = MyBubbles.generate_random_bubble()
	var bot_bubble = game.level_renderer.add_bubble(String(bot_id), 'bot')
	bubble_info.nurture.speed = rng.randi_range(0, 2)
	bubble_info.nurture.tough = rng.randi_range(0, 2)
	bubble_info.nurture.luck = rng.randi_range(0, 2)
	bot_bubble.customize(bubble_info)
	RPC.rpc('add_user', bot_id, bubble_info)
	bot_bubble.activate_autopilot(game.map)
	print('---------------------')


func stop() -> void:
	Logger.info('ServerGame::stop')
	if game:
		game.queue_free()
		game = null
	
	started_at = 0
	bot_count = 0
	ServerWatcher.stop()
	ServerWatcher.disconnect('update', self, '_on_server_watcher_update')
	$BotTimer.stop()
	
	# exit if ttl is up
	if Settings.temp.get('shutdown_pending', false):
		get_tree().notification(MainLoop.NOTIFICATION_WM_QUIT_REQUEST)


func _join_game():
	Logger.info('ServerGame::_join_game')
	if !game:
		Logger.info('ServerGame::_join_game', 'no game, skipping')
		return
	
	var id = get_tree().get_rpc_sender_id()
	RPC.rpc_id(id, 'join_game_result', game.map.export_data())


func _start_game():
	if started_at:
		return
	
	# Add users
	for id in ServerUsers.users:
		var user = ServerUsers.users[id]
		_user_added(id, ServerUsers.users[id])
	
	# Send updates to clients
	ServerWatcher.set_target_container(game.level_renderer)
	ServerWatcher.connect('update', self, '_on_server_watcher_update')
	ServerWatcher.start()

	# Add bots
	$BotTimer.start()
	
	# Track when this game started
	started_at = OS.get_unix_time()


func _update_player_pos(update: Array):
	Logger.debug('_update_player_pos')
	var id = get_tree().get_rpc_sender_id()
	if game:
		var puppet_bubble = game.level_renderer.get_node(String(id))
		if puppet_bubble:
			puppet_bubble.update_from_client(update)


func _on_server_watcher_update(_ms: int, updates: Dictionary) -> void:
	Logger.info('ServerGame::_on_server_watcher_update', updates)
	RPC.rpc('update_nodes', updates)


func _user_added(id: int, bubble_info: Dictionary):
	Logger.info('ServerGame::_user_added')
	if !game:
		return
	
	# Add a new bubble to the server
	var puppet_bubble = game.level_renderer.add_bubble(String(id), 'server')
	puppet_bubble.customize(bubble_info)


func _user_removed(id: int) -> void:
	if !game:
		return
		
	# Remove user's bubble
	var puppet_bubble = game.level_renderer.get_node(String(id))
	if puppet_bubble:
		puppet_bubble.queue_free()
	
	# Remove user from clients
	RPC.rpc('remove_user', id)
	
	# End the game if there are no users left
	if ServerUsers.get_user_count() == 0:
		stop()


func _get_game_status():
	if !game:
		return
		
	# setup vars
	var id = get_tree().get_rpc_sender_id()
	
	# send map
	var map_data = game.map.data
	RPC.rpc_id(id, 'get_game_status_response', map_data)

