extends Node2D

var tree: SceneTree


func _ready():
	tree = get_tree()


func _process(_delta):
	if tree.network_peer:
		tree.network_peer.poll()


func open(address: String):
	close()
	
	Logger.info('ClientConnection::open', address)
	
	var peer = WebSocketClient.new()
	var protocols = []
	var gd_mp_api = true
	
	peer.connect_to_url(address, protocols, gd_mp_api)
	get_tree().network_peer = peer


func close():
	if tree.network_peer:
		Logger.info('ClientConnection::close')
		tree.network_peer.disconnect_from_host()
		tree.network_peer = null
