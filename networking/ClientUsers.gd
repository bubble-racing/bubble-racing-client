extends Node2D

signal add_user
signal remove_user

var users = {}


func _ready():
	RPC.connect('add_user', self, '_add_user')
	RPC.connect('remove_user', self, '_remove_user')


func _add_user(id, account):
	Logger.debug('ClientUsers::_add_user', id, account)
	if !users.get(id):
		users[id] = account
		emit_signal('add_user', id, account)


func _remove_user(id):
	if users.get(id):
		users.erase(id)
		emit_signal('remove_user', id)


func clear() -> void:
	users = {}
