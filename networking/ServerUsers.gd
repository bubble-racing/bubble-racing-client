extends Node2D

signal user_added
signal user_removed

var users = {}
var max_users = 6


func _ready():
	if not '--server' in OS.get_cmdline_args():
		return
	get_tree().connect('network_peer_connected', self, '_peer_connected')
	get_tree().connect('network_peer_disconnected', self, '_peer_disconnected')
	RPC.connect('register_user', self, '_register_user')
	

func _peer_connected(id):
	print('Peer Connected: ' + str(id))


func _peer_disconnected(id):
	print('Peer Disconnected: ' + str(id))
	if users.get(id):
		users.erase(id)
		emit_signal('user_removed', id)


func _register_user(info):
	print('_register_user ', info)
	var id = get_tree().get_rpc_sender_id()
	
	# prevent same user from registering twice
	if users.has(id):
		RPC.rpc_id(id, 'register_user_result', 'can not register user, they are already registered')
		return
	
	# prevent more than max_users registering
	if len(users.keys()) >= max_users:
		RPC.rpc_id(id, 'register_user_result', 'can not register user, max_users reached')
		return
	
	# register
	users[id] = info.bubble
	if info.level:
		info.level.player_nickname = info.bubble.name
		LevelDB.add_level(info.level)

	# send back a response to just the new user
	RPC.rpc_id(id, 'register_user_result', 'success')
	
	# send user to all other members
	RPC.rpc('add_user', id, info.bubble)
	
	# send existing members to new user
	for existing_user_id in users:
		if id != existing_user_id:
			var existing_user = ServerUsers.users[existing_user_id]
			RPC.rpc_id(id, 'add_user', existing_user_id, existing_user)
	
	# let the rest of the server know
	emit_signal('user_added', id, info.bubble)


func get_user_count() -> int:
	return len(users.keys())
