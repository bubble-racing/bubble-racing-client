extends Node2D

var ClientWatcher = preload('./ClientWatcher.tscn')
var client_watcher


func init():
	var game = get_parent()
	client_watcher = ClientWatcher.instance()
	add_child(client_watcher)
	game.connect('begin', self, '_on_begin')
	client_watcher.connect('update', self, '_on_update')
	RPC.connect('update_nodes', self, '_update_nodes')
	RPC.connect('join_game_result', self, '_join_game_result')
	ClientUsers.connect('add_user', self, '_add_user')
	RPC.connect('remove_user', self, '_remove_user')
	RPC.rpc_id(1, 'join_game')


func _on_update(update: Array):
	RPC.rpc_id(1, 'update_player_pos', update)


func _update_nodes(updates):
	for key in updates.keys():
		var update: Array = updates[key]
		var node: Seeker = get_node('../LevelRenderer/{key}'.format({'key': key}))
		if node:
			node.nudge(update)


func _join_game_result(map_data: Dictionary) -> void:
	Logger.info('ClientSetup::_join_game_result')

	var game = get_parent()
	if !game:
		return
		
	var map = Map2D.new()
	map.import_data(map_data)
	game.map_data = map.data.duplicate(true)
	map = KeyDropper.drop(map)
	
	game.render_level(map)


func _add_user(id, account):
	Logger.info('ClientSetup::_add_user', id, account)
	var my_id = get_tree().get_network_unique_id()
	var game = get_parent()
	
	if id != my_id && game.local_bubble:
		Logger.debug('ClientSetup::_add_user - all good, adding user bubble')
		var bubble = game.level_renderer.add_bubble(String(id), 'puppet')
		bubble.customize(account)


func _remove_user(id):
	var bubble = get_parent().level_renderer.get_node(String(id))
	if bubble:
		bubble.queue_free()


func _on_begin():
	Logger.info('ClientSetup::_on_begin')
	var game = get_parent()
	if !game || !game.local_bubble:
		return
	
	Logger.info('ClientSetup::_on_begin - adding existing users')
	client_watcher.target = game.local_bubble
	for id in ClientUsers.users:
		var account = ClientUsers.users[id]
		_add_user(id, account)
