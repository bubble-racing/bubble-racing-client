extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	$ClientWatcher.target = $Bubble
	$Bubble.control = 'local'


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var one_second_ago = OS.get_ticks_msec() - 1000
	var past_position = $ClientWatcher.get_past_position(one_second_ago)
	$dot.position = past_position
