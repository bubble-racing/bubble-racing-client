extends Node2D


# ------------------------------------
# Client -----------------------------
# ------------------------------------
signal add_user
puppet func add_user(id, account):
	Logger.info('RPC::add_user', id)
	emit_signal('add_user', id, account)

signal remove_user
puppet func remove_user(id):
	Logger.info('RPC::remove_user', id)
	emit_signal('remove_user', id)

signal update_nodes
puppet func update_nodes(data: Dictionary):
	Logger.debug('RPC::update_nodes')
	emit_signal('update_nodes', data)

signal join_game_result
puppet func join_game_result(level_data: Dictionary) -> void:
	Logger.info('RPC::join_game_result')
	emit_signal('join_game_result', level_data)

signal get_level_options_result
puppet func get_level_options_result(levels: Dictionary, time_left: int) -> void:
	Logger.info('RPC::get_level_options_result')
	emit_signal('get_level_options_result', levels, time_left)

signal level_vote_update
puppet func level_vote_update(votes: Dictionary) -> void:
	Logger.info('RPC::level_vote_update', votes)
	emit_signal('level_vote_update', votes)

signal level_vote_result
puppet func level_vote_result(category: String) -> void:
	Logger.info('RPC::level_vote_result', category)
	emit_signal('level_vote_result', category)

signal register_user_result
puppet func register_user_result(status: String) -> void:
	emit_signal('register_user_result', status)


# --------------------------------------
# Server -------------------------------
# --------------------------------------
signal register_user
master func register_user(data) -> void:
	Logger.debug('RPC::register_user')
	emit_signal('register_user', data)

signal get_level_options
master func get_level_options() -> void:
	Logger.debug('RPC::get_level_options')
	emit_signal('get_level_options')

signal update_player_pos
master func update_player_pos(data) -> void:
	Logger.debug('RPC::update_player_pos')
	emit_signal('update_player_pos', data)

signal join_game
master func join_game() -> void:
	Logger.debug('RPC::join_game')
	emit_signal('join_game')

signal start_game
master func start_game() -> void:
	Logger.debug('RPC::start_game')
	emit_signal('start_game')

signal vote_on_level
master func vote_on_level(vote: String) -> void:
	Logger.debug('RPC::vote_on_level')
	emit_signal('vote_on_level', vote)
