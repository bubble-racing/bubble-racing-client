extends Node2D

signal update

var target: Seeker = null


func _on_Timer_timeout():
	if target:
		emit_signal("update", target.get_update())
