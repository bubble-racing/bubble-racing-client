extends Node2D

var initialScale = Vector2(1, 1)

func _on_RaceButton_pressed():
	$Music.play_race()

func _on_TitleButton_pressed():
	$Music.play_title()

func _process(_delta):
	$Wall.scale = initialScale + (initialScale * $Music.loudness / 100)

func _on_DampenButton_pressed():
	$Music.dampen(30, 0.5)
