## Music Credits & Licenses

# Music
Music is a relatively simple thing, but we're going to make it way complicated. Because yeahhhhh.

[Click here to watch the video tutorial](https://tube.jigg.io/videos/watch/b023941d-dff2-48c0-85d7-7e33cff8f841)

Or read on for the text tutorial.

## Finding songs
Finding songs was a bit tricky, because my normal methods of finding songs aren't going to cut it for this game. My normal methods are:

1. Buy a license for a song. There are websites dedicated to this sort of thing, and its super easy. The problem is that the license wouldn't allow me to re-distribute the song in these tutorials or in Bubble Racing's code repo. Making these tutorials is kind of the whole reason I'm making the game, so this option was out.
2. Find a cool song and ask the creator if I can use it. This method works great, but now that I'm old and crudgy I really prefer to have a license for legal security.

I'm licensing the art in this game under the Creative Commons [CC BY](https://creativecommons.org/licenses/by/4.0/) license, so I thought maybe I could find some songs that were licensed the same way. As it turns out, there are lots of awesome songs licensed with a CC BY license on youtube and soundcloud. I had a fun time listening through them and picking some out. I spent way to much time doing that, really.

### The Intro we Have Been Waiting For
Composed By: [Kevin Shrout](https://soundcloud.com/Kevin-Shrout)
License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)
Source: [https://soundcloud.com/unminus/the-intro-we-have-been-waiting-for](https://soundcloud.com/unminus/the-intro-we-have-been-waiting-for)

### Dance of the Pixies
Composed By: [Jens Kiilstofte](https://machinimasound.com/about/)
License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)
Source [https://machinimasound.com/?s=dance+of+the+pixies](https://machinimasound.com/?s=dance+of+the+pixies)

### Neuro Rhythm
Composed By: [Aaron Spencer](https://machinimasound.com/about/)
License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)
Source: [https://machinimasound.com/?s=neuro+rhythm](https://machinimasound.com/?s=neuro+rhythm)

### Nectar
Composed By: [Of Far Different Nature](fardifferent.carrd.co/)
License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
Source: [https://soundcloud.com/fardifferent/03-nectar-free-music-for-games](https://soundcloud.com/fardifferent/03-nectar-free-music-for-games)

## Visualizing Sound
So it would look kind of cool if the bubbles in this game pulsed along with the music. We could even do something impactful like making the bubbles go faster when the music is loud. But, to do this we need to know how loud the music is at any given time. It doesn't look like Godot has a way to get this info natively, but all that means is that it's time to bust out our silly hacking hats.

Take a look at [this python script](https://gitlab.com/bubble-racing/bubble-racing-client/-/blob/master/music/generate-volume-files.py) that takes a given audio file and spits out a series of numbers between 0 and 100. 0 means the song is quiet at that point in time, and 100 means its as loud as it's going to get. There are 10 readings per second, so if we want to know how loud a song is at the 37th second, we would take a look at the 370th number in the file. Where this gets extra fancy is that the numbers are formatted into a .gd script which Godot can read. Such convenience.

## Loading it all into Godot
We'll be using the "AudioStreamPlayer", which is a thing that Godot uses to play sounds. Godot also has an "AudioStreamPlayer2D" which plays sounds louder if they are closer to you and pans sounds to the left and right if the object if left or right of you. We'll use that later, but for music we'll just stick with the simpler "AudioStreamPlayer".

Create a Music scene, and add some "AudioStreamPlayers" and a "Timer" like so:

Music
 ┣ ElectricDreams <AudioStreamPlayer>
 ┣ DanceOfThePixies <AudioStreamPlayer>
 ┣ Nectar <AudioStreamPlayer>
 ┣ NeuroRhythm <AudioStreamPlayer>
 ┣ Timer <Timer>
 
Connect each of the songs to the .gd script made for that song. It's the script with all of the volume values.

Copy the Music script below, and attach it to the Music scene. This script has a lot of event handlers, so you'll need to connect the "Timer" and "AudioStreamPlayer"s to the script using the "Node" tab on the left. That part is a little hard to describe, so you might be better off watching the video at the top of this readme.

```
extends Node2D

var title                             # will be set in _ready
var race                              # will be set in _ready
var min_volume = -30                  # turn down a song until it reaches this volume, then stop playing it
var rnd = RandomNumberGenerator.new() # used to start on a random song
var loudness = 0                      # will be updated as music plays
var smoothing = 2                     # how smooth to make the visuals
var offset = 0.05                     # set the visuals a fraction of a second ahead of the audio
var cur_song                          # will be set once a song is playing

# Called when the node enters the scene tree for the first time.
func _ready():
	rnd.randomize()
	title = {
		'playlist': [$ElectricDreams],
		'index': 0,
		'position': 0,
		'volume': min_volume,
		'max_volume': 0
	}
	race = {
		'playlist': [$DanceOfThePixies, $Nectar, $NeuroRhythm],
		'index': rnd.randi_range(0, 2),
		'position': 0,
		'volume': min_volume,
		'max_volume': -3
	}
	
func play_title():
	play(title)
	stop(race)
	
func play_race():
	play(race)
	stop(title)
	
func play(target):
	target.volume = target.max_volume
	cur_song = target.playlist[target.index]
	if (!cur_song.playing):
		cur_song.play(target.position)

func stop(target):
	var cur_song = target.playlist[target.index]
	target.position = cur_song.get_playback_position()
	target.volume = min_volume

func on_song_complete(target):
	if (target.volume > min_volume):
		var index = target.index + 1
		if (index >= len(target.playlist)):
			index = 0
		cur_song = target.playlist[index]
		cur_song.play()
		target.index = index
		target.position = 0

func fade_music(target):
	var song = target.playlist[target.index]
	if (song.playing):
		if (song.volume_db > target.volume):
			song.volume_db -= 3
		elif (song.volume_db < target.volume):
			song.volume_db += 3
			if (song.volume_db > target.volume):
				song.volume_db = target.volume
		if (song.volume_db <= min_volume):
			song.stop()

func _on_Timer_timeout():
	fade_music(title)
	fade_music(race)

func _on_Title_finished():
	on_song_complete(title)

func _on_Race1_finished():
	on_song_complete(race)

func _on_Race2_finished():
	on_song_complete(race)

func _on_Race3_finished():
	on_song_complete(race)

func _process(_delta):
	if cur_song:
		var position = cur_song.get_playback_position() + offset
		var index = int(round(position * 10))
		if index < len(cur_song.volume_values):
			var new_loudness = cur_song.volume_values[index]
			loudness = ((loudness * smoothing) + new_loudness) / (smoothing + 1)
```

### Testing it out
Make a new `MusicTest` scene. Drag a couple of buttons and a Wall into the scene. Connect the buttons to the `_on_RaceButton_pressed` and `_on_TitleButton_pressed` functions in the script below. 

```
extends Node2D

var initialScale = Vector2(1, 1)

func _on_RaceButton_pressed():
	$Music.play_race()

func _on_TitleButton_pressed():
	$Music.play_title()

func _process(_delta):
	$Wall.scale = initialScale + (initialScale * $Music.loudness / 100)
```