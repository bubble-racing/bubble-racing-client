extends Node2D

var title                             # will be set in _ready
var race                              # will be set in _ready
var min_volume = -30                  # turn down a song until it reaches this volume, then stop playing it
var rnd = RandomNumberGenerator.new() # used to start on a random song
var loudness = 0                      # will be updated as music plays
var smoothing = 2                     # how smooth to make the visuals
var offset = 0.05                     # set the visuals a fraction of a second ahead of the audio
var cur_song                          # will be set once a song is playing
var fade_speed = 3                    # speed of fade from one song to the next
var dampen = 0                        # set to a value > 0 to temporarially dampen the music in db
var dampen_recovery_speed = 1         # db per timeout to recover from dampen


func _ready():
	rnd.randomize()
	title = {
		'playlist': [$TheIntroWeHaveBeenWaitingFor],
		'index': 0,
		'position': 0,
		'volume': min_volume,
		'max_volume': 0
	}
	race = {
		'playlist': [$DanceOfThePixies, $Nectar, $Kinetics],
		'index': rnd.randi_range(0, 2),
		'position': 0,
		'volume': min_volume,
		'max_volume': -3
	}
	
	
func play_title():
	stop(race)
	play(title)
	
	
func play_race():
	stop(title)
	play(race)
	
	
func play(target):
	target.volume = target.max_volume
	cur_song = target.playlist[target.index]
	cur_song.volume_db = target.volume
	if (!cur_song.playing && Settings.persist.get('music', true)):
		cur_song.play(target.position)


func stop(target):
	var cur_song = target.playlist[target.index]
	target.position = cur_song.get_playback_position()
	target.volume = min_volume


func on_song_complete(target):
	if (target.volume > min_volume):
		var index = target.index + 1
		if (index >= len(target.playlist)):
			index = 0
		cur_song = target.playlist[index]
		cur_song.play()
		target.index = index
		target.position = 0


func fade_music(target):
	if dampen > 0:
		dampen -= dampen_recovery_speed
	var song = target.playlist[target.index]
	if (song.playing):
		if (song.volume_db > target.volume - dampen):
			song.volume_db -= fade_speed
		elif (song.volume_db < target.volume - dampen):
			song.volume_db += fade_speed
			if (song.volume_db > target.volume - dampen):
				song.volume_db = target.volume - dampen
		if (song.volume_db <= min_volume - dampen):
			song.stop()


func dampen(db, recovery_speed=1):
	dampen = db
	dampen_recovery_speed = recovery_speed
	

func _on_Timer_timeout():
	fade_music(title)
	fade_music(race)


func _on_Race1_finished():
	on_song_complete(race)


func _on_Race2_finished():
	on_song_complete(race)


func _on_Race3_finished():
	on_song_complete(race)


func _on_TheIntroWeHaveBeenWaitingFor_finished():
	on_song_complete(title)


func _process(_delta):
	if cur_song:
		var position = cur_song.get_playback_position() + offset
		var index = int(round(position * 10))
		if index < len(cur_song.volume_values):
			var new_loudness = cur_song.volume_values[index]
			loudness = ((loudness * smoothing) + new_loudness) / (smoothing + 1)
