from pydub import AudioSegment
from math import isinf
import json
import os

# Create a list of volume values from a given sound file
def readVolumeFromSound(filename):
  sound = AudioSegment.from_mp3(filename)
  values = []

  # Get one sample per 100ms
  for _i, chunk in enumerate(sound[::100]):

    # Get dBFS from pydub
    # dBFS = decibels relative to Full Scale
    # 0 = full volume
    # -Infinity = no volume
    dBFS = chunk.dBFS

    # Replace '-infinity' values
    if isinf(chunk.dBFS):
      dBFS = -100

    # Treat -35 as the minimum volume
    if (dBFS < -35):
      dBFS = -35

    # Convert the -35 to 0 scale into a 0 to 100 scale
    vol = (dBFS + 35) * 4

    # Add it to the list
    values.append(vol)

  # Scale the max volume up to 100
  maxVol = max(values)
  multiple = 100 / maxVol
  values = [vol * multiple for vol in values]

  # Cut off decimal points to save some space
  values = [round(vol) for vol in values]

  return values


# Loop through every .mp3 file in this folder
# Write volume_values for each found .mp3 sound
for root, dirs, files in os.walk("."):
  for filename in files:
    if(filename.endswith(".mp3")):
      print(filename)
      volumes = readVolumeFromSound(filename)
      with open(f"{os.path.splitext(filename)[0]}.gd", 'w') as outfile:
        outfile.write(
          "extends AudioStreamPlayer\n"
          f"var volume_values = {volumes}"
        )
