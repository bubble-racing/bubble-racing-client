extends Node2D

# These kind of need to be multiples of 3
var map_width = 9
var map_height = 9


func _ready():
	var settings = {
		width = 9,
		height = 9,
		phosphorus = 1,
		speed = 2,
		currents = 1,
		debris = 1,
		vortex = 4,
		ghost = 1,
		oil = 1,
		bubbles = 4,
		breadcrumbs = 1
	}
	var map = $LevelGenerator.generate(settings)
	_render_map(map)


func _render_map(map):
	var text = ""
	for y in map.height:
		text = text + "\n"
		for x in map.width:
			var id = str( map.get_tile(x, y) )
			while id.length() < 2:
				id += "_"
			text = text + id + "_"
	$TextEdit.text = text
