extends Node

var MazeGenerator = preload("../maze_generator/MazeGenerator.gd")
var mazeGenerator = MazeGenerator.new()
var rng = RandomNumberGenerator.new()


func generate(settings) -> Map2D:
	# randomize
	rng.randomize()
	
	# create maze
	var maze = mazeGenerator.generate(settings.width / 3, settings.height / 3)
	var map = Map2D.new()
	
	# turn the maze into a basic 2D tilemap
	maze.shuffle()
	for node in maze:
		add_node(map, node, settings.width)
	
	# add bubble start position
	map.set_tile(round(settings.width / 2), settings.height - 2, TileTypes.BUBBLE)
	
	# add in extra goodies
	replace_tiles(map, settings, TileTypes.WALL, TileTypes.PHOSPHORUS, settings.get('phosphorus', 0), 1)
	replace_tiles(map, settings, TileTypes.EMPTY, TileTypes.PHOSPHORUS, settings.get('blockers', 0), 1)
	replace_tiles(map, settings, TileTypes.EMPTY, TileTypes.SPEED, settings.get('speed', 0))
	replace_tiles(map, settings, TileTypes.EMPTY, TileTypes.CURRENT_UP, settings.get('current_up', 0))
	replace_tiles(map, settings, TileTypes.EMPTY, TileTypes.CURRENT_RIGHT, settings.get('current_right', 0))
	replace_tiles(map, settings, TileTypes.EMPTY, TileTypes.CURRENT_DOWN, settings.get('current_down', 0))
	replace_tiles(map, settings, TileTypes.EMPTY, TileTypes.CURRENT_LEFT, settings.get('current_left', 0))
	replace_tiles(map, settings, TileTypes.EMPTY, TileTypes.DEBRIS, settings.get('debris', 0))
	replace_tiles(map, settings, TileTypes.EMPTY, TileTypes.VORTEX, settings.get('vortex', 0))
	replace_tiles(map, settings, TileTypes.EMPTY, TileTypes.GHOST, settings.get('ghost', 0))
	replace_tiles(map, settings, TileTypes.EMPTY, TileTypes.OIL, settings.get('oil', 0))
	replace_tiles(map, settings, TileTypes.EMPTY, TileTypes.BREADCRUMBS, settings.get('breadcrumbs', 0))
	replace_tiles(map, settings, TileTypes.EMPTY, TileTypes.KEY, settings.get('key', 0))
	replace_tiles(map, settings, TileTypes.EMPTY, TileTypes.CHEST, settings.get('chest', 0))
	replace_tiles(map, settings, TileTypes.EMPTY, TileTypes.TURTLE, settings.get('turtle', 0))
	replace_tiles(map, settings, TileTypes.EMPTY, TileTypes.DIVER, settings.get('diver', 0))
	replace_tiles(map, settings, TileTypes.EMPTY, TileTypes.CHERRIES, settings.get('cherries', 0))
	replace_tiles(map, settings, TileTypes.EMPTY, TileTypes.STARFISH, settings.get('starfish', 0), 2)
	
	# done!
	map.calc_bounds()
	return map


# Replace some number of in_type tiles with out_type
# Tiles will be selected randomly
# This function is a little hacky
func replace_tiles(map: Map2D, settings, in_type: int, out_type: int, count: int, inset: int=0):
	if(!count):
		return
		
	var found = 0
	for i in 100:
		var x = rng.randi_range(1 + inset, settings.width - 2 - inset)
		var y = rng.randi_range(1 + inset, settings.height - 2 - inset)
		var tile = map.get_tile(x, y)
		if(tile == in_type):
			map.set_tile(x, y, out_type)
			found += 1
			if(found >= count):
				break


func add_node(map, node, width):
	var up = false
	var right = false
	var down = false
	var left = false
	var x = node.position.x * 3
	var y = node.position.y * 3
	
	for connection in node.connections:
		if node.position.y - connection.y > 0:
			up = true
		if node.position.x - connection.x < 0:
			right = true
		if node.position.y - connection.y < 0:
			down = true
		if node.position.x - connection.x > 0:
			left = true
	
	# exit aka finish line
	if node.position.y == 0 and node.position.x == floor(width / 3 / 2):
		up = true
	
	# add corners
	add_box(map, x, y)
	add_box(map, x + 2, y)
	add_box(map, x + 2, y + 2)
	add_box(map, x, y + 2)
	
	# add walls
	if !up:
		add_box(map, x + 1, y)
	if !right:
		add_box(map, x + 2, y + 1)
	if !down:
		add_box(map, x + 1, y + 2)
	if !left:
		add_box(map, x, y + 1)


func add_box(map, x, y):
	var top = 1 if map.get_tile(x, y - 1) else 0
	var right = 1 if map.get_tile(x + 1, y) else 0
	var bottom = 1 if map.get_tile(x, y + 1) else 0
	var left = 1 if map.get_tile(x - 1, y) else 0
	if (!top and !bottom) or (!right and !left):
		map.set_tile(x, y, 1)
