# Bubble Racing

## Getting started
This game is built in Godot. It's free and open source. Pretty rad. 
[Download Godot](https://godotengine.org/download)
[![install godot](readme_images/install-godot.gif)](https://tube.jigg.io/videos/watch/1cd38eeb-a769-4a46-a206-e8d62cd575eb)

## Link Godot (macos)
```
sudo ln -s /Applications/Godot.app/Contents/MacOS/Godot /usr/local/bin/godot
```

## Run environment locally
```
docker compose \
  -f automation/compose/compose.yaml \
  --project-name bubble-racing-local \
  up \
  --remove-orphans \
  --build
```

## Run server locally by itself
```
export GAMESERVER_KEY='1234'
export GAMESERVER_ID='9'
export API_URL='http://localhost/api'
export SELF_URL='ws://localhost:9999'

godot -v --no-window --export-pack "Server" build/bubble-racing-server.pck && godot --server --no-window --main-pack build/bubble-racing-server.pck
```
