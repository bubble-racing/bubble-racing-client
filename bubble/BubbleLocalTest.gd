extends Node2D


func _ready():
	$CanvasLayer/SpeedSlider.connect("value_changed", self, "_on_settings_changed")
	$CanvasLayer/ToughSlider.connect("value_changed", self, "_on_settings_changed")
	$CanvasLayer/SkillSlider.connect("value_changed", self, "_on_settings_changed")
	_on_settings_changed(0)


func _on_settings_changed(_value):
	$Bubble.speed = $CanvasLayer/SpeedSlider.value
	$Bubble.tough = $CanvasLayer/ToughSlider.value
	$Bubble.skill = $CanvasLayer/SkillSlider.value
