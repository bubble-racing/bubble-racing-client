extends './Bubble.gd'

var client_positions = []
var position_index = 0
var rng = RandomNumberGenerator.new()
var mode_dist = Settings.tile_size


func _ready():
	control = 'remote'
	rng.randomize()
	$AutopilotTimer.connect('timeout', self, '_think')


func _save_client_position(client_position: Vector2) -> void:
	var client_tile = Vector2(int(client_position.x / Settings.tile_size), int(client_position.y / Settings.tile_size))
	
	if len(client_positions) == 0:
		client_positions.push_back(client_tile)
	else:
		var previous_client_tile = client_positions.back()
		if client_tile.x != previous_client_tile.x && client_tile.y != previous_client_tile.y:
			client_positions.push_back(client_tile)


func update_from_client(update: Array):
	var client_position = Vector2(update[0], update[1])

	# move toward target_position if we are close to being in the right spot
	var dist = position.distance_to(client_position)
	if dist < mode_dist:
		target_position = Vector2(update[2], update[3])
		client_positions.clear()
	
	# save positions in case we fall behind
	_save_client_position(client_position)


func _think():
	if len(client_positions) == 0:
		return
	var client_position = client_positions[len(client_positions) - 1] * Settings.tile_size
	if position.distance_to(client_position) > mode_dist:
		target_position = client_positions.front() * Settings.tile_size
		if position.distance_to(target_position) < mode_dist:
			client_positions.pop_front()
		
		# push target_position forward some, so the bubble can hit max speed
		target_position = position + (target_position - position).normalized() * 500


func impact(_vector):
	return false
