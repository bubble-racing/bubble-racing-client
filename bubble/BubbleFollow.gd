extends Bubble

var target: Bubble
var positions: PoolVector2Array = []
var target_follow_position = Vector2(0, 0)
var min_dist = 130


func _ready():
	$Timer.connect("timeout", self, "_on_timeout")


func _on_timeout():
	if target:
		var close_position = (position - target.position).normalized() * (min_dist + 25) + target.position
		positions.append(Vector2(close_position.x, close_position.y))
		if len(positions) > 2:
			var past_position = positions[0]
			positions.remove(0)
			target_follow_position = past_position


func _process(delta):
	if target:
		target_position = Vector2(target_follow_position.x, target_follow_position.y)
		var dist = position.distance_to(target.position)
		if dist < min_dist:
			target_position = (position - target.position).normalized() * (min_dist - dist) / min_dist * 500 + target.position
		elif dist > 700:
			position = position + (target.position - position) / 2


func _exit_tree():
	target = null
