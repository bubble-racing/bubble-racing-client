class_name AutoPilot

var target_tile: Vector2
var time_since_checkpoint: int = 0
var map: Map2D
var ghost_avoid_dist: int = 400
var target: Seeker
var timer: Timer
var level_solver: LevelSolver = LevelSolver.new()
var active: bool = false
var corners = [Vector2(-1, -1), Vector2(1, -1), Vector2(-1, 1), Vector2(1, 1)]
var phosphorus_position: Vector2
var phosphorus_timer = 0


func activate(target_seeker: Seeker, map_2d: Map2D):
	target = target_seeker
	map = map_2d

	timer = Timer.new()
	timer.wait_time = 0.15
	target.add_child(timer)
	timer.connect("timeout", self, "_run_bot")
	timer.start()
	
	level_solver.activate(map, _pos_to_tile(target.position))


func pause():
	if active:
		active = false


func resume():
	if !active:
		active = true
		target_tile = _pos_to_tile(target.position)


func _pos_to_tile(pos: Vector2) -> Vector2:
	var tile = Vector2(round(pos.x / Settings.tile_size), round(pos.y / Settings.tile_size))
	tile.x = clamp(tile.x, map.min_pos.x, map.max_pos.x)
	tile.y = clamp(tile.y, map.min_pos.y, map.max_pos.y)
	return tile


func _run_bot():
	if !active || target.finished:
		return
		
	if target.position.distance_to(target_tile * Settings.tile_size) < 300:
		target_tile = level_solver.get_next_position()
		time_since_checkpoint = 0
	else:
		time_since_checkpoint += 1
		
	var pos = target_tile * Settings.tile_size
	var dist = (pos - target.position).normalized() * 500
	target.target_position = target.position + dist
	
	# Look more in front for ghosts
	target.get_node('Area2D/CollisionShape2D').position = (pos - target.position).normalized() * 200
	
	# try to dodge ghosts
	var bodies = target.get_node('Area2D').get_overlapping_bodies()
	var found_ghost = false
	for body in bodies:
		if body.get_class() == 'Ghost':
			var d = target.position - body.position
			var run_position = target.position + (d.normalized().rotated(deg2rad(45)) * ghost_avoid_dist)
			target.target_position = (target.target_position + run_position) / 2
			found_ghost = true
			break
	
	# To avoid getting stuck, avoid ghosts less with continued exposure
	if found_ghost:
		if ghost_avoid_dist >= 10:
			ghost_avoid_dist -= 10
	else:
		ghost_avoid_dist = 400
	
	# Go to phosphorus, because it's cool
	if phosphorus_timer <= 0:
		for corner in corners:
			var corner_tile = target_tile + corner
			var tile = map.get_tile(corner_tile.x, corner_tile.y)
			if tile == TileTypes.PHOSPHORUS:
				phosphorus_position = corner_tile * Settings.tile_size
				phosphorus_timer = 20
	else:
		phosphorus_timer -= 1
		target.target_position = phosphorus_position

	# If we're stuck, calculate a new path
	if time_since_checkpoint > 20:
		target_tile = _pos_to_tile(target.position)
		level_solver.activate(map, target_tile)
