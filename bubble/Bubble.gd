extends Seeker
class_name Bubble

signal finished

var impact_vector: Vector2 = Vector2(0, 0)
var min_y = -50

var speed = 0
var tough = 0
var skill = 0

var speed_countdown      = 0.0
var oil_countdown        = 0.0
var impact_countdown     = 0.0
var cherry_countdown     = 0.0
var invincible_countdown = 0.0

var sprite_base_scale = Vector2(0.8, 0.8)
var sprite_grow_scale = Vector2(0.15, 0.15)
var sprite_base_alpha = 1
var sprite_grow_alpha = 0

var ring_base_scale = Vector2(0.20, 0.20)
var ring_grow_scale = Vector2(0.14, 0.14)
var ring_base_alpha = 0.4
var ring_grow_alpha = 0.4

var light_base_scale = Vector2(0.5, 0.5)
var light_grow_scale = Vector2(1.0, 1.0)
var light_base_energy = 0.5
var light_grow_energy = 0.5
var finished = false

# Used (sometimes)
var autopilot: AutoPilot


# This is a bubble
func get_class():
	return 'Bubble'


func customize(settings: Dictionary) -> void:
	speed = settings.nature.speed + settings.nurture.speed
	tough = settings.nature.tough + settings.nurture.tough
	skill = settings.nature.skill + settings.nurture.skill
	$Name.visible = true
	$Name.text = settings.name
	get_node('Sprite/Face').customize(settings)


func _ready():
	$Light2D.visible = true


func _exit_tree():
	Settings.bubble_counter -= 1


func finish():
	if finished:
		return
	finished = true
	emit_signal('finished')
	pop()


func pop():
	if Settings.persist.sound_effects:
		$PopSound.play()
	$Animator.play('pop')


func chomp():
	if Settings.persist.sound_effects:
		$ChompSound.play()


# Turn on a temporary speed burst
func activate_speed():
	$SpeedParticles.emitting = true
	set_linear_damp(base_linear_damp / 3)
	speed_countdown = 4 + (skill * 1.1)
	if oil_countdown > 0:
		oil_countdown = 0
		$OilParticles.emitting = false


# Turn on a temporary slow burst
func activate_oil():
	set_linear_damp(base_linear_damp * 2)
	$OilParticles.emitting = true
	oil_countdown = 4 - (tough * 0.5)
	if speed_countdown > 0:
		speed_countdown = 0
		$SpeedParticles.emitting = false


# Begin the ghost hunt
func activate_cherry():
	cherry_countdown = 10.0
	if control == 'local':
		get_parent().emit_signal('cherry_begin')


# Get hit by something, causes a stun effect
func impact(vector):
	if impact_countdown > 0 || invincible_countdown > 0 || finished:
		return false
	var impulse = vector * 1000
	apply_central_impulse(impulse)
	$SpeedParticles.emitting = true
	$OilParticles.emitting = true
	impact_vector = vector
	set_linear_damp(base_linear_damp / 5)
	impact_countdown = 3 - (tough * 0.4)
	return true

#
func _physics_process(delta):
	
	# Speed
	if speed_countdown > 0:
		speed_countdown -= delta
		if speed_countdown <= 0:
			$SpeedParticles.emitting = false
			set_linear_damp(base_linear_damp)
	
	# Oil
	if oil_countdown > 0:
		oil_countdown -= delta
		if oil_countdown <= 0:
			$OilParticles.emitting = false
			set_linear_damp(base_linear_damp)
	
	# Impact
	if impact_countdown > 0:
		impact_countdown -= delta
		target_position = position + (impact_vector.rotated(3 - impact_countdown) * 100)
		$Sprite.rotation_degrees += 10
		if impact_countdown <= 0:
			$Sprite.rotation_degrees = 0
			$SpeedParticles.emitting = false
			$OilParticles.emitting = false
			set_linear_damp(base_linear_damp)
			invincible_countdown = 6.0 + (1.0 * tough)
	
	# Invincible
	if invincible_countdown > 0:
		invincible_countdown -= delta
		var is_even = int(invincible_countdown * 10) % 2 == 0
		if is_even:
			$Sprite/Face.modulate.a = 1
		else:
			$Sprite/Face.modulate.a = 0.6
		if invincible_countdown <= 0:
			$Sprite/Face.modulate.a = 1
	
	# Cherry
	if cherry_countdown > 0:
		cherry_countdown -= delta
		if cherry_countdown <= 0:
			if control == 'local':
				get_parent().emit_signal('cherry_end')
	
	# Leave a trail when moving fast
	if $WakeParticles:
		$WakeParticles.emitting = oil_countdown <= 0 && linear_velocity.length() > 300
	
	# Oil dampens our groove
	var loudness = Music.loudness / 100.0
	if oil_countdown > 0:
		# loudness /= 4
		pass
	
	# move faster when the music is louder
	# acceleration = 20 + (loudness * 20)
	acceleration = 30 + (speed * 3)
	
	# Pulse with the music	
	if $Sprite:
		$Sprite.scale = sprite_base_scale + (sprite_grow_scale * loudness)
		$Sprite.modulate.a = sprite_base_alpha + (sprite_grow_alpha * loudness)
		$Ring.scale = ring_base_scale + (ring_grow_scale * loudness)
		$Ring.modulate.a = ring_base_alpha + (ring_grow_alpha * loudness)
		$Light2D.scale = light_base_scale + (light_grow_scale * loudness)
		$Light2D.energy = min(1, light_base_energy + (light_grow_energy * loudness))
	
	# Point face at target
	$Sprite/Face.target_position = target_position - position

	# Pop if we breach the surface
	if position.y <= min_y and !finished:
		finish()


func teleport(new_position):
	if !get_tree():
		position = new_position
		return
		
	# disable physics
	set_physics_process(false)

	# wait for physics engine to chill out
	yield(get_tree(), "idle_frame")

	# do teleport
	position = new_position

	# wait again to prevent physics engine from undoing teleport
	yield(get_tree(), "idle_frame")

	# re-enable physics
	set_physics_process(true)


func activate_autopilot(map: Map2D):
	autopilot = AutoPilot.new()
	autopilot.activate(self, map)
	autopilot.resume()
