extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	Music.play_race()


func _on_left_pressed():
	$Bubble.target_position = Vector2(100, 200)


func _on_right_pressed():
	$Bubble.target_position = Vector2(700, 200)


func _on_speed_pressed():
	$Bubble.activate_speed()


func _on_Button_pressed():
	$Bubble.activate_oil()
