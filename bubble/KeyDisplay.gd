extends Node2D


func _ready():
	_render()


func _render() -> void:
	var count = Pickups.get_stowed('keys') + Pickups.get_current('keys')	
	$KeyText.text = String(count)


func flash() -> void:
	_render()
	$AnimationPlayer.play('flash')
