extends './Bubble.gd'
class_name BubbleLocal

var mouse_is_down: bool = false
var target_zoom = Vector2(1.5, 1.5)
var target_camera_position = Vector2(0, 0)
var is_zooming = false
onready var camera = get_node('Camera2D')


# This is a BubbleLocal
func get_class():
	return 'BubbleLocal'

#
func _ready():
	target_position = position
	light_base_scale = Vector2(1.5, 1.5)
	light_grow_scale = Vector2(1.0, 1.0)
	light_base_energy = 0.8


func zoom_from(_zoom: Vector2, _position: Vector2):
	is_zooming = true
	camera.zoom = _zoom
	camera.position = _position - position
	camera.current = true


# Move towards the mouse
func _physics_process(delta):
	if finished:
		return
	if mouse_is_down:
		target_position = get_global_mouse_position()
	if is_zooming:
		var zoom_diff = target_zoom - camera.zoom
		var position_diff = target_camera_position - camera.position
		if zoom_diff.length() < 0.05:
			camera.zoom = target_zoom
			camera.position = target_camera_position
			is_zooming = false
		else:
			camera.zoom += zoom_diff / 25
			camera.position += position_diff / 25


func _input(event):
	if (
		!finished &&
		((event is InputEventMouseButton && event.button_index == BUTTON_LEFT) ||
		(event is InputEventScreenTouch && event.pressed))
	):
		mouse_is_down = event.pressed
		target_position = get_global_mouse_position()


func flash_keys() -> void:
	$KeyDisplay.flash()


func finish():
	if finished:
		return
	finished = true
	target_position = Vector2(position.x, -10)
	set_linear_damp(base_linear_damp * 2)
	emit_signal('finished')
	$AnimationPlayer.play('zoom_in')
	if Settings.persist.sound_effects:
		$Splash.play()
	Music.dampen(30, 0.5)
