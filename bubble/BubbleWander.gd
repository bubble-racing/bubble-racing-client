extends Bubble
class_name BubbleWander

var timer = Timer.new()


# Called when the node enters the scene tree for the first time.
func _ready():
	._ready()
	add_child(timer)
	timer.connect("timeout", self, "_on_timeout")
	timer.wait_time = 0.25
	timer.start()
	control = 'local'


# Move towards target position
func _on_timeout():
	if (linear_velocity.length() < 60):
		var move_x = rand_range(-1000, 1000)
		var move_y = rand_range(-1000, 1000)
		target_position = position + Vector2(move_x, move_y)
		
